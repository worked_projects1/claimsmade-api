module.exports.appConstants = {
    STATUS_ACTIVE: "Active",
    STATUS_INACTIVE: "Inactive",
    ADMIN_EMAIL_IDS: ['siva@miotiv.com'],
    USER_ROLES: ['physician', 'staff'],
    ADMIN_ROLES: ['superAdmin', 'manager', 'operator'],
    ROLE_PHYSICIAN: 'physician',
    ROLE_STAFF: 'staff',
    POLICY_TYPES: ['surgery', 'surgery-add-on', 'other'],
    APP_NAME: 'VettedClaims',
    ORDER_STATUS_COMPLETED: 'completed',
    ORDER_TYPE_RFA: 'RFA_FORM_DOC',
    PLAN_PAY_AS_YOU_GO: 'pay_as_you_go',
    ADMIN_EMAIL_ID: 'siva@miotiv.com'
}