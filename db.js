const Sequelize = require('sequelize');
const PracticesModel = require('./rest/practices/model');
const UsersModel = require('./rest/users/model');
const PasswordHistoryModel = require('./rest/users/password_history_model');
const SettingsModel = require('./rest/settings/model');
const TreatmentsModel = require('./rest/treatments/model');
const DiagnosisModel = require('./rest/treatments/diagnosis_model');
const BodySystemModel = require('./rest/bodySystem/model');
const TreatmentHistoryModel = require('./rest/treatments/treatment_history_model');
const VersionHistoryModel = require('./rest/versionHistory/model');
const rfaFormsModel = require('./rest/rfa_forms/model');
const ClaimsAdminsModel = require('./rest/claims_admin/model');
const SignAuthModel = require('./rest/users/signAuthModel');
const PlansModel = require('./rest/plans/model');
const OrdersModel = require('./rest/orders/model');
const SubscriptionsModel = require('./rest/subscriptions/model');
const SubscriptionHistoryModel = require('./rest/subscriptions/subscription_history_model');

const Op = Sequelize.Op;

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USER,
  process.env.DB_PASSWORD,
  {
    dialect: 'mysql',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialectOptions: { decimalNumbers: true },
    logging: false,
  }
);

const Practices = PracticesModel(sequelize, Sequelize);
const Users = UsersModel(sequelize, Sequelize);
const PasswordHistory = PasswordHistoryModel(sequelize, Sequelize);
const Settings = SettingsModel(sequelize, Sequelize);
const Treatments = TreatmentsModel(sequelize, Sequelize);
const Diagnosis = DiagnosisModel(sequelize, Sequelize);
const BodySystem = BodySystemModel(sequelize, Sequelize)
const TreatmentHistory = TreatmentHistoryModel(sequelize, Sequelize)
const VersionHistory = VersionHistoryModel(sequelize, Sequelize)
const RfaForms = rfaFormsModel(sequelize, Sequelize);
const ClaimsAdmins = ClaimsAdminsModel(sequelize, Sequelize);
const SignAuthorization = SignAuthModel(sequelize, Sequelize);
const Plans = PlansModel(sequelize, Sequelize);
const Orders = OrdersModel(sequelize, Sequelize);
const Subscriptions = SubscriptionsModel(sequelize, Sequelize);
const SubscriptionHistory = SubscriptionHistoryModel(sequelize, Sequelize);

Treatments.hasMany(Diagnosis, { foreignKey: 'treatment_id' });
Diagnosis.belongsTo(Treatments, { foreignKey: 'treatment_id' });

BodySystem.hasMany(Treatments, { foreignKey: 'body_system_id' });
Treatments.belongsTo(BodySystem, { foreignKey: 'body_system_id' });

Practices.hasMany(Users, { foreignKey: 'practice_id' });
Users.belongsTo(Practices, { foreignKey: 'practice_id' });

const Models = {
  Op,
  Practices,
  Users,
  PasswordHistory,
  Settings,
  sequelize,
  Treatments,
  Diagnosis,
  BodySystem,
  TreatmentHistory,
  VersionHistory,
  RfaForms,
  ClaimsAdmins,
  SignAuthorization,
  Plans,
  Orders,
  Subscriptions,
  SubscriptionHistory
};
const connection = {};

module.exports = async () => {
  if (connection.isConnected) {
    console.log('=> Using existing connection.');
    return Models;
  }
  await sequelize.sync();
  await sequelize.authenticate();
  connection.isConnected = true;
  console.log('=> Created a new connection.');
  return Models;
};
