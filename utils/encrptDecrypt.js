const CryptoJS = require("crypto-js");

module.exports.encrypt = async (input) => {
    const encrypt = CryptoJS.AES.encrypt(JSON.stringify(input), process.env.CRYPTO_ENCRYPTION_KEY).toString();
    return encrypt;
}

module.exports.decrypt = async (encryptedText) => {
    const decrypt = CryptoJS.AES.decrypt(encryptedText, process.env.CRYPTO_ENCRYPTION_KEY).toString(CryptoJS.enc.Utf8);
    return decrypt;
}

