const AWS = require('aws-sdk');
const { HTTPError } = require('./httpResp');
AWS.config.update({ region: process.env.REGION || 'us-west-2' });
const s3 = new AWS.S3();
const fs = require('fs');
AWS.config.setPromisesDependency(require('bluebird'));
const shell = require('shelljs');

const getSignedUrlForS3 = async (event, s3FileKey, bucketName, content_type, accessParam, metaData) => {

    if (!metaData) {
        let userObj = {
            user_id: event.user.id
        }
        metaData = userObj;
    }

    const s3Params = {
        Bucket: bucketName, // S3 bucket name
        Key: s3FileKey, //Should be unique (eg: timeStamp)
        ContentType: content_type,  // application/pdf // application/json
        ACL: accessParam,  // 'private' or 'public-read'
        Metadata: metaData  //Optional
    };
    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
        });
    });
};

//To get the upload URL from s3

module.exports.getS3UploadURL = async (event, s3FileKey, bucketName, content_type, accessParam, metaData) => {
    try {
        const uploadURLObject = await getSignedUrlForS3(event, s3FileKey, bucketName, content_type, accessParam, metaData);
        return uploadURLObject;
    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not get the Upload URL.');
    }
};

// To get the public URL with expire time
module.exports.getSecuredS3PublicUrl = async (bucketName, bucketRegion, s3_file_key) => {
    try {
        AWS.config.update({ region: bucketRegion });
        const publicURL = await s3.getSignedUrlPromise('getObject', {
            Bucket: bucketName,
            Expires: 60 * 60 * 1,
            Key: s3_file_key,
        });
        return publicURL;
    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not get the public URL.');
    }
};

module.exports.getDownloadUrl = async (s3Bucket, fileKey, contentType, bucketRegion) => {
    try {
        AWS.config.update({ region: bucketRegion });
        const publicUrl = await s3.getSignedUrlPromise('getObject', {
            Bucket: s3Bucket,
            Expires: 60 * 60 * 1,
            Key: fileKey,
            ResponseContentDisposition: 'attachment; filename ="' + fileKey + contentType + '"'
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: {
                publicUrl: publicUrl
            }
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the download URL.' }),
        };
    }
}

//To merge the multiple pdf files using their s3 file keys
module.exports.mergePdfFiles = async (s3FileKeys, read_bucket_name, upload_bucket_name, merged_file_key) => {

    try {
        let filesToMerge = "";
        //read_bucket_name - From which bucket we need to read the file content
        //upload_bucket_name - In which bucket we need to upload the merged file content


        //Download the files from S3 to the /tmp directory
        for (const filekey of s3FileKeys) {

            const paramsFile = {
                Bucket: read_bucket_name,
                Key: filekey
            };

            let readStream = await s3.getObject(paramsFile).promise();

            await fs.promises.writeFile(`/tmp/${filekey}`, readStream.Body);

            filesToMerge += `/tmp/${filekey}` + " ";
        }

        //Merge the files stored in /tmp
        await shell.exec(`gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=/tmp/result.pdf -dBATCH ${filesToMerge}`);

        //Upload the result to S3
        const fileContent = await fs.createReadStream(`/tmp/result.pdf`);

        const params = {
            Bucket: upload_bucket_name,
            Key: merged_file_key,
            Body: fileContent,
            ContentType: "application/pdf"
        };

        const uploadResponse = await s3.upload(params).promise();

        return uploadResponse;
    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not merge the pdf files.');
    }
}

// To get the all s3 keys using bucket name
module.exports.getAllKeysByBucketName = async (bucketName) => {
    try {
        let params = { Bucket: bucketName };
        const keysArr = []

        const response = await s3.listObjects(params).promise();

        response.Contents.forEach(item => {
            keysArr.push(item.Key)
        });

        return keysArr
    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not get the s3 keys.');
    }
};


// To read the fileContent using s3 key and bucket name
module.exports.readFileContent = async (s3_file_key, bucketName) => {
    try {

        return new Promise(function (resolve, reject) {
            AWS.config.update({ region: process.env.S3_BUCKET_DEFAULT_REGION });
            const s3BucketParams = {
                Bucket: bucketName,
                Key: s3_file_key,
            };
            s3.getObject(s3BucketParams, function (err, data) {
                if (err) {
                    console.log(err);
                    reject(err.message);
                } else {
                    var data = Buffer.from(data.Body).toString('utf8');
                    resolve(data);
                }
            });
        });
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not Read the Documents.' }),
        };
    }
}

module.exports.uploadContentToS3 = async (bucketName, s3_file_key, fileContent, contentType, accessParam) => {
    try {
        let Timestamp = Date.now();
        const s3Params = {
            Bucket: bucketName,
            Key: s3_file_key,
            Body: fileContent,
            ContentType: contentType,
            ACL: accessParam
            // Metadata: {
            //     user_id: input.id
            // },
        };
        return new Promise(function (resolve, reject) {
            s3.upload(s3Params, function (err, data) {
                if (err) {
                    console.log(err);
                    reject(err.message);
                } else {
                    const data = {
                        s3_file_key: s3Params.Key
                    }
                    resolve(data);
                }
            });
        });
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
};

// To delete particular object or file from s3 bucket (single record from s3)
module.exports.deleteObjectFromS3Bucket = async (bucketName, key) => {
    try {
        const s3Params = {
            Bucket: bucketName,
            Key: key,
        };
        await s3.deleteObject(s3Params).promise();

    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not delete the object.');
    }
};


//To remove the all objects or files from S3 bucket (Multiple records from s3)
module.exports.deleteObjectsFroms3 = async (bucketName) => {
    try {

        // To get the all s3 keys using bucket name
        const getAllKeyObjects = async (bucketName) => {
            try {
                let params = { Bucket: bucketName };
                const objects = []

                const response = await s3.listObjects(params).promise();

                response.Contents.forEach(item => {
                    objects.push({ Key: item.Key })
                });

                return objects;
            } catch (err) {
                console.log(err);
                throw new HTTPError(404, err.message || 'Could not get the s3 keys.');
            }
        };


        const keys = await getAllKeyObjects(bucketName);

        let deletedCount = 0;

        if (keys.length > 0) {

            var options = {
                Bucket: bucketName,
                Delete: {
                    Objects: keys
                }
            };

            let resultObj = await s3.deleteObjects(options).promise();
            deletedCount = resultObj.Deleted.length;

        } else {
            deletedCount = 0; //There is no files present in s3 bucket
        }

        return deletedCount;

    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not delete the files.');

    }

}

// To read the s3 objects using the folder name
// To get all the keys by folder and bucket name
//Sample response

// {
//     "IsTruncated": false,
//     "Marker": "",
//     "Contents": [
//       {
//         "Key": "acetaminophen/acetaminophen_1667756763390.json",
//         "LastModified": "2022-11-23T07:54:03.000Z",
//         "ETag": "\"c1e06679d7eb7e2f8cc614186839653c\"",
//         "ChecksumAlgorithm": [

//         ],
//         "Size": 1978,
//         "StorageClass": "STANDARD",
//         "Owner": {
//           "DisplayName": "ssouissi",
//           "ID": "8b5ddec0f9134320f43d7d68668fec5749542c0e8d5396aec31b591af522f770"
//         }
//       },
//       {
//         "Key": "acetaminophen/acetaminophen_1667758434191.json",
//         "LastModified": "2022-11-23T07:54:04.000Z",
//         "ETag": "\"eed53b242c02680ad8718b88779012c8\"",
//         "ChecksumAlgorithm": [

//         ],
//         "Size": 6341,
//         "StorageClass": "STANDARD",
//         "Owner": {
//           "DisplayName": "ssouissi",
//           "ID": "8b5ddec0f9134320f43d7d68668fec5749542c0e8d5396aec31b591af522f770"
//         }
//       }
//     ],
//     "Name": "verified-policy-result-doc",
//     "Prefix": "acetaminophen/",
//     "Delimiter": "/",
//     "MaxKeys": 1000,
//     "CommonPrefixes": [

//     ]
//   }

module.exports.getS3ObjectsByFolderName = async (bucketName, s3Folder) => {
    try {

        const params = {
            Bucket: bucketName,
            Delimiter: '/',
            Prefix: s3Folder + '/'
        };

        const data = await s3.listObjects(params).promise();

        // for (let index = 1; index < data['Contents'].length; index++) {
        //     console.log(data['Contents'][index]['Key'])
        // }

        return data;

    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not get the S3 objects.');
    }
};

module.exports.getAllKeyObjects = async (bucketName) => {
    try {
        let params = { Bucket: bucketName };
        const objects = []

        const response = await s3.listObjects(params).promise();

        response.Contents.forEach(item => {
            objects.push({ Key: item.Key })
        });

        return objects;
    } catch (err) {
        console.log(err);
        throw new HTTPError(404, err.message || 'Could not get the s3 keys.');
    }
};
