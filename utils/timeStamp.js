exports.unixTimeStamptoDate = (unixTimestamp) => {

    const date = new Date(unixTimestamp * 1000);
    const year = date.getFullYear();
    const month = padTo2Digits(date.getMonth() + 1);
    const day = padTo2Digits(date.getDate());
    const dateTime = `${month}/${day}/${year}`;
    return dateTime;
}

function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
}