const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);


describe('RFA Forms GetAll Validation', () => {
    test('login-in a user as not running part of all', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.testEmail,
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(response.body.authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
        process.env.is_admin = JSON.parse(response.text).user.is_admin;
    });

    test('Verify if user able to get the RFA forms', async () => {
        const response = await server.get('/rest/rfaForms')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: "false",
                search: "false"
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    test('Verify if pagination in RFA list page is working or not', async () => {
        const response = await server.get('/rest/rfaForms')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 1,
                limit: 25,
                sort: "false",
                search: "false"
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(0);
    });

    test('Verify if search in RFA list page is working or not', async () => {
        const response = await server.get('/rest/rfaForms')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: "false",
                search: "case_01_updated"
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });


    test('Verify if user able to get all the rfa forms sort by name', async () => {
        const response = await server.get('/rest/rfaForms')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: JSON.stringify({ "column": "name", "type": "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    test('Verify if user able to get all the rfa forms sort by date_of_injury', async () => {
        const response = await server.get('/rest/rfaForms')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: JSON.stringify({ "column": "date_of_injury", "type": "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    test('Verify if user able to get all the rfa forms sort by claim_number', async () => {
        const response = await server.get('/rest/rfaForms')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: JSON.stringify({ "column": "claim_number", "type": "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    test('Verify if user able to get all the rfa forms sort by employer', async () => {
        const response = await server.get('/rest/rfaForms')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: JSON.stringify({ "column": "employer", "type": "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });
})