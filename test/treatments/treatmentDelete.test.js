const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Treatment delete validation', () => {

    //manager login
    test('manager login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[5],
                password: 'Test@123'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the manager is able to delete the treatment', async () => {
        const response = await server.delete(`/treatments/${config.treatmentTestRecordIds[0]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe('you don\'t have access');
    });

    //operator login
    test('Operator login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the operator is able to delete the treatment', async () => {
        const response = await server.delete(`/treatments/${config.treatmentTestRecordIds[0]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe('you don\'t have access');
    });

    //super admin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the super admin able to delete the treatment', async () => {
        const response = await server.delete(`/treatments/${config.treatmentTestRecordIds[0]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

});