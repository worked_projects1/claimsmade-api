const request = require('supertest');
const { config } = require('.././global');
const server = request(config.server_url);

describe('Treatments', () => {

    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the super admin is able to create a treatment', async () => {
        const response = await server.post('/treatments')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                "id": config.treatmentTestRecordIds[0],
                "treatment": {
                    "index-name": "test-acupuncture",
                    "label-name": "Test_Acupuncture"
                },
                "body-system": {
                    "index-name": "neck-upper-back",
                    "label-name": "Neck and Upper Back"
                },
                "version": {
                    "mtus": "04/18/2019",
                    "odg": "12/17/2021",
                    "policy-doc": "05/01/2022",
                    "created-by": "Binu",
                    "comments": ""
                },
                "policies": [
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-chronic",
                            "label-name": "Cervicothoracic Pain - Chronic"
                        },
                        "recommendation": "1",
                        "questions": [
                            {
                                "#": "1",
                                "question": "Is Acupuncture requested in combination with a conditioning program of aerobic and strengthening exercises?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "2"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            },
                            {
                                "#": "2",
                                "question": "Is this request for less than 6 initial sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "3"
                                    }
                                ]
                            },
                            {
                                "#": "3",
                                "question": "Will this request make the total sessions more than 12?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "deny"
                                    },
                                    {
                                        "option": "no",
                                        "action": "4"
                                    }
                                ]
                            },
                            {
                                "#": "4",
                                "question": "Are there improvements in objective measures from previous Acupuncture sessions to justify additional sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            }
                        ],
                        "mtus-text": "An initial trial of 5 to 6 appointments is recommended in combination with a conditioning program of aerobic and strengthening exercises. Future appointments should be tied to improvements in objective measures to justify an additional 6 sessions, for a total of 12 sessions."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-acute",
                            "label-name": "Cervicothoracic Pain - Acute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-subacute",
                            "label-name": "Cervicothoracic Pain - Subacute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    }
                ],
                "references": {
                    "mtus-link": "https://app.mdguidelines.com/state-guidelines/ca-mtus/cervical-and-thoracic-spine/diagnostic-and-treatment-recommendations/cervicothoracic-pain/treatment-recommendations/allied-health",
                    "odg-link": "https://www.odgbymcg.com/odg-link?tags=%20Acupuncture%20for%20Pain&formfield_country=US&st=st&ltdplan=182&stdEndDateMax=364&thispage=treatment&formfield_physicaldemandlevel=All%20Classes&rtwoption=bp_target"
                }
            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin is able to create the same treatment', async () => {
        const response = await server.post('/treatments')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                "id": config.treatmentTestRecordIds[0],
                "treatment": {
                    "index-name": "test-acupuncture",
                    "label-name": "Test_Acupuncture"
                },
                "body-system": {
                    "index-name": "neck-upper-back",
                    "label-name": "Neck and Upper Back"
                },
                "version": {
                    "mtus": "04/18/2019",
                    "odg": "12/17/2021",
                    "policy-doc": "05/01/2022",
                    "created-by": "Binu",
                    "comments": ""
                },
                "policies": [
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-chronic",
                            "label-name": "Cervicothoracic Pain - Chronic"
                        },
                        "recommendation": "1",
                        "questions": [
                            {
                                "#": "1",
                                "question": "Is Acupuncture requested in combination with a conditioning program of aerobic and strengthening exercises?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "2"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            },
                            {
                                "#": "2",
                                "question": "Is this request for less than 6 initial sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "3"
                                    }
                                ]
                            },
                            {
                                "#": "3",
                                "question": "Will this request make the total sessions more than 12?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "deny"
                                    },
                                    {
                                        "option": "no",
                                        "action": "4"
                                    }
                                ]
                            },
                            {
                                "#": "4",
                                "question": "Are there improvements in objective measures from previous Acupuncture sessions to justify additional sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            }
                        ],
                        "mtus-text": "An initial trial of 5 to 6 appointments is recommended in combination with a conditioning program of aerobic and strengthening exercises. Future appointments should be tied to improvements in objective measures to justify an additional 6 sessions, for a total of 12 sessions."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-acute",
                            "label-name": "Cervicothoracic Pain - Acute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-subacute",
                            "label-name": "Cervicothoracic Pain - Subacute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    }
                ],
                "references": {
                    "mtus-link": "https://app.mdguidelines.com/state-guidelines/ca-mtus/cervical-and-thoracic-spine/diagnostic-and-treatment-recommendations/cervicothoracic-pain/treatment-recommendations/allied-health",
                    "odg-link": "https://www.odgbymcg.com/odg-link?tags=%20Acupuncture%20for%20Pain&formfield_country=US&st=st&ltdplan=182&stdEndDateMax=364&thispage=treatment&formfield_physicaldemandlevel=All%20Classes&rtwoption=bp_target"
                }
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error).toBe("Treatment with name: Test_Acupuncture already exist");
    });

    test('Input validation for body-system field', async () => {
        const response = await server.post('/treatments')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                "id": config.treatmentTestRecordIds[0],
                "treatment": {
                    "index-name": "test-acupuncture",
                    "label-name": "Test_Acupuncture"
                },
                "body-system": {
                    "index-name": "neck-upper-back",
                    "label-name": " "
                },
                "version": {
                    "mtus": "04/18/2019",
                    "odg": "12/17/2021",
                    "policy-doc": "05/01/2022",
                    "created-by": "Binu",
                    "comments": ""
                },
                "policies": [
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-chronic",
                            "label-name": "Cervicothoracic Pain - Chronic"
                        },
                        "recommendation": "1",
                        "questions": [
                            {
                                "#": "1",
                                "question": "Is Acupuncture requested in combination with a conditioning program of aerobic and strengthening exercises?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "2"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            },
                            {
                                "#": "2",
                                "question": "Is this request for less than 6 initial sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "3"
                                    }
                                ]
                            },
                            {
                                "#": "3",
                                "question": "Will this request make the total sessions more than 12?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "deny"
                                    },
                                    {
                                        "option": "no",
                                        "action": "4"
                                    }
                                ]
                            },
                            {
                                "#": "4",
                                "question": "Are there improvements in objective measures from previous Acupuncture sessions to justify additional sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            }
                        ],
                        "mtus-text": "An initial trial of 5 to 6 appointments is recommended in combination with a conditioning program of aerobic and strengthening exercises. Future appointments should be tied to improvements in objective measures to justify an additional 6 sessions, for a total of 12 sessions."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-acute",
                            "label-name": "Cervicothoracic Pain - Acute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-subacute",
                            "label-name": "Cervicothoracic Pain - Subacute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    }
                ],
                "references": {
                    "mtus-link": "https://app.mdguidelines.com/state-guidelines/ca-mtus/cervical-and-thoracic-spine/diagnostic-and-treatment-recommendations/cervicothoracic-pain/treatment-recommendations/allied-health",
                    "odg-link": "https://www.odgbymcg.com/odg-link?tags=%20Acupuncture%20for%20Pain&formfield_country=US&st=st&ltdplan=182&stdEndDateMax=364&thispage=treatment&formfield_physicaldemandlevel=All%20Classes&rtwoption=bp_target"
                }
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error.body_system[0]).toBe("The body system field is required.");
    });

    test('Input validation for name field', async () => {
        const response = await server.post('/treatments')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                "id": config.treatmentTestRecordIds[0],
                "treatment": {
                    "index-name": "test-acupuncture",
                    "label-name": ""
                },
                "body-system": {
                    "index-name": "neck-upper-back",
                    "label-name": "Neck and Upper Back"
                },
                "version": {
                    "mtus": "04/18/2019",
                    "odg": "12/17/2021",
                    "policy-doc": "05/01/2022",
                    "created-by": "Binu",
                    "comments": ""
                },
                "policies": [
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-chronic",
                            "label-name": "Cervicothoracic Pain - Chronic"
                        },
                        "recommendation": "1",
                        "questions": [
                            {
                                "#": "1",
                                "question": "Is Acupuncture requested in combination with a conditioning program of aerobic and strengthening exercises?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "2"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            },
                            {
                                "#": "2",
                                "question": "Is this request for less than 6 initial sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "3"
                                    }
                                ]
                            },
                            {
                                "#": "3",
                                "question": "Will this request make the total sessions more than 12?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "deny"
                                    },
                                    {
                                        "option": "no",
                                        "action": "4"
                                    }
                                ]
                            },
                            {
                                "#": "4",
                                "question": "Are there improvements in objective measures from previous Acupuncture sessions to justify additional sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            }
                        ],
                        "mtus-text": "An initial trial of 5 to 6 appointments is recommended in combination with a conditioning program of aerobic and strengthening exercises. Future appointments should be tied to improvements in objective measures to justify an additional 6 sessions, for a total of 12 sessions."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-acute",
                            "label-name": "Cervicothoracic Pain - Acute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-subacute",
                            "label-name": "Cervicothoracic Pain - Subacute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    }
                ],
                "references": {
                    "mtus-link": "https://app.mdguidelines.com/state-guidelines/ca-mtus/cervical-and-thoracic-spine/diagnostic-and-treatment-recommendations/cervicothoracic-pain/treatment-recommendations/allied-health",
                    "odg-link": "https://www.odgbymcg.com/odg-link?tags=%20Acupuncture%20for%20Pain&formfield_country=US&st=st&ltdplan=182&stdEndDateMax=364&thispage=treatment&formfield_physicaldemandlevel=All%20Classes&rtwoption=bp_target"
                }
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error.name[0]).toBe("The name field is required.");
    });

    //manager login
    test('manager login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[5],
                password: 'Test@123'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the manager is able to create a treatment', async () => {
        const response = await server.post('/treatments')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                "id": config.treatmentTestRecordIds[0],
                "treatment": {
                    "index-name": "test-acupuncture",
                    "label-name": "Test_Acupuncture"
                },
                "body-system": {
                    "index-name": "neck-upper-back",
                    "label-name": "Neck and Upper Back"
                },
                "version": {
                    "mtus": "04/18/2019",
                    "odg": "12/17/2021",
                    "policy-doc": "05/01/2022",
                    "created-by": "Binu",
                    "comments": ""
                },
                "policies": [
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-chronic",
                            "label-name": "Cervicothoracic Pain - Chronic"
                        },
                        "recommendation": "1",
                        "questions": [
                            {
                                "#": "1",
                                "question": "Is Acupuncture requested in combination with a conditioning program of aerobic and strengthening exercises?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "2"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            },
                            {
                                "#": "2",
                                "question": "Is this request for less than 6 initial sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "3"
                                    }
                                ]
                            },
                            {
                                "#": "3",
                                "question": "Will this request make the total sessions more than 12?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "deny"
                                    },
                                    {
                                        "option": "no",
                                        "action": "4"
                                    }
                                ]
                            },
                            {
                                "#": "4",
                                "question": "Are there improvements in objective measures from previous Acupuncture sessions to justify additional sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            }
                        ],
                        "mtus-text": "An initial trial of 5 to 6 appointments is recommended in combination with a conditioning program of aerobic and strengthening exercises. Future appointments should be tied to improvements in objective measures to justify an additional 6 sessions, for a total of 12 sessions."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-acute",
                            "label-name": "Cervicothoracic Pain - Acute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-subacute",
                            "label-name": "Cervicothoracic Pain - Subacute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    }
                ],
                "references": {
                    "mtus-link": "https://app.mdguidelines.com/state-guidelines/ca-mtus/cervical-and-thoracic-spine/diagnostic-and-treatment-recommendations/cervicothoracic-pain/treatment-recommendations/allied-health",
                    "odg-link": "https://www.odgbymcg.com/odg-link?tags=%20Acupuncture%20for%20Pain&formfield_country=US&st=st&ltdplan=182&stdEndDateMax=364&thispage=treatment&formfield_physicaldemandlevel=All%20Classes&rtwoption=bp_target"
                }
            });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe('you don\'t have access');
    });

    //operator login
    test('Operator login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the operator is able to create a treatment', async () => {
        const response = await server.post('/treatments')
            .set(config.headers)
            .set('Authorization', process.env.authToken)
            .send({
                "id": config.treatmentTestRecordIds[0],
                "treatment": {
                    "index-name": "test-acupuncture",
                    "label-name": "Test_Acupuncture"
                },
                "body-system": {
                    "index-name": "neck-upper-back",
                    "label-name": "Neck and Upper Back"
                },
                "version": {
                    "mtus": "04/18/2019",
                    "odg": "12/17/2021",
                    "policy-doc": "05/01/2022",
                    "created-by": "Binu",
                    "comments": ""
                },
                "policies": [
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-chronic",
                            "label-name": "Cervicothoracic Pain - Chronic"
                        },
                        "recommendation": "1",
                        "questions": [
                            {
                                "#": "1",
                                "question": "Is Acupuncture requested in combination with a conditioning program of aerobic and strengthening exercises?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "2"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            },
                            {
                                "#": "2",
                                "question": "Is this request for less than 6 initial sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "3"
                                    }
                                ]
                            },
                            {
                                "#": "3",
                                "question": "Will this request make the total sessions more than 12?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "deny"
                                    },
                                    {
                                        "option": "no",
                                        "action": "4"
                                    }
                                ]
                            },
                            {
                                "#": "4",
                                "question": "Are there improvements in objective measures from previous Acupuncture sessions to justify additional sessions?",
                                "option-list": [
                                    {
                                        "option": "yes",
                                        "action": "approve"
                                    },
                                    {
                                        "option": "no",
                                        "action": "deny"
                                    }
                                ]
                            }
                        ],
                        "mtus-text": "An initial trial of 5 to 6 appointments is recommended in combination with a conditioning program of aerobic and strengthening exercises. Future appointments should be tied to improvements in objective measures to justify an additional 6 sessions, for a total of 12 sessions."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-acute",
                            "label-name": "Cervicothoracic Pain - Acute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    },
                    {
                        "source": "mtus",
                        "diagnosis": {
                            "index-name": "cervicothoracic-pain-subacute",
                            "label-name": "Cervicothoracic Pain - Subacute"
                        },
                        "recommendation": "deny",
                        "questions": [],
                        "mtus-text": "Routine use of acupuncture is not recommended for treatment of acute or subacute cervicothoracic pain or for acute radicular pain."
                    }
                ],
                "references": {
                    "mtus-link": "https://app.mdguidelines.com/state-guidelines/ca-mtus/cervical-and-thoracic-spine/diagnostic-and-treatment-recommendations/cervicothoracic-pain/treatment-recommendations/allied-health",
                    "odg-link": "https://www.odgbymcg.com/odg-link?tags=%20Acupuncture%20for%20Pain&formfield_country=US&st=st&ltdplan=182&stdEndDateMax=364&thispage=treatment&formfield_physicaldemandlevel=All%20Classes&rtwoption=bp_target"
                }
            });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe('you don\'t have access');
    });

    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

});
