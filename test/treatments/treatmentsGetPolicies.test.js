const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Treatment getPolicies validation', () => {

    //super admin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the super admin able to gteAll the policies', async () => {
        const response = await server.post(`/getPolicies`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                body_system: "neck-upper-back"
            })
        expect(response.statusCode).toBe(200);
    });

    //manager login
    test('Manager login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[2],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the super admin able to gteAll the policies', async () => {
        const response = await server.post(`/getPolicies`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                body_system: "neck-upper-back"
            })
        expect(response.statusCode).toBe(403);
    });

    //Operator login
    test('Operator login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the super admin able to gteAll the policies', async () => {
        const response = await server.post(`/getPolicies`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                body_system: "neck-upper-back"
            })
        expect(response.statusCode).toBe(403);
    });
});