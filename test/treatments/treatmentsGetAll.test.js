const request = require('supertest');
const { config } = require('.././global');
const server = request(config.server_url);

describe('Treatments getAll', () => {

    //super admin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the super admin is able to get all the treatments', async () => {
        const response = await server.get('/rest/treatments')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: "false"
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(200)
    });

    test('Verify if the tablecount is exposing while getting all the treatments', async () => {
        const response = await server.get('/rest/treatments')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: "false"
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200)
    });

    //manager login
    test('Manager login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[2],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the manager is able to get all the treatments', async () => {
        const response = await server.get('/rest/treatments')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: "false"
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(403)
    });

    //Operator login
    test('Operator login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the operator is able to get all the treatments', async () => {
        const response = await server.get('/rest/treatments')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 25,
                sort: "false"
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(403)
    });

});