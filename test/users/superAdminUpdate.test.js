const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('SuperAdmin update validation', () => {
    
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set(config.headers)
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
        process.env.is_admin = JSON.parse(response.text).user.is_admin;
    });
    
    test('Verify if super admin able to update super admin', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[1]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Super admin updating super admin',
                password: "Test@123",
                role: "superAdmin",
                is_admin: null
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Super admin updating super admin').toBe(JSON.parse(response.text).name);
        expect('superAdmin').toBe(JSON.parse(response.text).role);
        expect(null).toBe(JSON.parse(response.text).is_admin);
    });

    test('Verify if super admin able to update manager', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[2]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Super admin updating manager',
                password: "Test@123",
                role: "manager",
                is_admin: null
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Super admin updating manager').toBe(JSON.parse(response.text).name);
        expect('manager').toBe(JSON.parse(response.text).role);
        expect(null).toBe(JSON.parse(response.text).is_admin);

    });

    test('Verify if super admin able to update operator', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[3]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Super admin updating operator',
                password: "Test@123",
                role: "operator",
                is_admin: null
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Super admin updating operator').toBe(JSON.parse(response.text).name);
        expect('operator').toBe(JSON.parse(response.text).role);
        expect(null).toBe(JSON.parse(response.text).is_admin);
    });

    test('Verify if super admin able to update practice admin', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[4]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Super admin updating practice admin user',
                password: "Test@123",
                role: "physician",
                is_admin: true
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Super admin updating practice admin user').toBe(JSON.parse(response.text).name);
        expect('physician').toBe(JSON.parse(response.text).role);
        expect(true).toBe(JSON.parse(response.text).is_admin);
    });

    test('Verify if super admin able to update practice non-admin', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[5]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Super admin updating practice non-admin user',
                password: "Test@123",
                role: "staff",
                is_admin: false
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Super admin updating practice non-admin user').toBe(JSON.parse(response.text).name);
        expect('staff').toBe(JSON.parse(response.text).role);
        expect(false).toBe(JSON.parse(response.text).is_admin);
    });

    test('Manager login', async () => {
        const response = await server.post('/login')
            .set(config.headers)
            .send({
                email: config.superAdminEmailIds[2],
                password: 'Test@123'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
        process.env.is_admin = JSON.parse(response.text).user.is_admin;
    });

    test('Verify if manager able to update super admin', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[6]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Manager updating super admin',
                password: "Test@123",
                role: "superAdmin",
                is_admin: null
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Manager updating super admin').toBe(JSON.parse(response.text).name);
        expect('superAdmin').toBe(JSON.parse(response.text).role);
        expect(null).toBe(JSON.parse(response.text).is_admin);
    });

    test('Verify if manager able to update manager', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[7]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Manager updating manger',
                password: "Test@123",
                role: "manager",
                is_admin: null
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Manager updating manger').toBe(JSON.parse(response.text).name);
        expect('manager').toBe(JSON.parse(response.text).role);
        expect(null).toBe(JSON.parse(response.text).is_admin);
    });

    test('Verify if manager able to update operator', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[8]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Manager updating operator',
                password: "Test@123",
                role: "operator",
                is_admin: null
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Manager updating operator').toBe(JSON.parse(response.text).name);
        expect('operator').toBe(JSON.parse(response.text).role);
        expect(null).toBe(JSON.parse(response.text).is_admin);
    });

    test('Verify if manager able to update practice admin user', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[9]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Manager updating practice admin user',
                password: "Test@123",
                role: "physician",
                is_admin: true
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Manager updating practice admin user').toBe(JSON.parse(response.text).name);
        expect('physician').toBe(JSON.parse(response.text).role);
        expect(true).toBe(JSON.parse(response.text).is_admin);
    });

    test('Verify if manager able to update practice non-admin user', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[10]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'Manager updating practice non-admin user',
                password: "Test@123",
                role: "staff",
                is_admin: false
            }
        });
        expect(response.statusCode).toBe(200);

        expect('Manager updating practice non-admin user').toBe(JSON.parse(response.text).name);
        expect('staff').toBe(JSON.parse(response.text).role);
        expect(false).toBe(JSON.parse(response.text).is_admin);
    });

    test('Operator login', async () => {
        const response = await server.post('/login')
            .set(config.headers)
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test@123'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
        process.env.is_admin = JSON.parse(response.text).user.is_admin;
    });

    test('Verify if operator able to update super admin', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[6]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'manager updating super admin',
                password: "Test@123",
                role: "superAdmin",
                is_admin: null
            }
        });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe(`you don't have access`);
    });

    test('Verify if operator able to update manager', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[7]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'operator updating manager',
                password: "Test@123",
                role: "manager",
                is_admin: null
            }
        });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe(`you don't have access`);
    });

    test('Verify if operator able to update operator', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[8]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'operator updating operator',
                password: "Test@123",
                role: "operator",
                is_admin: null
            }
        });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe(`you don't have access`);
    });

    test('Verify if operator able to update practice admin user', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[9]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'operator updating practice admin user',
                password: "Test@123",
                role: "operator",
                is_admin: true
            }
        });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe(`you don't have access`);
    });

    test('Verify if operator able to update practice non-admin user', async () => {
        const response = await server.put(`/users/${config.superAdminUserIds[10]}`)
        .set(config.headers)
        .set('Authorization', process.env.authToken)
        .send({
            user: {
                name: 'operator updating practice non-admin user',
                password: "Test@123",
                role: "staff",
                is_admin: false
            }
        });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe(`you don't have access`);
    });

});
