
const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Common api validation', () => {
    let userId = ''
    test('Get Me API test case', async () => {
        const response = await server.get('/users/me')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
        userId = JSON.parse(response.text).id;

    });

    test('Session API test case', async () => {
        const response = await server.post('/rest/users/session')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: userId
            });
        expect(response.statusCode).toBe(200);
    });

    // users/updateTwoFactorStatus
    test('verify if two factor is enabled or not.', async () => {
        const response = await server.put('/users/updateTwoFactorStatus')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                two_factor_status: true
            });
        expect(response.statusCode).toBe(200);
    });

    test('Get Me API test case', async () => {
        const response = await server.get('/users/me')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).two_factor_status).toBe(true);
    });

    test('verify if two factor is disabled or not.', async () => {
        const response = await server.put('/users/updateTwoFactorStatus')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                two_factor_status: false
            });
        expect(response.statusCode).toBe(200);
    });

    test('Get Me API test case', async () => {
        const response = await server.get('/users/me')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).two_factor_status).toBe(false);
    });
});