
const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Super admin create validation', () => {
  //creating new superAdmin
  test('Super admin create', async () => {
    const response = await server.post('/rest/users/createTestSuperAdmin')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[0],
        name: 'Claim Made Super Admin',
        email: config.superAdminEmailIds[0],
        password: 'Test123$',
        role: 'superAdmin'
      });
    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.text).authToken).not.toBe('');
  });

  //Super admin login
  test('Super admin login', async () => {
    const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: config.superAdminEmailIds[0],
        password: 'Test123$'
      });
    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.text).authToken).not.toBe('');
    process.env.authToken = JSON.parse(response.text).authToken;
  });

  test('Verify if super admin able to create super admin', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[1],
        name: "Super admin creating super admin",
        email: config.superAdminEmailIds[1],
        password: "Test123$",
        role: "superAdmin"
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[1]).toBe(JSON.parse(response.text).id);
    expect('Super admin creating super admin').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[1]).toBe(JSON.parse(response.text).email);
    expect('superAdmin').toBe(JSON.parse(response.text).role);
  });

  test('Verify if super admin able to create manager', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[2],
        name: "Super admin creating manager",
        email: config.superAdminEmailIds[2],
        password: "Test123$",
        role: "manager"
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[2]).toBe(JSON.parse(response.text).id);
    expect('Super admin creating manager').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[2]).toBe(JSON.parse(response.text).email);
    expect('manager').toBe(JSON.parse(response.text).role);
  });

  test('Verify if super admin able to create operator', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[3],
        name: "Super admin creating operator",
        email: config.superAdminEmailIds[3],
        password: "Test123$",
        role: "operator"
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[3]).toBe(JSON.parse(response.text).id);
    expect('Super admin creating operator').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[3]).toBe(JSON.parse(response.text).email);
    expect('operator').toBe(JSON.parse(response.text).role);
  });

  test('Verify if super admin able to create practice admin user', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[4],
        name: "Super admin creating practice admin",
        email: config.superAdminEmailIds[10],
        password: "Test123$",
        role: "physician",
        practice_id: config.practice_id[0],
        is_admin: true
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[4]).toBe(JSON.parse(response.text).id);
    expect('Super admin creating practice admin').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[10]).toBe(JSON.parse(response.text).email);
    expect('physician').toBe(JSON.parse(response.text).role);
    expect(config.practice_id[0]).toBe(JSON.parse(response.text).practice_id);
    expect(true).toBe(JSON.parse(response.text).is_admin);
  });

  test('Verify if super admin able to create practice non-admin user', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[5],
        name: "Super admin creating practice non-admin",
        email: config.superAdminEmailIds[11],
        password: "Test123$",
        role: "staff",
        practice_id: config.practice_id[0],
        is_admin: false
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[5]).toBe(JSON.parse(response.text).id);
    expect('Super admin creating practice non-admin').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[11]).toBe(JSON.parse(response.text).email);
    expect('staff').toBe(JSON.parse(response.text).role);
    expect(config.practice_id[0]).toBe(JSON.parse(response.text).practice_id);
    expect(false).toBe(JSON.parse(response.text).is_admin);
  });

  //Manager login
  test('Verify manager able to login', async () => {
    const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: config.superAdminEmailIds[2],
        password: 'Test123$'
      });
    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.text).authToken).not.toBe('');
    process.env.authToken = JSON.parse(response.text).authToken;
  });

  test('Verify if manager able to create superAdmin', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[6],
        name: "Manager creating superAdmin",
        email: config.superAdminEmailIds[4],
        password: "Test123$",
        role: "superAdmin"
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[6]).toBe(JSON.parse(response.text).id);
    expect('Manager creating superAdmin').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[4]).toBe(JSON.parse(response.text).email);
    expect('superAdmin').toBe(JSON.parse(response.text).role);
  });

  test('Verify if manager able to create manager', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[7],
        name: "Manager creating manager",
        email: config.superAdminEmailIds[5],
        password: "Test123$",
        role: "manager"
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[7]).toBe(JSON.parse(response.text).id);
    expect('Manager creating manager').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[5]).toBe(JSON.parse(response.text).email);
    expect('manager').toBe(JSON.parse(response.text).role);
  });

  test('Verify if manager able to create operator', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[8],
        name: "Manager creating operator",
        email: config.superAdminEmailIds[6],
        password: "Test123$",
        role: "operator"
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[8]).toBe(JSON.parse(response.text).id);
    expect('Manager creating operator').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[6]).toBe(JSON.parse(response.text).email);
    expect('operator').toBe(JSON.parse(response.text).role);
  });

  test('Verify if manager able to create practice admin user', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[9],
        name: "Manager creating practice admin",
        email: config.superAdminEmailIds[12],
        password: "Test123$",
        role: "staff",
        practice_id: config.practice_id[0],
        is_admin: true
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[9]).toBe(JSON.parse(response.text).id);
    expect('Manager creating practice admin').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[12]).toBe(JSON.parse(response.text).email);
    expect('staff').toBe(JSON.parse(response.text).role);
    expect(config.practice_id[0]).toBe(JSON.parse(response.text).practice_id);
    expect(true).toBe(JSON.parse(response.text).is_admin);
  });

  test('Verify if manager able to create practice non-admin user', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[10],
        name: "Manager creating practice non-admin",
        email: config.superAdminEmailIds[13],
        password: "Test123$",
        role: "staff",
        practice_id: config.practice_id[0],
        is_admin: true
      });
    expect(response.statusCode).toBe(200);

    expect(config.superAdminUserIds[10]).toBe(JSON.parse(response.text).id);
    expect('Manager creating practice non-admin').toBe(JSON.parse(response.text).name);
    expect(config.superAdminEmailIds[13]).toBe(JSON.parse(response.text).email);
    expect('staff').toBe(JSON.parse(response.text).role);
    expect(config.practice_id[0]).toBe(JSON.parse(response.text).practice_id);
    expect(true).toBe(JSON.parse(response.text).is_admin);
  });

  //Operator login
  test('Verify if operator able to login', async () => {
    const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: config.superAdminEmailIds[3],
        password: 'Test123$'
      });
    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.text).authToken).not.toBe('');
    process.env.authToken = JSON.parse(response.text).authToken;
  });

  test('Verify if operator is not able to create super admin', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[11],
        name: "Operator creating superAdmin",
        email: config.superAdminEmailIds[7],
        password: "Test123$",
        role: "superAdmin"
      });
    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.text).error).toBe('You Don\'t have Permission to create this user.');
  });

  test('Verify if operator is not able to create manager', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[12],
        name: "Operator creating manager",
        email: config.superAdminEmailIds[8],
        password: "Test123$",
        role: "manager"
      });
    expect(response.statusCode).toBe(403);
    expect(JSON.parse(response.text).error).toBe('you don\'t have access');
  });

  test('Verify if operator is not able to create operator', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[13],
        name: "Operator creating operator",
        email: config.superAdminEmailIds[9],
        password: "Test123$",
        role: "operator"
      });
    expect(response.statusCode).toBe(403);
    expect(JSON.parse(response.text).error).toBe('you don\'t have access');
  });

  //operator trying to create user
  test('Verify if operator is not able to create practice admin user', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[14],
        name: "Operator creating practice admin",
        email: config.superAdminEmailIds[14],
        password: "Test123$",
        role: "staff",
        practice_id: config.practice_id[0],
        is_admin: true,
        notification: false
      });
    expect(response.statusCode).toBe(403);
    expect(JSON.parse(response.text).error).toBe('you don\'t have access');
  });

  test('Verify if operator is not able to create practice non-admin user', async () => {
    const response = await server.post('/rest/users')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .set('Authorization', process.env.authToken)
      .send({
        id: config.superAdminUserIds[15],
        name: "Operator creating practice admin",
        email: config.superAdminEmailIds[15],
        password: "Test123$",
        role: "staff",
        practice_id: config.practice_id[0],
        is_admin: true,
        notification: false
      });
    expect(response.statusCode).toBe(403);
    expect(JSON.parse(response.text).error).toBe('you don\'t have access');
  });

  test('Back to practice user login', async () => {
    const response = await server.post('/login')
      .set('content-type', 'application/json')
      .set('Accept', 'application/json')
      .send({
        email: config.testEmail,
        password: 'Test123$'
      });
    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.text).authToken).not.toBe('');
    process.env.authToken = JSON.parse(response.text).authToken;
    process.env.is_admin = JSON.parse(response.text).user.is_admin;
  });

});
