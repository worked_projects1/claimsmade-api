process.env.viaAll = true;
require('./signup.test');
require('./login.test');
require('./userGetMe.test');
require('./userCreate.test');
require('./userUpdate.test');
require('./userChangePassword.test');
require('./userGetAll.test');
require('./superAdminCreate.test')
require('./superAdminUpdate.test');
require('./superAdminGetAll.test');
require('./superAdminDelete.test');
require('./userDelete.test');
