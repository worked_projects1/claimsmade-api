const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Super admin delete validation', () => {
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if super admin able to delete super admin', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[1]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to delete manager', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[2]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to delete operator', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[3]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to delete practice admin user', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[4]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to delete practice non-admin user', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[5]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    //super admin creating manager
    test('Creating manager for login', async () => {
        const response = await server.post('/rest/users')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.superAdminUserIds[2],
                name: "Super admin creating manager",
                email: config.superAdminEmailIds[2],
                password: "Test123$",
                role: "manager"
            });
        expect(response.statusCode).toBe(200);
    });

    //manager login
    test('manager login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[5],
                password: 'Test@123'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if manager able to delete super admin', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[6]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    test('Verify if manager able to delete manager', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[7]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(403);
        expect(JSON.parse(response.text).error).toBe('forbidden');
    });

    test('Verify if manager able to delete operator', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[8]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    //create operator for login
    test('Creating operator for login', async () => {
        const response = await server.post('/rest/users')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.superAdminUserIds[3],
                name: "Super admin creating operator",
                email: config.superAdminEmailIds[3],
                password: "Test123$",
                role: "operator"
            });
        expect(response.statusCode).toBe(200);
    });

    //operator login
    test('Operator login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });


    test('Verify if operator is not able to delete super admin', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[7]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(403);
    });

    test('Verify if operator is not able to delete manager', async () => {
        const response = await server.delete(`/users/${config.superAdminUserIds[8]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(403);
    });

    test('Back to practice user login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.testEmail,
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
        process.env.is_admin = JSON.parse(response.text).user.is_admin;
    });

});