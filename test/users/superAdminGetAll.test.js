const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('SuperAdmin getAll validation', () => {

    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
        process.env.is_admin = JSON.parse(response.text).user.is_admin;
    });

    test('Verify if super admin able to get all the admins', async () => {
        const response = await server.get('/rest/users?type=admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 1,
                limit: 10,
                sort: "false"
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to get all the admins by limit', async () => {
        const response = await server.get('/rest/users?type=admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 1,
                limit: 5,
                sort: "false"
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to get all the practice users', async () => {
        const response = await server.get('/rest/users?type=practice')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 1,
                limit: 10,
                sort: "false"
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    test('Verify if sorting is working while super admin get the practice admin', async () => {
        const response = await server.get('/rest/users?type=admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 1,
                limit: 10,
                sort: JSON.stringify({ column: "Users.is_admin", type: "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    test('Verify if totalPageCount is exposing in the response header for admin type', async () => {
        const response = await server.get('/rest/users?type=admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ column: "Users.is_admin", type: "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        // expect(String(response.header.totalpagecount)).toEqual(JSON.parse(response.text).length.toString());
    });

    test('Verify if offset is working or not for admin type', async () => {
        const response = await server.get('/rest/users?type=admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 1,
                limit: 100,
                sort: JSON.stringify({ column: "Users.is_admin", type: "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        // expect(response.header.totalpagecount - 1).toEqual(JSON.parse(response.text).length);
    });

    test('Verify if super admin able to get the limit of practice users', async () => {
        const response = await server.get('/rest/users?type=practice')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 1,
                limit: 2,
                sort: "false"
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(2);
    });

    test('Verify if sorting is working while super admin get the practice users', async () => {
        const response = await server.get('/rest/users?type=practice')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 1,
                limit: 2,
                sort: JSON.stringify({ column: "Users.is_admin", type: "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    test('Verify if totalPageCount is exposing in the response header', async () => {
        const response = await server.get('/rest/users?type=practice')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ column: "Users.is_admin", type: "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        // expect(String(response.header.totalpagecount)).toEqual(JSON.parse(response.text).length.toString());
    });

    test('Verify if offset is working or not', async () => {
        const response = await server.get('/rest/users?type=practice')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 1,
                limit: 100,
                sort: JSON.stringify({ column: "Users.is_admin", type: "ASC" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        // expect(response.header.totalpagecount - 1).toEqual(JSON.parse(response.text).length);
    });

    test('Back to practice user login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.testEmail,
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
        process.env.is_admin = JSON.parse(response.text).user.is_admin;
    });



});
