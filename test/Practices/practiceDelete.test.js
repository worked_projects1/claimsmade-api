const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Practice delete validation', () => {
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the super admin able to delete practice', async () => {
        const response = await server.delete(`/practices/${config.practice_id[2]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    //operator login
    test('Operator login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });


    test('Verify if the operator is not able to delete practice', async () => {
        const response = await server.delete(`/practices/${config.practice_id[4]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });

    //manager login
    test('manager login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[5],
                password: 'Test@123'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if the manager able to delete practice', async () => {
        const response = await server.delete(`/practices/${config.practice_id[3]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({

            });
        expect(response.statusCode).toBe(200);
    });


    test('Back to practice user login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.testEmail,
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
        process.env.is_admin = JSON.parse(response.text).user.is_admin;
    });

});