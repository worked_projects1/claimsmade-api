const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Practice getAll validation', () => {
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if super admin able to get all the practices', async () => {
        const response = await server.get('/rest/practices')
            .set(config.headers)
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ column: 'Practices.name', type: 'DESC' })
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to get all the practices by offset', async () => {
        const response = await server.get('/rest/practices')
            .set(config.headers)
            .set({
                offset: 1,
                limit: 100,
                sort: JSON.stringify({ column: 'Practices.name', type: 'DESC' })
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(200);
    });

    // sorting

    test('Verify if super admin able to get all the practices sort by created_at', async () => {
        const response = await server.get('/rest/practices')
            .set(config.headers)
            .set({
                offset: 0,
                limit: 25,
                sort: JSON.stringify({ column: 'created_at', type: 'DESC' })
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(200);
    });

    test('Verify if super admin able to get all the practices sort by city', async () => {
        const response = await server.get('/rest/practices')
            .set(config.headers)
            .set({
                offset: 0,
                limit: 25,
                sort: JSON.stringify({ column: 'city', type: 'DESC' })
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(200);
    });


    test('Verify if super admin able to get all the practices sort by state', async () => {
        const response = await server.get('/rest/practices')
            .set(config.headers)
            .set({
                offset: 0,
                limit: 25,
                sort: JSON.stringify({ column: 'state', type: 'DESC' })
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(200);
    });

    //manager login
    test('Verify manager able to login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[2],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if manager able to get all the practices', async () => {
        const response = await server.get('/rest/practices')
            .set(config.headers)
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ column: 'Practices.name', type: 'DESC' })
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(200);
    });

    //Operator login
    test('Verify if operator able to login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[3],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if operator able to get all the practices', async () => {
        const response = await server.get('/rest/practices')
            .set(config.headers)
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ column: 'Practices.name', type: 'DESC' })
            })
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(200);
    });

    test('Verify if operator able to get response when the limit is 0', async () => {
        const response = await server.get('/rest/practices')
            .set(config.headers)
            .set({
                offset: 0,
                limit: 0,
                sort: JSON.stringify({ column: 'Practices.name', type: 'DESC' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(0);
    });

});