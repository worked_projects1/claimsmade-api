const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Orders validation', () => {

    // create
    test('Verify if able to create orders', async () => {
        const inputJson = {
            id: config.order_ids[0],
            status: "completed",
            activity_id: "test_activity_id",
            activity_type: "RFA_FORM_DOC",
            amount_charged: 0,
            plan_type: "tek_as_you_go",
            patient_name: "rfa_test_record",
            claim_number: 12,
            employer: "John",
            claims_admin_name: "madhan",
            claims_admin_id: "claims_admin_id"
        }
        const response = await server.post('/rest/orders')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send(inputJson)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).id).toBe(inputJson.id);
        expect(JSON.parse(response.text).status).toBe(inputJson.status);
        expect(JSON.parse(response.text).activity_id).toBe(inputJson.activity_id);
        expect(JSON.parse(response.text).amount_charged).toBe(inputJson.amount_charged);
        expect(JSON.parse(response.text).plan_type).toBe(inputJson.plan_type);
        expect(JSON.parse(response.text).patient_name).toBe(inputJson.patient_name);
        expect(JSON.parse(response.text).claim_number).toBe(inputJson.claim_number);
        expect(JSON.parse(response.text).employer).toBe(inputJson.employer);
        expect(JSON.parse(response.text).claims_admin_name).toBe(inputJson.claims_admin_name);
        expect(JSON.parse(response.text).claims_admin_id).toBe(inputJson.claims_admin_id);
    });

    // getOne
    test('Verify if able to read order', async () => {
        const inputJson = {
            id: config.order_ids[0],
            status: "completed",
            activity_id: "test_activity_id",
            activity_type: "RFA_FORM_DOC",
            amount_charged: 0,
            plan_type: "tek_as_you_go",
            patient_name: "rfa_test_record",
            claim_number: "12",
            employer: "John",
            claims_admin_name: "madhan",
            claims_admin_id: "claims_admin_id"
        }
        const response = await server.get(`/rest/orders/${config.order_ids[0]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)

        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).id).toBe(inputJson.id);
        expect(JSON.parse(response.text).status).toBe(inputJson.status);
        expect(JSON.parse(response.text).activity_id).toBe(inputJson.activity_id);
        expect(JSON.parse(response.text).amount_charged).toBe(inputJson.amount_charged);
        expect(JSON.parse(response.text).plan_type).toBe(inputJson.plan_type);
        expect(JSON.parse(response.text).patient_name).toBe(inputJson.patient_name);
        expect(JSON.parse(response.text).claim_number).toBe(inputJson.claim_number);
        expect(JSON.parse(response.text).employer).toBe(inputJson.employer);
        expect(JSON.parse(response.text).claims_admin_name).toBe(inputJson.claims_admin_name);
        expect(JSON.parse(response.text).claims_admin_id).toBe(inputJson.claims_admin_id);
    });

    // update
    test('Verify if able to update order', async () => {
        const inputJson = {
            id: config.order_ids[0],
            status: "updated",
            activity_id: "test_activity_id_update",
            activity_type: "RFA_FORM_DOC_UPDATED",
            amount_charged: 0,
            plan_type: "tek_as_you_go_updated",
            patient_name: "rfa_test_record_updated",
            claim_number: "12",
            employer: "John_updated",
            claims_admin_name: "madhan_updated",
            claims_admin_id: "claims_admin_id_updated"
        }
        const response = await server.put(`/rest/orders/${config.order_ids[0]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send(inputJson)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).id).toBe(inputJson.id);
        expect(JSON.parse(response.text).status).toBe(inputJson.status);
        expect(JSON.parse(response.text).activity_id).toBe(inputJson.activity_id);
        expect(JSON.parse(response.text).amount_charged).toBe(inputJson.amount_charged);
        expect(JSON.parse(response.text).plan_type).toBe(inputJson.plan_type);
        expect(JSON.parse(response.text).patient_name).toBe(inputJson.patient_name);
        expect(JSON.parse(response.text).claim_number).toBe(inputJson.claim_number);
        expect(JSON.parse(response.text).employer).toBe(inputJson.employer);
        expect(JSON.parse(response.text).claims_admin_name).toBe(inputJson.claims_admin_name);
        expect(JSON.parse(response.text).claims_admin_id).toBe(inputJson.claims_admin_id);
    });

    // getAll
    test('Verify if able to read order', async () => {
        const response = await server.get(`/rest/orders`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: false,
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    // getAll sort by plan_type
    test('Verify if able to get the orders by sorting plan_type column', async () => {
        const response = await server.get(`/rest/orders`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "plan_type", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    // getAll sort by user_name
    test('Verify if able to get the orders by sorting user_name column', async () => {
        const response = await server.get(`/rest/orders`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "user_name", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    // getAll sort by claim_number
    test('Verify if able to get the orders by sorting claim_number column', async () => {
        const response = await server.get(`/rest/orders`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "claim_number", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    // getAll sort by order_date
    test('Verify if able to get the orders by sorting order_date column', async () => {
        const response = await server.get(`/rest/orders`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "order_date", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    // getAll sort by amount_charged
    test('Verify if able to get the orders by sorting amount_charged column', async () => {
        const response = await server.get(`/rest/orders`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "amount_charged", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });

    // superAdmin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    // adminGetAll validation
    test('Verify if admin able to get the orders', async () => {
        const response = await server.get(`/rest/orders/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: false,
                search: false,
                filter: JSON.stringify({ practice_name: 'all' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAllAdmin order_date sorting
    test('Verify if admin able to get the orders by sorting order_date column', async () => {
        const response = await server.get(`/rest/orders/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "order_date", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ practice_name: 'all' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAllAdmin plan_type sorting
    test('Verify if admin able to get the orders by sorting plan_type column', async () => {
        const response = await server.get(`/rest/orders/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "plan_type", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ practice_name: 'all' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAllAdmin practice_name sorting
    test('Verify if admin able to get the orders by sorting practice_name column', async () => {
        const response = await server.get(`/rest/orders/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "practice_name", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ practice_name: 'all' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAllAdmin claim_number sorting
    test('Verify if admin able to get the orders by sorting claim_number column', async () => {
        const response = await server.get(`/rest/orders/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "claim_number", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ practice_name: 'all' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });


    // getAllAdmin user_name sorting
    test('Verify if admin able to get the orders by sorting user_name column', async () => {
        const response = await server.get(`/rest/orders/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "user_name", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ practice_name: 'all' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAllAdmin amount_charged sorting
    test('Verify if admin able to get the orders by sorting amount_charged column', async () => {
        const response = await server.get(`/rest/orders/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "amount_charged", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ practice_name: 'all' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // practice name: Test Practice1
    test('Verify if admin able to get the orders by using filter', async () => {
        const response = await server.get(`/rest/orders/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "amount_charged", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ practice_name: 'Test Practice1' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).length).toBe(1);
    });
});