const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Subscription getAll validation', () => {

    //Practice admin user login
    test('Physicain user login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.testEmail,
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    // getAll
    test('Verify if user able to read subscription histories', async () => {
        const response = await server.get(`/rest/subscription/getAllHistories`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: false,
                search: false
                // filter: JSON.stringify({ practice_name: 'all' })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAll sort by user name
    test('Verify if user able to read subscription histories sort by user name', async () => {
        const response = await server.get(`/rest/subscription/getAllHistories`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "name", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAll sort by price
    test('Verify if user able to read subscription histories sort by price', async () => {
        const response = await server.get(`/rest/subscription/getAllHistories`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "price", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAll sort by order_date
    test('Verify if user able to read subscription histories sort by order_date', async () => {
        const response = await server.get(`/rest/subscription/getAllHistories`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "order_date", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAll sort by subscription_valid_till
    test('Verify if user able to read subscription histories sort by subscription_valid_till', async () => {
        const response = await server.get(`/rest/subscription/getAllHistories`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "subscription_valid_till", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAll sort by payment_type
    test('Verify if user able to read subscription histories sort by payment_type', async () => {
        const response = await server.get(`/rest/subscription/getAllHistories`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "payment_type", "type": "ASC" }),
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });


    // adminGetAll
    test('Verify if admin able to read subscription histories', async () => {
        const response = await server.get(`/rest/subscription/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: false,
                search: false,
                filter: JSON.stringify({ "practice_name": "all", "payment_type": "all" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // adminGetAll sort by practice_name
    test('Verify if admin able to read subscription histories sort by practice_anme', async () => {
        const response = await server.get(`/rest/subscription/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "practice_name", "type": "DESC" }),
                search: false,
                filter: JSON.stringify({ "practice_name": "all", "payment_type": "all" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // adminGetAll sort by user_name
    test('Verify if admin able to read subscription histories sort by user_name', async () => {
        const response = await server.get(`/rest/subscription/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "user_name", "type": "DESC" }),
                search: false,
                filter: JSON.stringify({ "practice_name": "all", "payment_type": "all" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // adminGetAll sort by price
    test('Verify if admin able to read subscription histories sort by price', async () => {
        const response = await server.get(`/rest/subscription/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "price", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ "practice_name": "all", "payment_type": "all" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // adminGetAll sort by order_date
    test('Verify if admin able to read subscription histories sort by order_date', async () => {
        const response = await server.get(`/rest/subscription/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "order_date", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ "practice_name": "all", "payment_type": "all" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // adminGetAll sort by subscription_valid_till
    test('Verify if admin able to read subscription histories sort by subscription_valid_till', async () => {
        const response = await server.get(`/rest/subscription/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "subscription_valid_till", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ "practice_name": "all", "payment_type": "all" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // adminGetAll sort by payment_type
    test('Verify if admin able to read subscription histories sort by payment_type', async () => {
        const response = await server.get(`/rest/subscription/adminGetAll`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: JSON.stringify({ "column": "payment_type", "type": "ASC" }),
                search: false,
                filter: JSON.stringify({ "practice_name": "all", "payment_type": "all" })
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

    // getAll subscription
    test('Verify if admin able to read subscription histories sort by payment_type', async () => {
        const response = await server.get(`/rest/subscriptions`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set({
                offset: 0,
                limit: 100,
                sort: false,
                search: false
            })
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
    });

});