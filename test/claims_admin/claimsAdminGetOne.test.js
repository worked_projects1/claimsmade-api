const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Claims Admin getOne validation', () => {

    //superAdmin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if super admin able to get the claims admins', async () => {
        const response = await server.get(`/claims_admin/${config.claims_admin_ids[0]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).id).toBe('claims_admin_id1')
        expect(JSON.parse(response.text).company_name).toBe('Not ONe')
        expect(JSON.parse(response.text).email).toBe(config.claims_admin_email_ids[0])
        expect(JSON.parse(response.text).address).toBe("Address check")
        expect(JSON.parse(response.text).city).toBe("LA")
        expect(JSON.parse(response.text).state).toBe("CA")
        expect(JSON.parse(response.text).zip_code).toBe("52134")
        expect(JSON.parse(response.text).phone_number).toBe("9876543218")
        expect(JSON.parse(response.text).fax_number).toBe("56321456789")
    });

    test('Verify if super admin able to get the claims admin with invalid id', async () => {
        const response = await server.get(`/claims_admin/9gd0f9g-6tn6l402-u0g4jg4589`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
        expect(response.statusCode).toBe(404);
    });

});