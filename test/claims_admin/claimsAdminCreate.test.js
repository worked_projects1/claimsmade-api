const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Claims Admin Create Validation', () => {

    //superAdmin login
    test('Super admin login', async () => {
        const response = await server.post('/login')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .send({
                email: config.superAdminEmailIds[0],
                password: 'Test123$'
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).authToken).not.toBe('');
        process.env.authToken = JSON.parse(response.text).authToken;
    });

    test('Verify if super admin can create Claims Admin user.', async () => {
        const response = await server.post('/rest/claims_admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.claims_admin_ids[0],
                company_name: "Not ONe",
                email: config.claims_admin_email_ids[0],
                address: "Address check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).id).toBe('claims_admin_id1')
        expect(JSON.parse(response.text).company_name).toBe('Not ONe')
        expect(JSON.parse(response.text).email).toBe(config.claims_admin_email_ids[0])
        expect(JSON.parse(response.text).address).toBe("Address check")
        expect(JSON.parse(response.text).city).toBe("LA")
        expect(JSON.parse(response.text).state).toBe("CA")
        expect(JSON.parse(response.text).zip_code).toBe("52134")
        expect(JSON.parse(response.text).phone_number).toBe("9876543218")
        expect(JSON.parse(response.text).fax_number).toBe("56321456789")
    });

    test('Verify if super admin could not create Claims Admin (Same Email).', async () => {
        const response = await server.post('/rest/claims_admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.claims_admin_ids[1],
                email: config.claims_admin_email_ids[0],
                company_name: "Not ONe",
                address: "Address check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error).toBe(`Claims Administarator with Email: ${config.claims_admin_email_ids[0]} is already exists`)
    });

    test('Verify if super admin could not create Claims Admin (Same Company name).', async () => {
        const response = await server.post('/rest/claims_admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.claims_admin_ids[2],
                email: config.claims_admin_email_ids[1],
                company_name: "Not ONe",
                address: "Address check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error).toBe(`Claims Administarator with Name: Not ONe is already exists`)
    });

    test('Verify if super admin able create Claims Admin with only mandatory fields.', async () => {
        const response = await server.post('/rest/claims_admin')
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.claims_admin_ids[3],
                email: config.claims_admin_email_ids[2],
                company_name: "Not ONe 01",
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).id).toBe(config.claims_admin_ids[3])
        expect(JSON.parse(response.text).email).toBe(config.claims_admin_email_ids[2])
        expect(JSON.parse(response.text).company_name).toBe('Not ONe 01')
    });

});