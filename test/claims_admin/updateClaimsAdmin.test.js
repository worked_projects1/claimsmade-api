const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Claims Admin Update Validation', () => {
    test('Verify if super admin can update Claims Admin user.', async () => {
        const response = await server.put(`/claims_admin/${config.claims_admin_ids[0]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.claims_admin_ids[0],
                company_name: "Not ONe",
                email: config.claims_admin_email_ids[0],
                address: "Updated check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).id).toBe(config.claims_admin_ids[0])
        expect(JSON.parse(response.text).company_name).toBe('Not ONe')
        expect(JSON.parse(response.text).email).toBe(config.claims_admin_email_ids[0])
        expect(JSON.parse(response.text).address).toBe("Updated check")
        expect(JSON.parse(response.text).city).toBe("LA")
        expect(JSON.parse(response.text).state).toBe("CA")
        expect(JSON.parse(response.text).zip_code).toBe("52134")
        expect(JSON.parse(response.text).phone_number).toBe("9876543218")
        expect(JSON.parse(response.text).fax_number).toBe("56321456789")
    });

    test('Verify if user not able to update the existing name.', async () => {
        const response = await server.put(`/claims_admin/${config.claims_admin_ids[3]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                company_name: "Not ONe",
                email: config.claims_admin_email_ids[0],
                address: "Updated check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error).toBe("Claims Administarator with Name: Not ONe already exist.");
    });

    test('Verify if user not able to update the existing email.', async () => {
        const response = await server.put(`/claims_admin/${config.claims_admin_ids[3]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                id: config.claims_admin_ids[0],
                company_name: "Not ONe1",
                email: config.claims_admin_email_ids[0],
                address: "Updated check",
                city: "LA",
                state: "CA",
                zip_code: "52134",
                phone_number: "9876543218",
                fax_number: "56321456789"
            });
        expect(response.statusCode).toBe(400);
        expect(JSON.parse(response.text).error).toBe(`Claims Administarator with Email: ${config.claims_admin_email_ids[0]} already exist.`);
    });

    test('Verify if user able to update the mandatory fields.', async () => {
        const response = await server.put(`/claims_admin/${config.claims_admin_ids[3]}`)
            .set('content-type', 'application/json')
            .set('Accept', 'application/json')
            .set('Authorization', process.env.authToken)
            .send({
                company_name: "Not ONe updated",
                email: config.claims_admin_email_ids[1],
            });
        expect(response.statusCode).toBe(200);
        expect(JSON.parse(response.text).id).toBe(config.claims_admin_ids[3])
        expect(JSON.parse(response.text).company_name).toBe('Not ONe updated')
        expect(JSON.parse(response.text).email).toBe(config.claims_admin_email_ids[1])
    });

});