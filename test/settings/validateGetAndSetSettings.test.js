const request = require('supertest');
const { config } = require('../global');
const server = request(config.server_url);

describe('Validate get and set settings', () => {
    
    if (!process.env.viaAll) {
        test('Login', async () => {
            const response = await server.post('/login')
                .set(config.headers)
                .send({
                    email: config.testEmail,
                    password: 'Test@123'
                });
            
            expect(response.statusCode).toBe(200);
            expect(JSON.parse(response.text).authToken).not.toBe('');
            process.env.authToken = JSON.parse(response.text).authToken;
            process.env.is_admin = JSON.parse(response.text).user.is_admin;
        });
    }
    
    test('Verify if user able to get settings', async () => {
        const response = await server.get('/users/getSettings')
                .set(config.headers)
                .set('Authorization', process.env.authToken)
                .send({
                    
                });
            expect(response.statusCode).toBe(200);
    });

    test('Verify if user able to set settings', async () => {
        const sessionTimeout = 3201;
        const response = await server.put('/users/setSettings')
                .set(config.headers)
                .set('Authorization', process.env.authToken)
                .send({
                        session_timeout: sessionTimeout
                });
            expect(response.statusCode).toBe(200);
            expect(JSON.parse(response.text).session_timeout).toEqual(sessionTimeout);
    });
});