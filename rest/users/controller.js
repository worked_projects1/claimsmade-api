const uuid = require('uuid');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const authMiddleware = require('../../auth');
const jwt = require('jsonwebtoken');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { validateSignup, validateUpdateUser, validatePracticeUser, validateCreateUser, validateResetPassword, validateIsAdmin, validateForgetPassword, validateSignature, validateTwofactor, validateUpdateAuth } = require('./validation');
const { authorizeUpdate, authorizeCreate, authorizeDelete, authorizeGetAllUsersByRole, authorizeUpdateAuth } = require('./authorize');
const { QueryTypes } = require('sequelize');
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);
const CryptoJS = require("crypto-js");
const { sendEmail } = require('../../utils/mailModule');
const { appConstants } = require('../../appConstants');

const signUp = async (event) => {
  try {
    const { Users, Op, PasswordHistory, Practices } = await connectToDatabase();
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

    const dataObject = Object.assign(input, {
      id: input.user_id || uuid.v4(),
    });

    validateSignup(dataObject);

    const userObject = await Users.findOne({
      where: {
        email: dataObject.email,
        is_deleted: { [Op.not]: true }
      },
    });

    if (userObject) throw new HTTPError(400, `User with email: ${dataObject.email} already exist`);

    const practiceObject = await Practices.findOne({
      where: {
        name: dataObject.practice_name,
        is_deleted: { [Op.not]: true }
      },
    });

    if (practiceObject) throw new HTTPError(400, `Practice with name: ${dataObject.practice_name} already exist`);

    const practice = await Practices.create({
      id: input.practice_id || uuid.v4(),
      name: input.practice_name,
      contact_name: input.contact_name ? input.contact_name : null
    });

    const user = await Users.create({
      id: input.id || uuid.v4(),
      name: input.name,
      email: input.email,
      password: input.password,
      practice_id: practice.id,
      role: appConstants.USER_ROLES[0],
      specialty: input.specialty,
      is_admin: true,
      temporary_password: false
    });

    user.created_by = user.id;
    await user.save();

    practice.created_by = user.id;
    await practice.save();

    await PasswordHistory.create({ email: dataObject.email, password: user.password, id: uuid.v4() });

    const tokenUser = {
      id: user.id,
      role: user.role,
    };
    const token = jwt.sign(
      tokenUser,
      process.env.JWT_SECRET,
      {
        expiresIn: process.env.JWT_EXPIRATION_TIME,
      }
    );

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        authToken: `JWT ${token}`,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not create the user.' }),
    };
  }
};


const update = async (event) => {
  try {
    const params = event.params || event.pathParameters;
    const input = typeof event.body == "string" ? JSON.parse(event.body) : event.body;
    const loggedInUser = event.user;
    const user = input.user;
    const userId = params.id;
    const { Users, PasswordHistory, Op } = await connectToDatabase();

    authorizeUpdate(loggedInUser);
    validateUpdateUser(user);
    const userObject = await Users.findOne({ where: { id: userId, is_deleted: { [Op.not]: true } }, });
    if (!userObject) throw new HTTPError(400, `User not found to update`);

    if (input.user.password) {
      if (await userObject.checkIfLast6Password(input.user.password, PasswordHistory)) throw new HTTPError(401, 'Can not used any last 6 past password');
      input.user.password = await userObject.generateNewPassword(input.user.password);
    } else {
      input.user.password = userObject.password;
    }
    if (user.role && 'staff' == user.role.toLowerCase()
      && (user.specialty && user.specialty != "")) delete user.specialty;
    const updatedUser = Object.assign(userObject, user);
    const result = await updatedUser.save();
    await PasswordHistory.create({ email: result.email, password: result.password, id: uuid.v4() });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(result),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not update the user.' }),
    };
  }
};


const destroy = async (event) => {
  try {
    const params = event.params || event.pathParameters;
    const { Users, Op } = await connectToDatabase();
    const logginedUser = event.user;
    authorizeDelete(logginedUser);
    if (logginedUser.id === params.id) throw new HTTPError(403, `forbidden`);
    const user = await Users.destroy({ where: { id: params.id } });
    if (user < 1) throw new HTTPError(404, 'User not found');

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify("User removed"),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not delete the user.' }),
    };
  }
}

const create = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

    const claimsMadeRoles = ['superAdmin', 'manager', 'operator'];
    const majorUsers = ['superAdmin', 'manager'];
    const newUser = input.role;
    if (newUser == 'superAdmin') {
      if (!majorUsers.includes(event.user.role)) {
        throw new HTTPError(400, `You Don't have Permission to create this user.`);
      }
    }
    if ('staff' == newUser.toLowerCase()
      && (input.specialty && input.specialty != "")) delete input.specialty;
    if (!claimsMadeRoles.includes(event.user.role)) {
      if (event.user.practice_id) {
        input.practice_id = event.user.practice_id;
      }
    }
    const practiceRoles = ["physician", "staff"];
    if (majorUsers.includes(event.user.role) && practiceRoles.includes(newUser)) {
      validatePracticeUser(event.user, input);
    } else if (practiceRoles.includes(event.user.role)) {
      validateIsAdmin(input);
      if (!practiceRoles.includes(newUser)) throw new HTTPError(400, `Invalid role`)
    }
    authorizeCreate(event.user);
    const emailNotification = input.notification;
    const accountType = input.userCreatedIn;

    let temp_password;
    if (emailNotification) {
      temp_password = true;
    } else {
      temp_password = false;
    }
    const dataObject = Object.assign(input, {
      id: input.id || uuid.v4(), created_by: event.user.id
    }, { temporary_password: temp_password });

    validateCreateUser(dataObject);
    const { Users, Practices, PasswordHistory, Op } = await connectToDatabase();
    const userObject = await Users.findOne({
      where: {
        email: dataObject.email,
        is_deleted: { [Op.not]: true }
      },
    });

    if (userObject) throw new HTTPError(400, `User with email id: ${dataObject.email} already exist`);


    const userModel = await Users.create(dataObject);


    await PasswordHistory.create({ email: dataObject.email, password: userModel.password, id: uuid.v4() });

    const user = userModel.get({ plain: true });

    if (user.practice_id) {
      const practiceData = await Practices.findOne({ where: { id: user.practice_id }, });
      if (practiceData) {
        user.practice_name = practiceData.name;
      }
    }

    let emailTemplate = '';
    if (accountType == 'super_admin') {
      emailTemplate = 'Hi ' + input.name + `,<br/><br/> ${appConstants.APP_NAME} has created an account for you to access the site. Here is the site url with login credentials for initial login. On the first login, you must change your password.` +
        '<br/><br/>' + process.env.APP_URL + '<br/><br/>' + 'Email&nbsp;:&nbsp;' + input.email + '<br/>Password&nbsp;:&nbsp;' + input.password;
    } else if (accountType == 'customer_admin') {
      emailTemplate = 'Hi ' + input.name + ',<br/><br/> ' + user.practice_name + ` has created an account for you to access the ${appConstants.APP_NAME} site. Here is the site url with login credentials for initial login. On the first login, you must change your password.` +
        '<br/><br/>' + process.env.APP_URL + '<br/><br/>' + 'Email&nbsp;:&nbsp;' + input.email + '<br/>Password&nbsp;:&nbsp;' + input.password;
      ;
    }
    if (emailNotification) {
      await sendEmail(input.email, `Welcome To ${appConstants.APP_NAME}`, emailTemplate, user.practice_name);
    }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(user),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not create the user.' }),
    };
  }
};

const TestEmail = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    console.log(input.email);
    await sendEmail(input.email, 'Test Email', `Welcome To ${appConstants.APP_NAME}`, "Test practice");
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ status: 'Ok' }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not create the user.' }),
    };
  }
}

const getOne = async (event) => {
  try {
    const params = event.params || event.pathParameters;
    const { Users, Practices, Op } = await connectToDatabase();
    const user = await Users.findOne({ where: { id: params.id, is_deleted: { [Op.not]: true } } });
    if (!user) throw new HTTPError(404, `User with id: ${params.id} was not found`);
    const userObj = await user.get({ plain: true });
    const practiceData = await Practices.findOne({ where: { id: userObj.practice_id, is_deleted: { [Op.not]: true } } });
    if (practiceData && 'name' in practiceData) userObj.practice_name = practiceData.name;
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(userObj),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not fetch the Users.' }),
    };
  }
};

const getAll = async (event) => {
  try {
    const { sequelize } = await connectToDatabase();
    const urlParams = event.query || {};
    let searchKey = '';
    const sortKey = {};

    let sortQuery = '';
    let searchQuery = '';

    const query = event.headers;
    const majorRoles = ["superAdmin", "manager", "operator"];

    if (majorRoles.includes(event.user.role)) {
      if (urlParams && urlParams.type) {
        if (urlParams.type == 'admin') {
          return getAllPaginationAdmin(event);
        } else if (urlParams.type == 'practice') {
          return getAllPaginationPractice(event);
        }
      }
    }
    if (query.offset) query.offset = parseInt(query.offset, 10);
    if (query.limit) query.limit = parseInt(query.limit, 10);
    if (query.search) searchKey = query.search;

    let where = ` WHERE Users.is_deleted IS NOT true  AND Users.practice_id ='${event.user.practice_id}' AND Users.id !='${event.user.id}'`;

    /**Sort**/
    if (query.sort == 'false') {
      sortQuery += ` ORDER BY Users.created_at DESC`
    } else if (query.sort != 'false') {
      const sortObj = JSON.parse(query.sort)
      sortQuery += ` ORDER BY ${sortObj.column} ${sortObj.type}`
    }

    /**Search**/
    const roleSearchKey = searchKey.trim().replace(/\s/g, '');
    if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
      searchQuery = ` AND (Users.role LIKE '%${roleSearchKey}%' OR Users.email LIKE '%${searchKey}%' OR Users.name LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%' OR Users.specialty LIKE '%${searchKey}%') `;
    }

    /**Limit**/
    const limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset}`

    let sqlQuery;
    let sqlQueryCount;
    if (appConstants.ROLE_PHYSICIAN == event.user.role) {
      sqlQuery = `SELECT Users.id, Users.name, Users.practice_id, Users.email,Users.role, Users.is_admin,
      Practices.name AS practice_name, Users.specialty, SignAuthorizations.staff_id FROM Users INNER JOIN Practices ON Users.practice_id = Practices.id LEFT JOIN SignAuthorizations ON Users.id = SignAuthorizations.staff_id AND SignAuthorizations.physician_id = '${event.user.id}'`;

      sqlQueryCount = `SELECT Users.id, Users.name, Users.practice_id, Users.email,Users.role, Users.is_admin,
      Practices.name AS practice_name, Users.specialty, SignAuthorizations.staff_id FROM Users INNER JOIN Practices ON Users.practice_id = Practices.id LEFT JOIN SignAuthorizations ON Users.id = SignAuthorizations.staff_id AND SignAuthorizations.physician_id = '${event.user.id}'`;
    } else {
      sqlQuery = `SELECT Users.id, Users.name, Users.practice_id, Users.email,Users.role, Users.is_admin,
      Practices.name AS practice_name, Users.specialty FROM Users inner JOIN Practices ON Users.practice_id = Practices.id`;

      sqlQueryCount = `SELECT Users.id, Users.name, Users.practice_id, Users.email,Users.role, Users.is_admin,
      Practices.name AS practice_name, Users.specialty FROM Users inner JOIN Practices ON Users.practice_id = Practices.id`;
    }

    sqlQuery += where + searchQuery + sortQuery + limitQuery;
    sqlQueryCount += where + searchQuery;

    const serverData = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT
    });
    const tableDataCount = await sequelize.query(sqlQueryCount, {
      type: QueryTypes.SELECT
    });
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Expose-Headers': 'totalPageCount',
        'totalPageCount': tableDataCount.length
      },
      body: JSON.stringify(serverData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not fetch the Users.' }),
    };
  }
}

const getAllPaginationAdmin = async (event) => {
  const { sequelize } = await connectToDatabase();
  let role = "'superAdmin', 'manager', 'operator'";
  let searchKey = '';
  let sortQuery = '';
  let searchQuery = '';
  let query = event.headers;

  if (query.offset) query.offset = parseInt(query.offset, 10);
  if (query.limit) query.limit = parseInt(query.limit, 10);
  if (query.search) searchKey = query.search;

  const selectQuery = `SELECT Users.id, Users.name, Users.email, Users.role FROM Users`;
  const selectCount = `SELECT COUNT(*) FROM Users`

  const where = ` WHERE (Users.is_deleted IS NOT true AND Users.role IN ( ${role} ) AND Users.email !='${event.user.email}') `

  /**Search**/
  searchQuery = searchKey == 'false' ? '' : ` AND (Users.role LIKE '%${searchKey}%' OR Users.email LIKE '%${searchKey}%' OR Users.name LIKE '%${searchKey}%')`

  /**Sort**/
  if (query.sort == 'false') {
    sortQuery = ` ORDER BY Users.created_at DESC`
  } else {
    const sortObj = JSON.parse(query.sort)
    sortQuery = ` ORDER BY ${sortObj.column} ${sortObj.type}`
  }

  /**Limit**/
  const limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset}`

  const sqlQuery = selectQuery + where + searchQuery + sortQuery + limitQuery;
  const countQuery = selectCount + where + searchQuery;

  const serverData = await sequelize.query(sqlQuery, {
    type: QueryTypes.SELECT
  });

  const tableDataCount = await sequelize.query(countQuery, {
    type: QueryTypes.SELECT
  });

  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': false,
      'Access-Control-Expose-Headers': 'totalPageCount',
      'totalPageCount': tableDataCount[0]['COUNT(*)']
    },
    body: JSON.stringify(serverData)
  };
}

const getAllPaginationPractice = async (event) => {
  const { sequelize } = await connectToDatabase();
  let searchKey = '';
  let sortQuery = '';
  let searchQuery = '';
  const query = event.headers;
  const role = `'physician', 'staff'`;

  if (query.offset) query.offset = parseInt(query.offset, 10);
  if (query.limit) query.limit = parseInt(query.limit, 10);
  if (query.search) searchKey = query.search;

  let selectQuery = `SELECT Users.id, Users.name, Users.email, Users.practice_id, Users.role, Users.is_admin, Practices.name AS practice_name, Users.specialty FROM Users LEFT JOIN Practices ON Users.practice_id = Practices.id`;
  let selectCount = `SELECT COUNT(*) FROM Users LEFT JOIN Practices ON Users.practice_id = Practices.id`;

  const where = ` WHERE (Users.is_deleted IS NOT true AND Users.role IN (${role}) AND Users.id !='${event.user.email}')`;

  /**Search**/
  searchQuery = searchKey == "false" ? "" : ` AND ( Users.name LIKE '%${searchKey}%' OR Users.email LIKE '%${searchKey}%' OR Users.role LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%' OR Users.specialty LIKE '%${searchKey}%' )`

  /**Sort**/
  if (query.sort == "false") {
    sortQuery = ` ORDER BY Users.created_at DESC`
  } else {
    const sortObj = JSON.parse(query.sort);
    sortQuery = ` ORDER BY ${sortObj.column} ${sortObj.type}`
  }

  /**Limit**/
  const limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset}`

  const sqlSelectQuery = selectQuery + where + searchQuery + sortQuery + limitQuery;
  const sqlCountQUery = selectCount + where;

  const serverData = await sequelize.query(sqlSelectQuery, {
    type: QueryTypes.SELECT
  });

  const tableDataCount = await sequelize.query(sqlCountQUery, {
    type: QueryTypes.SELECT
  });


  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': false,
      'Access-Control-Expose-Headers': 'totalPageCount',
      'totalPageCount': tableDataCount[0]['COUNT(*)']
    },
    body: JSON.stringify(serverData)
  };
}

const getMe = async (event, context) => {
  try {
    const { Practices, PasswordHistory, Settings, VersionHistory, Subscriptions, Plans } = await connectToDatabase();
    const user = event.user;
    const settings = await Settings.findOne({ where: { key: 'global_settings' } });
    if (settings) {
      const settingsObject = JSON.parse(settings.value);
      user.session_timeout = settingsObject.session_timeout ? settingsObject.session_timeout : "";
      user.rfa_form_row_limit = settingsObject.rfa_form_row_limit ? settingsObject.rfa_form_row_limit : 25;
      user.policy_doc_updated_date = settingsObject.policy_doc_updated_date;
    } else {
      user.session_timeout = "3000";
      user.rfa_form_row_limit = 25;
    }

    if (user.practice_id) {

      const practiceDetails = await Practices.findOne({ where: { id: event.user.practice_id }, });

      if (practiceDetails) {

        let getcurrentSubscription = await Subscriptions.findOne({
          where: {
            practice_id: event.user.practice_id,
          }, order: [["created_at", "DESC"]]
        });
        if (getcurrentSubscription && getcurrentSubscription.stripe_subscription_id) {
          const subscription = await stripe.subscriptions.retrieve(getcurrentSubscription.stripe_subscription_id);
          getcurrentSubscription.stripe_subscription_data = JSON.stringify(subscription);
          await getcurrentSubscription.save();
        }

        user.practiceDetails = practiceDetails.get({ plain: true });


      }
    }

    const versionHistoryObject = await VersionHistory.findOne({
      where: {},
    });

    if (versionHistoryObject) {
      user.version = versionHistoryObject.version;
    } else {
      user.version = 1;
    }

    const lastPasswordChangeObject = await PasswordHistory.findOne({
      order: [
        ['created_at', 'DESC'],
      ],
      where: {
        email: user.email,
      }
    });

    if (lastPasswordChangeObject) {
      user.lastPasswordChangeDate = new Date(lastPasswordChangeObject.created_at);
    } else {
      const days30DaysBack = new Date();
      days30DaysBack.setDate(days30DaysBack.getDate() - 90);
      user.lastPasswordChangeDate = days30DaysBack;
    }

    // if (event.user.practice_id) {
    //   const subscriptionObj = await Subscriptions.findOne({
    //     where: { practice_id: event.user.practice_id }
    //   });
    //   if (subscriptionObj && subscriptionObj.plan_id) {
    //     const planObj = await Plans.findOne({ where: { id: subscriptionObj.plan_id } });
    //     user.plan_type = planObj.plan_type;
    //   } else {
    //     user.plan_type = appConstants.PLAN_PAY_AS_YOU_GO;
    //   }
    // }

    let plan_type = appConstants.PLAN_PAY_AS_YOU_GO;
    let plansObj;
    let subscriptionObj = await Subscriptions.findOne({
      where: { practice_id: event.user.practice_id }
    });

    if (subscriptionObj && subscriptionObj.plan_id) {
      plansObj = await Plans.findOne({ where: { id: subscriptionObj.plan_id, active: true }, raw: true });
    }

    if (subscriptionObj && subscriptionObj.plan_id && plansObj && plansObj.plan_type !== appConstants.PLAN_PAY_AS_YOU_GO) {

      // subscription plan have 3 status: plan-active:cancel_at_end_of_time-false,canceled, plan-active:cancel_at_end_of_time-true

      /* 
          ######## Immediate cancel sample response ######### (we can cancel immediatly only in stripe dashboard)
          "cancel_at": null,
          "cancel_at_period_end": false,
          "canceled_at": 1663844574,
          "collection_method": "charge_automatically",
          "created": 1662974204,
          "currency": "usd",
          "current_period_end": 1666158012,
          "current_period_start": 1663566012,
          "status": "canceled",
      */

      /* 
         ######## cancel at plan end of time - sample response ######### (we can cancel end of time using subscriptioncancel api)
         "cancel_at": 1666336294,
         "cancel_at_period_end": true,
         "canceled_at": 1663846433,
         "collection_method": "charge_automatically",
         "created": 1662705920,
         "currency": "usd",
         "current_period_end": 1666336294,
         "current_period_start": 1663830694,
         "status": "active",  
         
         ######## cancel at plan end of time - sample response ######### (we can cancel customize date only in stripe dashboard)
         "cancel_at": 1663934400,     // cancel the plan using custom date
         "cancel_at_period_end": false,
         "canceled_at": 1663857869,
         "collection_method": "charge_automatically",
         "created": 1663056357,
         "currency": "usd",
         "current_period_end": 1663934400,
         "current_period_start": 1663056357,
         "status": "active",
      */

      plan_type = plansObj.plan_type;
      const stripe_subscription_data = JSON.parse(subscriptionObj.stripe_subscription_data);
      const current_date = new Date();
      const current_period_end = unixTimetoDateconvert(stripe_subscription_data.current_period_end);
      if ((stripe_subscription_data.status != "active") || (current_period_end <= current_date && stripe_subscription_data.canceled_at && stripe_subscription_data.status === "active" && (stripe_subscription_data.cancel_at_period_end || !stripe_subscription_data.cancel_at_period_end))) {
        let updateColumn = { stripe_product_id: null, stripe_subscription_id: null, subscribed_on: null, plan_id: null, subscribed_valid_till: null }
        await Subscriptions.update(updateColumn, { where: { practice_id: event.user.practice_id, id: subscriptionObj.id } });
        plan_type = '';
      }
    } else if (subscriptionObj && !subscriptionObj.plan_id) {
      plan_type = '';
    }

    user.plan_type = plan_type;

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(user),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': false
      },
      body: JSON.stringify({ error: err.message || 'Could not fetch the Users.' }),
    };
  }
};

const sessionTokenGenerate = async (event) => {
  try {
    const { Users, Op, Settings } = await connectToDatabase();
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const userId = input.id;
    const userObject = await Users.findOne({ where: { id: userId, is_deleted: { [Op.not]: true } }, });
    const userPlain = userObject.get({ plain: true });

    const tokenUser = {
      id: userPlain.id,
      role: userPlain.role,
    };
    const token = jwt.sign(
      tokenUser,
      process.env.JWT_SECRET,
      {
        expiresIn: process.env.JWT_EXPIRATION_TIME,
      }
    );
    const authToken = `JWT ${token}`;
    const ciphertext = CryptoJS.AES.encrypt(authToken, userId).toString();
    const responseData = {
      user: {
        name: userPlain.name,
        role: userPlain.role,
      },
    };
    responseData.authToken = ciphertext;
    const settingsObj = await Settings.findOne({ where: { key: 'global_settings' } });
    if (settingsObj) {
      responseData.session_timeout = JSON.parse(settingsObj.value).session_timeout;
    }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(responseData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Cannot Generate session Token.' }),
    };
  }
};


const changePassword = async (event) => {
  try {
    const input = JSON.parse(event.body);
    validateResetPassword(input);
    const { Users, PasswordHistory, Op } = await connectToDatabase();
    const userObject = await Users.findOne({
      where: {
        is_deleted: { [Op.not]: true },
        id: event.user.id
      }
    });
    if (await userObject.checkIfLast6Password(input.new_password, PasswordHistory)) throw new HTTPError(401, 'Can not use any last 6 past password');
    userObject.password = await userObject.generateNewPassword(input.new_password);
    userObject.temporary_password = false;
    await userObject.save();
    await PasswordHistory.create({ email: userObject.email, password: userObject.password, id: uuid.v4() });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'ok',
        message: 'Password Changed',
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not update the Users.' }),
    };
  }

};

const insertUser = async () => {
  try {
    const { Users } = await connectToDatabase();
    const isExists = await Users.findOne({
      where: { id: "claimsmade_super_admin_id" }
    })
    if (isExists) throw new HTTPError(400, `User already exist`);
    const userObj = {
      id: "claimsmade_super_admin_id",
      name: "ClaimsMade Admin",
      email: "admin@claimsmade.com",
      password: "ClaimsMade22!",
      role: "superAdmin"
    }
    const response = await Users.create(userObj);
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(response)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: error.message || 'Could not create the Users.' }),
    };
  }
};

const getUserDetails = async (event) => {
  try {
    const userId = event.pathParameters.id;
    const { sequelize, SignAuthorization } = await connectToDatabase();
    const sqlQuery = `SELECT Users.name, Practices.name as practice_name, Practices.contact_name, Practices.address, 
    Practices.city, Practices.state, Practices.zip_code, Practices.fax_number, Users.specialty, 
    Practices.npi_number, Practices.phone_number, Users.email, Users.signature FROM Users INNER JOIN Practices ON Users.practice_id = Practices.id where Users.id = '${userId}';`;

    const response = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT
    });

    const hasAccessObj = await SignAuthorization.findOne({
      where: {
        physician_id: userId,
        staff_id: event.user.id
      }
    });

    if (response && hasAccessObj) {
      response[0].hasAccess = true;
    } else if (response) {
      response[0].hasAccess = false;
      delete response[0].signature;
    }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(response)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: error.message || 'Could not get the user.' }),
    };
  }
}

const forgetPassword = async (event) => {
  try {
    const input = JSON.parse(event.body);
    validateForgetPassword(input);

    const email = input.email;

    const { Users, Op } = await connectToDatabase();

    const userObject = await Users.findOne({
      where: {
        email,
        is_deleted: { [Op.not]: true }
      },
    });

    if (!userObject) throw new HTTPError(404, `Couldn\'t find your ${appConstants.APP_NAME} account`);

    const tokenUser = {
      id: userObject.id,
      role: userObject.role,
    };
    const token = jwt.sign(
      tokenUser,
      process.env.JWT_SECRET,
      {
        expiresIn: 60 * 90,
      }
    );
    // await sendEmail(userObject.email, 'Important: Reset your password', `This email is in response to your request to reset password. If you have not requested to reset password, please ignore this email. <br/><br/>

    let link = `<a href="${process.env.APP_URL}/reset-password/?email=${userObject.email}&token=${token}&expireIn=90">link</a>`;
    await sendEmail(userObject.email, "Important: Reset your password", `This email is in response to your request to reset password. If you have not requested to reset password, please ignore this email. <br/><br/>
Reset your password by clicking on the ${link}.`, `${appConstants.APP_NAME}`);

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'ok',
        message: 'Password reset instructions sent to your email.',
      }),
    };
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not create the users.' }),
    };
  }
};

const resetPassword = async (event) => {
  try {
    const input = JSON.parse(event.body);
    validateResetPassword(input);
    const { Users, PasswordHistory, Op } = await connectToDatabase();
    /* const userObject = await Users.findById(event.user.id); */
    const userObject = await Users.findOne({ where: { is_deleted: { [Op.not]: true }, id: event.user.id } });
    if (!userObject) throw new HTTPError(400, 'Users not found');
    if (await userObject.checkIfLast6Password(input.new_password, PasswordHistory)) throw new HTTPError(401, 'Can not use any last 6 past password');
    userObject.password = await userObject.generateNewPassword(input.new_password);
    userObject.login_attempts = 0;
    userObject.reactivation_token = null;
    await userObject.save();
    await PasswordHistory.create({ email: userObject.email, password: userObject.password, id: uuid.v4() });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'ok',
        message: 'Password Changed',
      }),
    };
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not reset the password.' }),
    };
  }
};

const resetLoginAttempt = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { Users, Op } = await connectToDatabase();
    const userObject = await Users.findOne({
      where: {
        email: input.email,
        is_deleted: { [Op.not]: true }
      },
    });
    if (!userObject) throw new HTTPError(400, 'Users not found');
    userObject.login_attempts = 0;
    await userObject.save();
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'ok',
        message: 'Login Attempt reset',
      }),
    };
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not update the Users.' }),
    };
  }
};

const updateSignature = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    validateSignature(input);
    const { Users, Op } = await connectToDatabase();
    const userObject = await Users.findOne({
      where: { is_deleted: { [Op.not]: true }, id: event.user.id },
    });
    if (!userObject) throw new HTTPError(400, "Users not found");

    userObject.signature = input.signature;
    await userObject.save();
    return {
      statusCode: 200, headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      }, body: JSON.stringify({
        status: "ok", message: "Signature Updated",
      }),
    };
  } catch (err) {
    return {
      statusCode: err.statusCode || 500, headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      }, body: JSON.stringify({
        error: err.message || "Could not update the Users.",
      }),
    };
  }
};

const getAllUsersByRole = async (event) => {
  try {
    const headers = event.headers;
    authorizeGetAllUsersByRole(event.user.role);
    const { Users, Op } = await connectToDatabase();
    const users = await Users.findAll({
      where: {
        is_deleted: { [Op.not]: true },
        role: headers.role,
        practice_id: event.user.practice_id
      },
      attributes: ['id', 'name']
    });
    return {
      statusCode: 200, headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      }, body: JSON.stringify(users),
    };
  } catch (err) {
    return {
      statusCode: err.statusCode || 500, headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      }, body: JSON.stringify({
        error: err.message || "Could not fetch users.",
      }),
    };
  }
}

const updateTwoFactorStatus = async (event) => {
  try {
    const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
    validateTwofactor(input);
    const { Users, Op } = await connectToDatabase();
    const id = event.user.id;
    const userObject = await Users.findOne({
      where: { id: id, is_deleted: { [Op.not]: true } },
    });

    if (!userObject) throw new HTTPError(400, `User not found to update`);

    if (input.two_factor_status == true) {
      userObject.two_factor_status = true;
    } else {
      userObject.two_factor_status = false;
    }

    await userObject.save();

    return {
      statusCode: 200, headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      }, body: JSON.stringify({
        message: "Two Factor Authentication Settings Updated",
      }),
    };


  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500, headers: {
        "Content-Type": "text/plain",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": true,
      }, body: JSON.stringify({
        error: err.message || "Could not update the Users.",
      }),
    };
  }
};

const updateAuthorization = async (event) => {
  try {
    const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
    authorizeUpdateAuth(event.user);
    validateUpdateAuth(input);
    const { SignAuthorization } = await connectToDatabase();

    if ('authorize' === input.authType) {
      const alreadyAuthorized = await SignAuthorization.findOne({
        where: { physician_id: event.user.id, staff_id: input.staff_id }
      });
      if (!alreadyAuthorized) {
        await SignAuthorization.create({
          id: uuid.v4(),
          physician_id: event.user.id,
          staff_id: input.staff_id
        });
      } else {
        throw new HTTPError(400, `Already authorized`);
      }
    } else if ('revoke' === input.authType) {
      await SignAuthorization.destroy({
        where: {
          physician_id: event.user.id,
          staff_id: input.staff_id
        }
      });
    }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: "Ok",
        message: "Updated access"
      }),
    };
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not update access.' }),
    };
  }
}

const unixTimetoDateconvert = (timestamp) => {
  const unixTimestamp = timestamp;
  const date = new Date(unixTimestamp * 1000);
  return date;
};

module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.signUp = signUp;
module.exports.create = create;
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = getAll;
module.exports.getMe = middy(getMe).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());;
module.exports.sessionTokenGenerate = sessionTokenGenerate;
module.exports.changePassword = middy(changePassword).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.TestEmail = TestEmail;
module.exports.insertUser = insertUser;
module.exports.getUserDetails = middy(getUserDetails).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.forgetPassword = forgetPassword;
module.exports.resetPassword = middy(resetPassword).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.resetLoginAttempt = resetLoginAttempt;
module.exports.updateSignature = updateSignature;
module.exports.getAllUsersByRole = getAllUsersByRole;
module.exports.updateTwoFactorStatus = middy(updateTwoFactorStatus).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.updateAuthorization = middy(updateAuthorization).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
