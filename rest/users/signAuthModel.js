module.exports = (sequelize, type) => sequelize.define('SignAuthorization', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    physician_id: type.STRING,
    staff_id: type.STRING
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});
