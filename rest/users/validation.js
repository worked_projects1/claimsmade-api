const Validator = require('validatorjs');
const commonPassword = require('common-password-checker');
const { HTTPError } = require('../../utils/httpResp');

exports.validateSignup = function (data) {
  const rules = {
    name: 'required|min:4|max:64',
    email: 'required|email|min:4|max:64',
    password: 'required|min:4|max:64',
    role: 'required',
  };

  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }

  const hasUpperCase = /[A-Z]/.test(data.password);
  const hasLowerCase = /[a-z]/.test(data.password);
  const hasNumbers = /\d/.test(data.password);
  const hasNonalphas = /\W/.test(data.password);
  const hasTripple = /(.)\1\1/.test(data.password);


  if (!hasUpperCase && !hasLowerCase) throw new HTTPError(400, 'Password must contain one alphabetic character');
  if (!hasNumbers && !hasNonalphas) throw new HTTPError(400, 'Password must contain one numeric or special character');
  if (hasTripple) throw new HTTPError(400, 'Password must not have consecutive characters');
  if (commonPassword(data.password)) throw new HTTPError(400, 'Password must not be a common or easily guessable password');

  const validRoles = ['superAdmin', 'manager', 'operator', 'physician', 'staff'];
  if (!validRoles.includes(data.role)) throw new HTTPError(400, 'Invalid role');
};

exports.validateUpdateUser = function (data) {
  const rules = {
    name: 'min:4|max:64',
    email: 'email|min:4|max:64',
    password: 'min:4|max:64',
    is_admin: 'boolean'
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }

  if (data.password) {
    const hasUpperCase = /[A-Z]/.test(data.password);
    const hasLowerCase = /[a-z]/.test(data.password);
    const hasNumbers = /\d/.test(data.password);
    const hasNonalphas = /\W/.test(data.password);
    const hasTripple = /(.)\1\1/.test(data.password);


    if (!hasUpperCase && !hasLowerCase) throw new HTTPError(400, 'Password must contain one alphabetic character');
    if (!hasNumbers && !hasNonalphas) throw new HTTPError(400, 'Password must contain one numeric or special character');
    if (hasTripple) throw new HTTPError(400, 'Password must not have consecutive characters');
    if (commonPassword(data.password)) throw new HTTPError(400, 'Password must not be a common or easily guessable password');
  }

  const validRoles = ['physician', 'staff', 'superAdmin', 'manager', 'operator'];
  if (data.role && !validRoles.includes(data.role)) throw new HTTPError(400, 'Invalid role');

};

exports.validateCreateUser = function (data) {
  const rules = {
    name: 'required|min:4|max:64',
    email: 'required|email|min:4|max:64',
    password: 'required|min:4|max:64',
    role: 'required',
    is_admin: 'boolean'
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
  const hasUpperCase = /[A-Z]/.test(data.password);
  const hasLowerCase = /[a-z]/.test(data.password);
  const hasNumbers = /\d/.test(data.password);
  const hasNonalphas = /\W/.test(data.password);
  const hasTripple = /(.)\1\1/.test(data.password);


  if (!hasUpperCase && !hasLowerCase) throw new HTTPError(400, 'Password must contain one alphabetic character');
  if (!hasNumbers && !hasNonalphas) throw new HTTPError(400, 'Password must contain one numeric or special character');
  if (hasTripple) throw new HTTPError(400, 'Password must not have consecutive characters');
  if (commonPassword(data.password)) throw new HTTPError(400, 'Password must not be a common or easily guessable password');

  const validRoles = ['superAdmin', 'manager', 'operator', 'physician', 'staff'];
  if (!validRoles.includes(data.role)) throw new HTTPError(400, 'Invalid role');
};

exports.validateReadAllUsers = function (data) {
  const rules = {
    offset: 'numeric',
    limit: 'numeric',
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};


exports.validateResetPassword = function (data) {
  const rules = {
    new_password: 'required|min:4|max:64',
  };
  const validation = new Validator(data, rules);

  const hasUpperCase = /[A-Z]/.test(data.new_password);
  const hasLowerCase = /[a-z]/.test(data.new_password);
  const hasNumbers = /\d/.test(data.new_password);
  const hasNonalphas = /\W/.test(data.new_password);
  const hasTripple = /(.)\1\1/.test(data.new_password);
  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }

  if (!hasUpperCase && !hasLowerCase) throw new HTTPError(400, 'Password must contain one alphabetic character');
  if (!hasNumbers && !hasNonalphas) throw new HTTPError(400, 'Password must contain one numeric or special character');
  if (hasTripple) throw new HTTPError(400, 'Password must not have consecutive characters');
  if (commonPassword(data.new_password)) throw new HTTPError(400, 'Password must not be a common or easily guessable password');
};

exports.validatePracticeUser = function (user, input) {
  const rules = {
    practice_id: 'required',
    is_admin: 'required|boolean'
  }
  const validation = new Validator(input, rules);
  if (validation.fails()) throw new HTTPError(400, validation.errors.all())
}

exports.validateIsAdmin = function (input) {
  const rules = {
    is_admin: 'required|boolean'
  }
  const validation = new Validator(input, rules);
  if (validation.fails()) throw new HTTPError(400, validation.errors.all())
}

exports.validateForgetPassword = function (data) {
  const rules = {
    email: 'required|email|min:4|max:64',
  };

  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};

exports.validateSignature = (data) => {
  const rules = {
    signature: 'required'
  };

  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};

exports.validateTwofactor = (data) => {
  const rules = {
    two_factor_status: 'required'
  };

  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};

exports.validateUpdateAuth = function (data) {
  const rules = {
      staff_id: 'required',
      authType: 'required'
  }

  if (!['authorize', 'revoke'].includes(data.authType)) throw new HTTPError(400, `Invalid authorization type`)
  const validation = new Validator(data, rules);
  if (validation.fails()) {
      throw new HTTPError(400, validation.errors.all());
  }
}