const Validator = require('validatorjs');
const { HTTPError } = require('../../utils/httpResp');

exports.validateGetOneOrder = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateUpdateOrders = function (data) {
    const rules = {
        order_date: 'date',
        status: 'min:4|max:64',
        practice_id: 'min:4|max:64',
        case_id: 'min:4|max:64',
        activity_id: 'min:4|max:64',
        activity_type: 'min:3|max:64',
        charge_id: 'min:4|max:64'
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};