const { appConstants } = require('../../appConstants');
const { HTTPError } = require('../../utils/httpResp');


exports.authorizeAdminGetAll = function (user) {
    if (!appConstants.ADMIN_ROLES.includes(user.role)) throw new HTTPError(403, `You don't have access`)
}