module.exports = (sequelize, type) => sequelize.define('Orders', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    order_date: type.DATE,
    status: type.STRING, // completed, pending
    practice_id: type.STRING,
    user_id: type.STRING,
    case_id: type.STRING,
    activity_id: type.STRING,
    activity_type: type.STRING,
    amount_charged: type.FLOAT,
    charge_id: type.STRING,
    is_deleted: type.BOOLEAN,
    order_id: {
        type: type.INTEGER,
        autoIncrement: true,
        allowNull: false,
        unique: true,
    },
    plan_type: type.STRING,
    patient_name: type.STRING,
    claim_number: type.STRING,
    employer: type.STRING,
    claims_admin_name: type.STRING,
    claims_admin_id: type.STRING
}, {
    updatedAt: 'updated_at',
    createdAt: 'created_at',
});
