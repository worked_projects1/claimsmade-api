const uuid = require('uuid');
const connectToDatabase = require('../../db');

const { QueryTypes } = require('sequelize');
const sequelize = require('sequelize');
const { validateUpdateOrders, validateGetOneOrder } = require('./validation');
const { authorizeAdminGetAll } = require('./authorize');

const create = async (event) => {
    try {
        let id;
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        if (process.env.NODE_ENV === 'test' && input.id) id = input.id;
        const dataObject = Object.assign(input, {
            id: input.id || uuid.v4(),
            practice_id: event.user.practice_id,
            user_id: event.user.user_id
        });
        // validateCreateOrders(dataObject);
        const { Orders } = await connectToDatabase();
        const orders = await Orders.create(dataObject);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(orders),
        };
    } catch (err) {
        console.log(error);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the orders.' }),
        };
    }
};

const getOne = async (event) => {
    try {
        event.pathParameters = event.pathParameters || event.params;
        const { Orders } = await connectToDatabase();
        validateGetOneOrder(event.pathParameters);
        const orders = await Orders.findOne({ where: { id: event.pathParameters.id } });
        if (!orders) throw new HTTPError(404, `Orders with id: ${event.pathParameters.id} was not found`);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(orders),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Orders.' }),
        };
    }
};

const update = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        event.pathParameters = event.pathParameters || event.params;
        validateUpdateOrders(input);
        const { Orders } = await connectToDatabase();
        const orders = await Orders.findOne({ where: { id: event.pathParameters.id } });
        if (!orders) throw new HTTPError(404, `Orders with id: ${event.pathParameters.id} was not found`);
        const updatedModel = Object.assign(orders, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Orders.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);

        const { sequelize, Users } = await connectToDatabase();

        const select = `SELECT Orders.id, Orders.plan_type, Orders.patient_name, Orders.claim_number, Orders.employer, Users.name as 'user_name', ClaimsAdmins.company_name as 'claims_admin_name', Orders.order_date, Orders.amount_charged FROM Orders LEFT JOIN ClaimsAdmins ON ClaimsAdmins.id = Orders.claims_admin_id LEFT JOIN Users ON Orders.user_id = Users.id WHERE Orders.is_deleted IS NOT TRUE AND Orders.practice_id = '${event.user.practice_id}'`
        const count = `SELECT COUNT(*) FROM Orders LEFT JOIN ClaimsAdmins ON ClaimsAdmins.id = Orders.claims_admin_id LEFT JOIN Users ON Orders.user_id = Users.id WHERE Orders.is_deleted IS NOT TRUE AND Orders.practice_id = '${event.user.practice_id}'`

        //search
        let searchQuery = '';
        if (query.search != "false") {
            query.search = query.search == 'PAY AS-YOU-GO' ? 'pay_as_you_go' : query.search;
            searchQuery = ` AND ( status LIKE '%${query.search}%' OR plan_type LIKE '%${query.search}%' OR patient_name LIKE '%${query.search}%' OR employer LIKE '%${query.search}%' OR claims_admin_name LIKE '%${query.search}%' OR claim_number LIKE '%${query.search}%' OR amount_charged LIKE '%${query.search}%' )`;
        }

        //sort
        let orderQuery = '';
        if (query.sort != "false") {
            query.sort = JSON.parse(query.sort)
            orderQuery = ` ORDER BY ${query.sort.column} ${query.sort.type}`
        } else {
            orderQuery = ` ORDER BY Orders.created_at DESC`
        }

        //limit
        let limitQuery = '';
        if (query.limit && query.offset) {
            limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset}`
        } else {
            limitQuery = ' LIMIT 25 OFFSET 0'
        }

        let selectQuery = select + searchQuery + orderQuery + limitQuery
        let countQuery = count + searchQuery;

        let data = await sequelize.query(selectQuery, {
            type: QueryTypes.SELECT
        });
        const totalPageCount = await sequelize.query(countQuery, {
            type: QueryTypes.SELECT
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': totalPageCount[0]['COUNT(*)']
            },
            body: JSON.stringify(data),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the orders.' }),
        };
    }
}


const adminGetAll = async (event) => {
    try {
        authorizeAdminGetAll(event.user)
        const query = event.headers;
        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);

        const { sequelize, Users } = await connectToDatabase();

        const select = `SELECT Orders.id, Orders.order_date, Orders.claim_number, Orders.plan_type, Practices.name as 'practice_name', Users.name as 'user_name', Orders.amount_charged, Orders.user_id FROM Orders LEFT JOIN Users ON Users.id = Orders.user_id LEFT JOIN Practices ON Practices.id = Orders.practice_id WHERE Orders.is_deleted IS NOT TRUE`
        const count = `SELECT COUNT(*) FROM Orders LEFT JOIN Users ON Users.id = Orders.user_id LEFT JOIN Practices ON Practices.id = Orders.practice_id WHERE Orders.is_deleted IS NOT TRUE`


        //filter
        let filterQuery = '';
        const filterObj = JSON.parse(query.filter)
        if (filterObj.practice_name != 'all') {
            filterQuery = ` AND Practices.name = '${filterObj.practice_name}'`
        }

        //search
        let searchQuery = '';
        if (query.search != "false") {
            query.search = query.search == 'PAY AS-YOU-GO' ? 'pay_as_you_go' : query.search;
            searchQuery = ` AND ( status LIKE '%${query.search}%' OR plan_type LIKE '%${query.search}%' OR Practices.name LIKE '%${query.search}%' OR Users.name LIKE '%${query.search}%' OR Orders.claim_number LIKE '%${query.search}%' OR amount_charged LIKE '%${query.search}%' )`;
        }

        //sort
        let orderQuery = '';
        if (query.sort != "false") {
            query.sort = JSON.parse(query.sort)
            orderQuery = ` ORDER BY ${query.sort.column} ${query.sort.type}`
        } else {
            orderQuery = ` ORDER BY Orders.created_at DESC`
        }

        //limit
        let limitQuery = '';
        if (query.limit && query.offset) {
            limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset}`
        } else {
            limitQuery = ' LIMIT 25 OFFSET 0'
        }

        let selectQuery = select + filterQuery + searchQuery + orderQuery + limitQuery
        let countQuery = count + filterQuery + searchQuery;

        let data = await sequelize.query(selectQuery, {
            type: QueryTypes.SELECT
        });
        const totalPageCount = await sequelize.query(countQuery, {
            type: QueryTypes.SELECT
        });

        // for (var i = 0; i < data.length; i++) {
        //     const user = await Users.findOne({ where: { id: data[i].user_id } });
        //     data[i].user_name = user.name;
        // }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': totalPageCount[0]["COUNT(*)"]
            },
            body: JSON.stringify(data),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the orders.' }),
        };
    }
}


const destroy = async (event) => {
    try {
        const orderId = event.pathParameters || event.params;
        console.log("OrderId > " + JSON.stringify(orderId));
        const { Orders } = await connectToDatabase();
        const ordersObj = await Orders.findOne({ where: { id: orderId.id } });
        await ordersObj.destroy();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Deleted"
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the orders.' }),
        };
    }
}


module.exports.create = create;
module.exports.getOne = getOne;
module.exports.update = update;
module.exports.getAll = getAll;
module.exports.destroy = destroy;
module.exports.adminGetAll = adminGetAll;
