const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');

const healthCheckApi = require('../handler');
const authMiddleware = require('../authExpress');
const usersApi = require('./users/controller');
const practicesApi = require('./practices/controller');
const testApi = require('./Test_api_module/controller');
const treatmentApi = require('./treatments/controller');
const bodySystemApi = require('./bodySystem/controller');
const versionHistoryApi = require('./versionHistory/controller');
const rfaFormsApi = require('./rfa_forms/controller');
const claimsAdminApi = require('./claims_admin/controller');
const migrationApi = require('./migration/controller');
const plansApi = require('./plans/controller');
const billingApi = require('./billing/controller');
const ordersApi = require('./orders/controller');
const subscriptionsApi = require('./subscriptions/controller');
const stripeApi = require('../rest/stripe_webhook/controller');
const statisticsApi = require('../rest/statistics/controller');
const policyDocVerificationAPI = require('../rest/policy_doc_verification/controller');

const app = express();
app.use(bodyParser.json({ strict: false, limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(cors());
app.options('*', cors());

app.get('/rest/', authMiddleware, async (req, res) => {
  try {
    const responseData = await healthCheckApi.healthCheck(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/users', authMiddleware, async (req, res) => {
  try {
    const responseData = await usersApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/users/session', async (req, res) => {
  try {
    const responseData = await usersApi.sessionTokenGenerate(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.delete('/rest/users/removeTestRecords', authMiddleware, async (req, res) => {
  try {
    const testApi = require('./Test_api_module/controller');
    const responseData = await testApi.destroyTestRecords(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/users/createTestSuperAdmin', authMiddleware, async (req, res) => {
  try {
    const responseData = await testApi.createTestSuperAdmin(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/practices', authMiddleware, async (req, res) => {
  try {
    const responseData = await practicesApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/practicesNames', authMiddleware, async (req, res) => {
  try {
    const responseData = await practicesApi.getAllPracticesNames(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/treatments', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/diagnoses', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllDiagnoses(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/diagnosisNames', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllDiagnosisNames(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/bodySystem', authMiddleware, async (req, res) => {
  try {
    const responseData = await bodySystemApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/users/reset-login-attempt', async (req, res) => {
  try {
    const responseData = await usersApi.resetLoginAttempt(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/getTreatmentDiagnosisByType', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getTreatmentDiagnosisByType(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/versionHistory', async (req, res) => {
  try {
    const responseData = await versionHistoryApi.getVersionHistory(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});
app.put('/rest/versionHistory', authMiddleware, async (req, res) => {
  try {
    const responseData = await versionHistoryApi.setVersionHistory(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/rfaForms', authMiddleware, async (req, res) => {
  try {
    const responseData = await rfaFormsApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/users/reset-login-attempt', async (req, res) => {
  try {
    const responseData = await usersApi.resetLoginAttempt(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/user/update-signature', authMiddleware, async (req, res) => {
  try {
    const responseData = await usersApi.updateSignature(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/rfaForm/updateRfaFormDetails', authMiddleware, async (req, res) => {
  try {
    const responseData = await rfaFormsApi.updateRfaFormDetails(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/users/getAllUsersByRole', authMiddleware, async (req, res) => {
  try {
    const response = await usersApi.getAllUsersByRole(req);
    return res
      .status(response.statusCode).header(response.headers)
      .json(JSON.parse(response.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/claims_admin', authMiddleware, async (req, res) => {
  try {
    const response = await claimsAdminApi.getAll(req);
    return res
      .status(response.statusCode).header(response.headers)
      .json(JSON.parse(response.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/getAllBodySystemNames', authMiddleware, async (req, res) => {
  try {
    const responseData = await bodySystemApi.getAllBodySystemNames(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/treatments/getAllTreatmentNames', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllTreatmentNames(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/claims_admin', authMiddleware, async (req, res) => {
  try {
    const responseData = await claimsAdminApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/rfaForms', authMiddleware, async (req, res) => {
  try {
    const responseData = await rfaFormsApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/bodySystem', authMiddleware, async (req, res) => {
  try {
    const responseData = await bodySystemApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/users', authMiddleware, async (req, res) => {
  try {
    const responseData = await usersApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/practices', authMiddleware, async (req, res) => {
  try {
    const responseData = await practicesApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/getAllSyncData', async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllSyncData(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/migration/importClaimsAdmins', async (req, res) => {
  try {
    const responseData = await migrationApi.importClaimsAdmins(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/updateAPIForsync', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.updateAPIForsync(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/claimsAdmin/getAllClaimsAdmins', authMiddleware, async (req, res) => {
  try {
    const responseData = await claimsAdminApi.getAllClaimsAdmins(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/migration/updateRfaFormDoc', authMiddleware, async (req, res) => {
  try {
    const responseData = await migrationApi.updateRfaFormDoc(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/treatments/getAllDiagnosisByBodysystemId', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllDiagnosisByBodysystemId(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/treatments/getAllTreatmentDetails', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllTreatmentDetails(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/treatments/getAllTreatmentByBodySystems', authMiddleware, async (req, res) => {
  try {
    const responseData = await treatmentApi.getAllTreatmentByBodySystems(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/plans', authMiddleware, async (req, res) => {
  try {
    const responseData = await plansApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/plans', async (req, res) => {
  try {
    const responseData = await plansApi.getAll(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/plans/:id', authMiddleware, async (req, res) => {
  try {
    const responseData = await plansApi.getOne(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/plans/:id', authMiddleware, async (req, res) => {
  try {
    const responseData = await plansApi.update(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.delete('/rest/plans/:id', authMiddleware, async (req, res) => {
  try {
    const responseData = await plansApi.destroy(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/billing/set-payment-intent', authMiddleware, async (req, res) => {
  try {
    const responseData = await billingApi.setUpPaymentIntent(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/billing/save-card-data', authMiddleware, async (req, res) => {
  try {
    const responseData = await billingApi.saveCardData(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/orders', authMiddleware, async (req, res) => {
  try {
    const responseData = await ordersApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/orders', authMiddleware, async (req, res) => {
  try {
    const responseData = await ordersApi.getAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/orders/adminGetAll', authMiddleware, async (req, res) => {
  try {
    const responseData = await ordersApi.adminGetAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/orders/:id', authMiddleware, async (req, res) => {
  try {
    const responseData = await ordersApi.getOne(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/orders/:id', authMiddleware, async (req, res) => {
  try {
    const responseData = await ordersApi.update(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.delete('/rest/orders/:id', authMiddleware, async (req, res) => {
  try {
    const responseData = await ordersApi.destroy(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

/* Subscriptions */
app.post('/rest/subscriptions', authMiddleware, async (req, res) => {
  try {
    const responseData = await subscriptionsApi.create(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/subscriptions/:id', authMiddleware, async (req, res) => {
  try {
    const responseData = await subscriptionsApi.getOne(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/billing/fetch-billing-status', authMiddleware, async (req, res) => {
  try {
    const responseData = await billingApi.fetchBillingStatus(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/subscriptions/:id', authMiddleware, async (req, res) => {
  try {
    const responseData = await subscriptionsApi.update(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/subscription/adminGetAll', authMiddleware, async (req, res) => {
  try {
    const responseData = await subscriptionsApi.adminGetAll(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/subscriptions', authMiddleware, async (req, res) => {
  try {
    const responseData = await subscriptionsApi.getAll(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/subscription/getAllHistories', authMiddleware, async (req, res) => {
  try {
    const responseData = await subscriptionsApi.getAllHistory(req);
    return res
      .status(responseData.statusCode).header(responseData.headers)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/stripe/updateSubscription', async (req, res) => {
  try {
    const responseData = await billingApi.updateSubscriptionWebHookStatus(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/admin/subscription-cancel', authMiddleware, async (req, res) => {
  try {
    const responseData = await stripeApi.subscriptionCancel(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/statistics', authMiddleware, async (req, res) => {
  try {
    const responseData = await statisticsApi.getAll(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/statistics/practices-and-revenue', authMiddleware, async (req, res) => {
  try {
    const responseData = await statisticsApi.getPracticesAndRevenueStat(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/practices/getPracticesForPricing', authMiddleware, async (req, res) => {
  try {
    const responseData = await practicesApi.getPracticesForPricing(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/plans/getAllPricingPlans', authMiddleware, async (req, res) => {
  try {
    const responseData = await plansApi.getAllPricingPlans(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.post('/rest/migration/getUploadUrlForClaimsAdminImport', authMiddleware, async (req, res) => {
  try {
    const responseData = await migrationApi.getUploadUrlForClaimsAdminImport(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/verifyPolicyDocs', authMiddleware, async (req, res) => {
  try {
    const responseData = await policyDocVerificationAPI.verifyPolicyDocs(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/getUploadUrlForPolicyDocVerify', async (req, res) => {
  try {
    const responseData = await policyDocVerificationAPI.getUploadUrlForPolicyDocVerify(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.delete('/rest/deleteObjects', authMiddleware, async (req, res) => {
  try {
    const responseData = await policyDocVerificationAPI.deleteObjects(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.get('/rest/checkExistFileCount', async (req, res) => {
  try {
    const responseData = await policyDocVerificationAPI.checkExistFileCount(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

app.put('/rest/getDownloadUrlForFailedDocs', async (req, res) => {
  try {
    const responseData = await policyDocVerificationAPI.getDownloadUrlForFailedDocs(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

// startMultiPart
app.post('/rest/startMultiPart', async (req, res) => {
  try {
    const responseData = await rfaFormsApi.startMultiPart(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

// uploadMultiPart
app.post('/rest/uploadMultiPart', async (req, res) => {
  try {
    const responseData = await rfaFormsApi.uploadMultiPart(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

// completeMultiPart
app.post('/rest/completeMultiPart', async (req, res) => {
  try {
    const responseData = await rfaFormsApi.completeMultiPart(req);
    return res
      .status(responseData.statusCode)
      .json(JSON.parse(responseData.body));
  } catch (err) {
    return res
      .status(err.statusCode || 500)
      .json({ error: err.message });
  }
});

module.exports.handler = serverless(app);
