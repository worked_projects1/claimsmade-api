const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const authMiddleware = require('../../auth');
const connectToDatabase = require('../../db');
const uuid = require('uuid');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const csvToJson = require('convert-csv-to-json');
const { authorizeImportClaimsAdmin } = require('./authorize');
const { validateImportClaimsAdmin } = require('./validation');
const { getS3UploadURL, getSecuredS3PublicUrl } = require('../../utils/awsService');

const importClaimsAdmins = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        authorizeImportClaimsAdmin(event.user);
        validateImportClaimsAdmin(input);
        const { ClaimsAdmins } = await connectToDatabase();

        const claimsAdmins = await new Promise(function (resolve, reject) {
            AWS.config.update({ region: process.env.S3_BUCKET_DEFAULT_REGION });
            const s3BucketParams = {
                Bucket: process.env.S3_CLAIMCONNECTS_COMMON_BUCKET,
                Key: `${input.s3_file_key}`,
            };
            s3.getObject(s3BucketParams, function (err, data) {
                if (err) {
                    console.log(err);
                    reject(err.message);
                } else {
                    const json = csvToJson.fieldDelimiter(',').csvStringToJson(data.Body.toString('utf8'));
                    resolve(json);
                }
            });
        });
        let recordsNotUpdated = [];
        let bulkRecords = [];
        for (let i = 0; i < claimsAdmins.length; i++) {
            if (claimsAdmins[i].company_name) {
                const isNameAlreadyExists = await ClaimsAdmins.findOne({
                    where: {
                        company_name: claimsAdmins[i].company_name,
                        is_claims_admin: true
                    }
                });
                if (isNameAlreadyExists && isNameAlreadyExists.company_name) {
                    recordsNotUpdated.push(claimsAdmins[i].no);
                } else {
                    const obj = {
                        id: uuid.v4(),
                        company_name: claimsAdmins[i].company_name,
                        email: null,
                        practice_id: null,
                        is_claims_admin: true,
                        address: claimsAdmins[i].address,
                        city: claimsAdmins[i].city,
                        state: claimsAdmins[i].state,
                        zip_code: claimsAdmins[i].zip_code,
                        phone_number: claimsAdmins[i].phone_number,
                        fax_number: null
                    }
                    bulkRecords.push(obj)
                }
            } else {
                recordsNotUpdated.push(claimsAdmins[i].no);
            }
        }
        if (bulkRecords.length > 0) {
            await ClaimsAdmins.bulkCreate(bulkRecords);
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Success",
                no_of_records_not_updated: recordsNotUpdated.length,
                records_not_updated: recordsNotUpdated
            })
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': false
            },
            body: JSON.stringify({ error: err.message || 'Could not import claims admins' }),
        };
    }
}

const updateRfaFormDoc = async (event) => {
    try {
        const { RfaForms } = await connectToDatabase();
        let rfaForms = await RfaForms.findAll({
            attributes: ['id']
        });
        let formDoc = {};
        let bulkData = [];
        for (let i = 0; i < rfaForms.length; i++) {

            const rfaForm = await RfaForms.findOne({ where: { id: rfaForms[i].id } });
            formDoc = rfaForm.form_doc ? JSON.parse(rfaForm.form_doc) : null;

            if (formDoc && formDoc.treatments) {
                for (let j = 0; j < formDoc.treatments.length; j++) {
                    formDoc.treatments[j].bodySystemId = formDoc.treatments[j].bodySystemNames
                    // delete formDoc.treatments[j]["bodySystemNames"];
                    // Object.assign(formDoc.treatments[j], { bodySystemNames: rfaForm.id})
                }
            }
            rfaForm.form_doc = formDoc ? JSON.stringify(formDoc) : '';
            bulkData.push(rfaForm.save());
        }
        await Promise.all(bulkData);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Success"
            })
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': false
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Users.' }),
        };
    }
}

// s3_file_key
// content_type
const getUploadUrlForClaimsAdminImport = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        const bucketName = process.env.S3_CLAIMCONNECTS_COMMON_BUCKET;
        const s3_file_key = `claimsAdmin/${input.file_name}`
        const uploadUrlRes = await getS3UploadURL(event, s3_file_key, bucketName, input.content_type, 'private', {});

        const publicUrlRes = await getSecuredS3PublicUrl(bucketName, process.env.S3_BUCKET_DEFAULT_REGION, s3_file_key);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                uploadURL: uploadUrlRes.uploadURL,
                public_url: publicUrlRes,
                s3_file_key: s3_file_key
            })
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': false
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Users.' }),
        };
    }
}

module.exports.importClaimsAdmins = middy(importClaimsAdmins).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.updateRfaFormDoc = updateRfaFormDoc;
module.exports.getUploadUrlForClaimsAdminImport = middy(getUploadUrlForClaimsAdminImport).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());