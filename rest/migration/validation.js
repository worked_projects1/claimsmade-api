const Validator = require('validatorjs');
const { HTTPError } = require('../../utils/httpResp');

exports.validateImportClaimsAdmin = function (data) {
    const rules = {
        s3_file_key: 'required'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all())
}