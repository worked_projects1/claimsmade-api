const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const authMiddleware = require('../../auth');
const connectToDataBase = require('../../db');
const uuid = require('uuid');
const { HTTPError } = require('../../utils/httpResp');
const { validateCreate, validateUpdate, validateGetOne, validateuploadUrl, validateRfaFormDetails, validateUploadMultipleFiles } = require('./validation');
const { authorizeCreate, authorizeUpdate, authorizeDelete, authorizeGetAll, authorizeGetOne, authorizeUpdateAuth } = require('./authorize');
const { QueryTypes } = require('sequelize');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const { getS3UploadURL, getDownloadUrl, getSecuredS3PublicUrl } = require('../../utils/awsService');
const PDFDocument = require('pdf-lib').PDFDocument;
const fetch = require('node-fetch');
const { appConstants } = require('../../appConstants');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);


const create = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        //authorizeCreate(event.user);

        const rfaFormbj = Object.assign(input, { id: input.id || uuid.v4(), practice_id: event.user.practice_id, created_by: event.user.id });

        validateCreate(rfaFormbj);
        const { RfaForms } = await connectToDataBase();

        const rfaFormData = await RfaForms.create(rfaFormbj);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(rfaFormData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the record.' }),
        };
    }
};


const update = async (event) => {
    try {
        const input = typeof event.body == "string" ? JSON.parse(event.body) : event.body;
        //authorizeUpdate(event.user);
        validateUpdate(input);
        const { RfaForms } = await connectToDataBase();

        const existingRecord = await RfaForms.findOne({
            where: { id: event.pathParameters.id }
        });
        if (!existingRecord) throw new HTTPError(404, 'Patient not found');
        const updateValues = Object.assign(existingRecord, input);
        const response = await updateValues.save();

        // let bodySysName = '';

        // if (response.body_system_id && response.body_system_id != '') {
        //     let body_system_id = response.body_system_id;
        //     const bodySysObj = await BodySystem.findOne({ where: { id: body_system_id } });
        //     bodySysName = bodySysObj.name;
        // }

        const resultData = {
            "case_data": {
                id: response.id,
                name: response.name,
                date_of_birth: response.date_of_birth,
                date_of_injury: response.date_of_injury,
                claim_number: response.claim_number,
                employer: response.employer
            },
            "form_doc": JSON.parse(response.form_doc),
            "request_type": response.request_type,
            "attachments": JSON.parse(response.attachments),
            "assignee_id": response.assignee_id,
            "claims_admin_id": response.claims_admin_id
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(resultData)
        }
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || `Couldn't update the Rfa form.` })
        }
    }
}


const destroy = async (event) => {
    try {
        // authorizeDelete(event.user);
        const params = event.params || event.pathParameters;
        const { RfaForms } = await connectToDataBase();
        const rfaFormObj = await RfaForms.findOne({
            where: { id: params.id }
        });
        if (!rfaFormObj) throw new HTTPError(404, `No records found`);
        rfaFormObj.is_deleted = true;
        await rfaFormObj.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'Ok',
                message: 'Patient info deleted'
            })
        }
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || `Couldn't delete record.` })
        }
    }
}

// const getAll = async (event) => {
//     try {
//         const query = event.headers;
//         //authorizeGetAll(event.user);
//         const { sequelize } = await connectToDataBase();
//         let selectQuery = `SELECT RfaForms.id, RfaForms.name, RfaForms.date_of_birth, RfaForms.date_of_injury, RfaForms.claim_number, RfaForms.employer, RfaForms.created_at, RfaForms.s3_file_key, RfaForms.form_generate_date FROM RfaForms `;

//         let countQuery = `SELECT COUNT(*) FROM RfaForms `;

//         let searchQuery = '';
//         let searchKey = '';

//         if (query.offset) query.offset = parseInt(query.offset, 10);
//         if (query.limit) query.limit = parseInt(query.limit, 10);
//         if (query.search) searchKey = query.search;

//         if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
//             searchQuery = ` RfaForms.name LIKE '%${searchKey}%' OR RfaForms.claim_number LIKE '%${searchKey}%' OR RfaForms.employer LIKE '%${searchKey}%'`;
//             searchQuery = ' WHERE (' + searchQuery + ')'
//         }

//         let sortQuery = '';
//         if (query.sort == 'false') {
//             sortQuery += ` ORDER BY created_at DESC`
//         } else if (query.sort != 'false') {
//             query.sort = JSON.parse(query.sort);

//             if (query.sort.column == 'claim_number') {
//                 sortQuery += ` ORDER BY cast(claim_number as unsigned)`;
//             } else if (query.sort.column) {
//                 sortQuery += ` ORDER BY ${query.sort.column}`;
//             }
//             if (query.sort.type) {
//                 sortQuery += ` ${query.sort.type}`;
//             }

//         }

//         let limitQuery;
//         limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset};`;

//         selectQuery = selectQuery + searchQuery + sortQuery + limitQuery;
//         countQuery = countQuery + searchQuery;

//         const data = await sequelize.query(selectQuery, {
//             type: QueryTypes.SELECT
//         });
//         const tableDataCount = await sequelize.query(countQuery, {
//             type: QueryTypes.SELECT
//         });

//         return {
//             statusCode: 200,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-Control-Allow-Credentials': true,
//                 'Access-Control-Expose-Headers': 'totalPageCount',
//                 'totalPageCount': tableDataCount[0]["COUNT(*)"]
//             },
//             body: JSON.stringify(data)
//         }
//     } catch (err) {
//         return {
//             statusCode: err.statusCode || 500,
//             headers: {
//                 'Content-Type': 'text/plain',
//                 'Access-Control-Allow-Origin': '*',
//                 'Access-control-Allow-Credentials': true
//             },
//             body: JSON.stringify({ error: err.message || `Couldn't fetch records` })
//         }
//     }
// }

const getAll = async (event) => {
    try {
        const { sequelize } = await connectToDataBase();
        let searchKey = '';
        let sortQuery = '';
        let searchQuery = '';
        const query = event.headers;

        if (query.offset) query.offset = parseInt(query.offset, 10);
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.search) searchKey = query.search;
        let practice_id = event.user.practice_id;
        if (!practice_id) throw new HTTPError(404, `Practice id: ${practice_id} was not found`);

        /**Sort**/
        if (query.sort == 'false') {
            sortQuery += ` ORDER BY RfaForms.created_at DESC`
        } else if (query.sort != 'false') {
            query.sort = JSON.parse(query.sort);

            if (query.sort.column == 'claim_number') {
                sortQuery += ` ORDER BY cast(claim_number as unsigned)`;
            } else if (query.sort.column) {
                sortQuery += ` ORDER BY ${query.sort.column}`;
            }
            if (query.sort.type) {
                sortQuery += ` ${query.sort.type}`;
            }
        }

        /**Search**/
        if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
            searchQuery = ` AND ( RfaForms.name LIKE '%${searchKey}%' OR RfaForms.claim_number LIKE '%${searchKey}%' OR RfaForms.employer LIKE '%${searchKey}%' OR ClaimsAdmins.company_name LIKE '%${searchKey}%' OR Users.name LIKE '%${searchKey}%' ) `;
        }

        let limitQuery;
        limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset};`;

        let selectQuery = `SELECT RfaForms.id, RfaForms.name, RfaForms.date_of_birth, RfaForms.date_of_injury, RfaForms.claim_number, RfaForms.employer, RfaForms.created_at, RfaForms.s3_file_key, RfaForms.form_generate_date, RfaForms.assignee_id,  RfaForms.claims_admin_id, Users.name as 'generated_by', assignee.name as 'assingee_name', ClaimsAdmins.company_name as 'claims_company_name' FROM RfaForms LEFT JOIN Users ON Users.id = RfaForms.generated_by LEFT JOIN Users AS assignee ON RfaForms.assignee_id = assignee.id LEFT JOIN ClaimsAdmins ON RfaForms.claims_admin_id = ClaimsAdmins.id WHERE ( RfaForms.practice_id = '${practice_id}' AND RfaForms.is_deleted IS NOT TRUE ) `;

        let countQuery = `SELECT RfaForms.id, RfaForms.name, RfaForms.date_of_birth, RfaForms.date_of_injury, RfaForms.claim_number, RfaForms.employer, RfaForms.created_at, RfaForms.s3_file_key, RfaForms.form_generate_date, RfaForms.assignee_id,  RfaForms.claims_admin_id, Users.name as 'generated_by', assignee.name as 'assingee_name', ClaimsAdmins.company_name as 'claims_company_name' FROM RfaForms LEFT JOIN Users ON Users.id = RfaForms.generated_by LEFT JOIN Users AS assignee ON RfaForms.assignee_id = assignee.id LEFT JOIN ClaimsAdmins ON RfaForms.claims_admin_id = ClaimsAdmins.id WHERE ( RfaForms.practice_id = '${practice_id}' AND RfaForms.is_deleted IS NOT TRUE ) `;

        selectQuery = selectQuery + searchQuery + sortQuery + limitQuery;
        countQuery = countQuery + searchQuery;

        const serverData = await sequelize.query(selectQuery, {
            type: QueryTypes.SELECT
        });

        const TableDataCount = await sequelize.query(countQuery, {
            type: QueryTypes.SELECT
        });

        //To get the Assignee name using assignee_id
        // for (let i = 0; i < serverData.length; i++) {

        //     //To get the Assignee name using assignee_id
        //     let userId = serverData[i].assignee_id;
        //     if (userId && userId != '' && userId != null) {
        //         const user = await Users.findOne({ where: { id: userId } });
        //         serverData[i].assingee_name = user.name;
        //     }

        //     //To get the generated_by name
        //     let userIdForGeneratedBy = serverData[i].generated_by;
        //     if (userIdForGeneratedBy && userIdForGeneratedBy != '') {
        //         const userObj = await Users.findOne({ where: { id: userIdForGeneratedBy } });
        //         serverData[i].generated_by = userObj.name;
        //     }

        //     //To get the Claims company name using claims_admin_id
        //     let adminId = serverData[i].claims_admin_id;
        //     if (adminId && adminId != '' && adminId != null) {
        //         const adminObj = await ClaimsAdmins.findOne({ where: { id: adminId } });
        //         if (adminObj && adminObj != '') {
        //             serverData[i].claims_company_name = adminObj.company_name;
        //         } else {
        //             serverData[i].claims_company_name = null;
        //         }
        //     }
        // }

        //To get the Claims company name using claims_admin_id
        // for (let i = 0; i < serverData.length; i++) {
        //     let adminId = serverData[i].claims_admin_id;
        //     if (adminId && adminId != '' && adminId != null) {
        //         const adminObj = await ClaimsAdmins.findOne({ where: { id: adminId } });
        //         if (adminObj && adminObj != '') {
        //             serverData[i].claims_company_name = adminObj.company_name;
        //         } else {
        //             serverData[i].claims_company_name = null;
        //         }
        //     }
        // }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': TableDataCount.length
            },
            body: JSON.stringify(serverData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the RFA Forms.' }),
        };
    }
}

const getOne = async (event) => {
    try {
        //authorizeGetOne(event.user);
        validateGetOne(event.pathParameters);
        const { RfaForms, ClaimsAdmins, Settings, Op } = await connectToDataBase();

        const rfaFormData = await RfaForms.findOne({ where: { id: event.pathParameters.id, is_deleted: { [Op.not]: true } }, });
        if (!rfaFormData) throw new HTTPError(404, `Rfa Forms with id: ${event.pathParameters.id} was not found`);

        const claimsAdmin = await ClaimsAdmins.findOne({
            where: { id: rfaFormData.claims_admin_id }
        });

        // let bodySysName = '';

        // //To get the bodySystem Name using id
        // if (rfaFormData.body_system_id && rfaFormData.body_system_id != '') {
        //     let body_system_id = rfaFormData.body_system_id;
        //     const bodySysObj = await BodySystem.findOne({ where: { id: body_system_id } });
        //     bodySysName = bodySysObj.name;
        // }

        const settingsObj = await Settings.findOne({
            where: { key: 'global_settings' }
        });
        //   if (!settingsObj) throw new HTTPError(400, 'Settings data not found');

        let policy_doc_updated_date = new Date();
        if (settingsObj && settingsObj.value) {
            let parseValue = JSON.parse(settingsObj.value)
            if (parseValue && parseValue.policy_doc_updated_date) {
                policy_doc_updated_date = parseValue.policy_doc_updated_date;
            }
        }

        const resultData = {
            "case_data": {
                id: rfaFormData.id,
                name: rfaFormData.name,
                date_of_birth: rfaFormData.date_of_birth,
                date_of_injury: rfaFormData.date_of_injury,
                claim_number: rfaFormData.claim_number,
                employer: rfaFormData.employer
            },
            "form_doc": JSON.parse(rfaFormData.form_doc),
            "request_type": rfaFormData.request_type,
            "attachments": JSON.parse(rfaFormData.attachments),
            "assignee_id": rfaFormData.assignee_id,
            "claims_admin_id": rfaFormData.claims_admin_id,
            "is_claims_admin": claimsAdmin ? claimsAdmin.is_claims_admin : null,
            "policy_doc_updated_date": policy_doc_updated_date
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(resultData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the records.' }),
        };
    }
};

const getSignedUrlForS3 = async (input, event) => {
    let fileExtention = '';
    if (input.file_name) {
        fileExtention = `.${input.file_name.split('.').pop()}`;
    }

    let Timestamp = Date.now();

    const s3Params = {
        Bucket: process.env.S3_BUCKET_FOR_RFA_FORM,
        Key: `${Timestamp}${fileExtention}`,
        ContentType: input.content_type,
        ACL: 'private',
        Metadata: {
            user_id: event.user.id
        },
    };
    return new Promise((resolve, reject) => {
        const uploadURL = s3.getSignedUrl('putObject', s3Params);
        resolve({
            uploadURL,
            s3_file_key: s3Params.Key,
            public_url: `https://${s3Params.Bucket}.s3.amazonaws.com/${s3Params.Key}`
        });
    });
};

const getUploadUrl = async (event) => {
    try {

        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        validateuploadUrl(input);

        const uploadURLObject = await getSignedUrlForS3(input, event);
        const publicUrl = await getSecuredS3PublicUrl(process.env.S3_BUCKET_FOR_RFA_FORM, process.env.S3_BUCKET_DEFAULT_REGION, uploadURLObject.s3_file_key);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
                public_url: publicUrl
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the Treatments.' }),
        };
    }
};

const getSecuredPublicUrl = async (event) => {
    try {
        // authorizeGetOne(event.user);
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        AWS.config.update({ region: process.env.S3_BUCKET_FOR_RFA_FORM_REGION });
        const publicUrl = await s3.getSignedUrlPromise('getObject', {
            Bucket: process.env.S3_BUCKET_FOR_RFA_FORM,
            Expires: 60 * 60 * 1,
            Key: input.s3_file_key,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                publicUrl,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
};

const updateRfaFormDetails = async (event) => {
    try {
        const input = typeof event.body === "string" ? JSON.parse(event.body) : event.body;
        validateRfaFormDetails(input);
        const { RfaForms, ClaimsAdmins, Users, Op } = await connectToDataBase();
        const rfaFormObject = await RfaForms.findOne({
            where: { id: input.id, is_deleted: { [Op.not]: true } },
        });
        if (!rfaFormObject) throw new HTTPError(400, "record not found");

        let attachmentObj = {
            cover_page: input.attachments.cover_page ? input.attachments.cover_page : [],
            clinical_notes: input.attachments.clinical_notes ? input.attachments.clinical_notes : [],
            imaging_reports: input.attachments.imaging_reports ? input.attachments.imaging_reports : [],
            supporting_docs: input.attachments.supporting_docs ? input.attachments.supporting_docs : [],
            non_confirming_claims: input.attachments.non_confirming_claims ? input.attachments.non_confirming_claims : []
        }
        rfaFormObject.attachments = JSON.stringify(attachmentObj);
        // rfaFormObject.s3_file_key = input.s3_file_key;
        if (input.form_doc) rfaFormObject.form_doc = input.form_doc;
        if (input.request_type) rfaFormObject.request_type = input.request_type;
        if (input.claims_admin_id && input.claims_admin_id != '') rfaFormObject.claims_admin_id = input.claims_admin_id;
        // rfaFormObject.form_generate_date = new Date();

        //Physician Assignee
        if (event.user.role == 'physician') {
            rfaFormObject.assignee_id = event.user.id;
        } else if (input.assignee_id && input.assignee_id != '') {
            rfaFormObject.assignee_id = input.assignee_id;
        }

        if (input.claim_admin_details && input.claim_admin_details != '') {

            const claim_admin_details = typeof input.claim_admin_details === 'string' ? JSON.parse(input.claim_admin_details) : input.claim_admin_details;

            // const isAlreadyExist = await ClaimsAdmins.findOne({
            //     where: { email: claim_admin_details.email }
            // });
            // if (isAlreadyExist) throw new HTTPError(400, `Claims Administarator with Email: ${claim_admin_details.email} is already exists`);

            // const isNameAlreadyExist = await ClaimsAdmins.findOne({
            //     where: { company_name: claim_admin_details.company_name }
            // });
            // if (isNameAlreadyExist) throw new HTTPError(400, `Claims Administarator with Name: ${claim_admin_details.company_name} is already exists`);

            const responseObj = Object.assign(claim_admin_details, { id: uuid.v4(), is_claims_admin: false, practice_id: event.user.practice_id, created_by: event.user.id });
            const claimsAdminObj = await ClaimsAdmins.create(responseObj);

            rfaFormObject.claims_admin_id = claimsAdminObj.id;
        }

        const resObj = await rfaFormObject.save();

        let user;
        let userName = null;

        if (resObj && resObj.generated_by) {
            user = await Users.findOne({ where: { id: resObj.generated_by } });
        }

        if (user && user.name) {
            userName = user.name;
        }

        const resultData = Object.assign(resObj, { form_doc: JSON.parse(resObj.form_doc) }, { attachments: JSON.parse(resObj.attachments) }, { generated_by: userName });

        const claimsAdminData = await ClaimsAdmins.findOne({ where: { id: resultData.claims_admin_id } });

        const resultObj = await resultData.get({ plain: true });

        resultObj.is_claims_admin = claimsAdminData.is_claims_admin;

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(resultObj),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not generate the RFA form.",
            }),
        };
    }
};

const getUploadUrlForAttachment = async (event) => {
    try {

        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        let practice_id = event.user.practice_id;

        let bucketKey = `${practice_id}/${input.rfa_form_id}/${input.path_identifier}/${input.file_name}`;
        let bucketName = process.env.S3_BUCKET_FOR_RFA_FORM_ATTACHMENTS;

        // get accelerated endpoint url
        const s3Params = {
            Bucket: bucketName, // S3 bucket name
            Key: bucketKey, //Should be unique (eg: timeStamp)
            ContentType: input.content_type,  // application/pdf // application/json
            ACL: 'private'  // 'private' or 'public-read'
        };
        const s3_bucket = new AWS.S3({ useAccelerateEndpoint: true });

        const uploadURLObject = await new Promise((resolve, reject) => {
            const uploadURL = s3_bucket.getSignedUrl('putObject', s3Params);
            resolve({
                uploadURL,
                s3_file_key: s3Params.Key,
            });
        });

        const publicUrl = await getSecuredS3PublicUrl(bucketName, appConstants.S3_BUCKET_DEFAULT_REGION, bucketKey);
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
                publicUrl: publicUrl
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the upload URL.' }),
        };
    }
};

const uploadContentToS3 = async (input) => {
    try {
        let Timestamp = Date.now();
        const s3Params = {
            Bucket: process.env.S3_BUCKET_FOR_RFA_FORM,
            Key: `${Timestamp}.pdf`,
            Body: input.content,
            ContentType: 'application/pdf',
            ACL: 'private',
            Metadata: {
                user_id: input.id
            },
        };
        return new Promise(function (resolve, reject) {
            s3.upload(s3Params, function (err, data) {
                if (err) {
                    console.log(err);
                    reject(err.message);
                } else {
                    const data = {
                        s3_file_key: s3Params.Key
                    }
                    resolve(data);
                }
            });
        });
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
};

const uploadMultipleFiles = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        // need to add input validation here
        validateUploadMultipleFiles(input);
        const price = input.price;
        const plan = input.plan;
        const { RfaForms, Orders, Practices, ClaimsAdmins } = await connectToDataBase();
        const fileUrls = input.fileUrls;
        const mergedPdf = await PDFDocument.create();
        const case_data = input.case_data;

        for (let i = 0; i < fileUrls.length; i++) {
            const pdf = await PDFDocument.load(await getFileContent(fileUrls[i]));
            const copiedPages = await mergedPdf.copyPages(pdf, pdf.getPageIndices());
            copiedPages.forEach((page) => {
                mergedPdf.addPage(page);
            });
        }
        const buf = await mergedPdf.save();
        // const uploadRes = await uploadContentToS3({ id: event.user.id, content: new Buffer(buf) });
        const uploadRes = await uploadContentToS3({ id: event.user.id, content: Buffer.from(buf) });
        const rfaForm = await RfaForms.findOne({ where: { id: input.id } });
        rfaForm.s3_file_key = uploadRes.s3_file_key;
        rfaForm.form_generate_date = new Date();
        rfaForm.generated_by = event.user.id;
        await rfaForm.save();
        const genDocObj = await getDownloadUrl(process.env.S3_BUCKET_FOR_RFA_FORM, uploadRes.s3_file_key,
            '', process.env.S3_BUCKET_FOR_RFA_FORM_REGION);

        //claims_admin_record
        const claimsAdminData = await ClaimsAdmins.findOne({ where: { id: input.claims_admin_id } });

        //orders
        let orderData = {
            id: uuid.v4(),
            order_date: new Date(),
            status: appConstants.ORDER_STATUS_COMPLETED,
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            activity_id: rfaForm.id,
            activity_type: appConstants.ORDER_TYPE_RFA,
            amount_charged: '',
            charge_id: '',
            plan_type: plan,
            patient_name: case_data.name,
            claim_number: case_data.claim_number,
            employer: case_data.employer,
            claims_admin_name: claimsAdminData.company_name ? claimsAdminData.company_name : null,
            claims_admin_id: input.claims_admin_id
        }

        if (appConstants.PLAN_PAY_AS_YOU_GO === plan && price != 0) {
            const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });

            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });

            const paymentMethod = paymentMethods.data[0];
            const paymentIntent = await stripe.paymentIntents.create({
                amount: parseInt(price * 100),
                currency: process.env.STRIPE_CURRENCY,
                customer: practiceObject.stripe_customer_id,
                payment_method: paymentMethod.id,
                off_session: true,
                confirm: true,
            });
            orderData.charge_id = paymentIntent.id;
            orderData.amount_charged = price;
        } else {
            orderData.amount_charged = 0;
        }
        await Orders.create(orderData);

        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify(genDocObj.body),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            },
            body: JSON.stringify({
                error: err.message || "Could not generate the RFA form.",
            }),
        };
    }
}

const readSecuredFile = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        return new Promise(function (resolve, reject) {
            AWS.config.update({ region: process.env.S3_BUCKET_DEFAULT_REGION });
            const s3BucketParams = {
                Bucket: process.env.S3_BUCKET_FOR_RFA_FORM_ATTACHMENTS,
                Key: input.s3_file_key,
            };
            s3.getObject(s3BucketParams, function (err, data) {
                if (err) {
                    console.log(err);
                    reject(err.message);
                } else {
                    //var data = data.Body.toString('base64');
                    var data = Buffer.from(data.Body).toString('base64');
                    resolve(data);
                }
            });
        });
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not Read the Documents.' }),
        };
    }
}

const readFileContent = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        const fileText = await readSecuredFile(event);
        const public_url = `https://${process.env.S3_BUCKET_FOR_RFA_FORM_ATTACHMENTS}.s3.amazonaws.com/${input.s3_file_key}`;


        const resultdata = {
            "base64_data": fileText,
            "public_url": public_url
        };


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(resultdata),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not read the file content.' }),
        };
    }
};


const getPublicUrl = async (event) => {
    try {
        // authorizeGetOne(event.user);
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        AWS.config.update({ region: process.env.S3_BUCKET_DEFAULT_REGION });
        const publicUrl = await s3.getSignedUrlPromise('getObject', {
            Bucket: process.env.S3_BUCKET_FOR_RFA_FORM_ATTACHMENTS,
            Expires: 60 * 60 * 1,
            Key: input.s3_file_key,
        });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                publicUrl,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
};

const getFileContent = async (url) => {
    try {
        const response = await fetch(url);
        const arrayBuffer = await response.arrayBuffer();
        const buffer = Buffer.from(arrayBuffer);
        return buffer
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || '.' }),
        };
    }
}

// type
// file_name
const startMultiPart = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;

        let bucketName = '';
        if (input.type == 'attachment') {
            bucketName = process.env.S3_BUCKET_FOR_RFA_FORM_ATTACHMENTS
        } else if (input.type == 'merge') {
            bucketName = process.env.S3_BUCKET_FOR_RFA_FORM
        } else {
            throw new HTTPError(400, `forbidden`);
        }

        const params = {
            Key: input.file_name,
            Bucket: bucketName,
        };
        const res = await new Promise((resolve, reject) => {
            s3.createMultipartUpload(params, (err, data) => {
                if (err) return reject(err);
                return resolve(data);
            });
        })
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                s3_file_key: res.Key,
                upload_id: res.UploadId
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
}


// key
// body
// partNumber
// uploadId
// type
const uploadMultiPart = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        const parts = input.parts;
        let bucketName = '';
        if (input.type == 'attachment') {
            bucketName = process.env.S3_BUCKET_FOR_RFA_FORM_ATTACHMENTS
        } else if (input.type == 'merge') {
            bucketName = process.env.S3_BUCKET_FOR_RFA_FORM
        } else {
            throw new HTTPError(400, `forbidden`);
        }

        let promises = [];
        for (let i = 0; i < parts; i++) {
            const s3Params = {
                Bucket: bucketName,
                Key: input.s3_file_key,
                UploadId: input.uploadId,
                PartNumber: i + 1,
                ContentType: input.content_type,  // application/pdf // application/json
            };
            const s3_bucket = new AWS.S3({ useAccelerateEndpoint: true });
            promises.push(s3_bucket.getSignedUrl('uploadPart', s3Params));
        }
        const upldPartRes = await Promise.all(promises);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(upldPartRes),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload.' }),
        };
    }
}

// key
// uploadId
// multipartUploadObject
// type
const completeMultiPart = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;

        let bucketName = '';
        if (input.type == 'attachment') {
            bucketName = process.env.S3_BUCKET_FOR_RFA_FORM_ATTACHMENTS
        } else if (input.type == 'merge') {
            bucketName = process.env.S3_BUCKET_FOR_RFA_FORM
        } else {
            throw new HTTPError(400, `forbidden`);
        }

        const params = {
            Key: input.key,
            Bucket: bucketName,
            UploadId: input.uploadId,
            MultipartUpload: {
                Parts: input.parts,
            },
        };
        await new Promise((resolve, reject) => {
            s3.completeMultipartUpload(params, (err, data) => {
                if (err) return reject(err);
                return resolve(data);
            });
        });

        const publicUrl = await getSecuredS3PublicUrl(bucketName, process.env.S3_BUCKET_DEFAULT_REGION, input.key);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(publicUrl),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not upload the legalForms.' }),
        };
    }
}


module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = middy(getAll).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getUploadUrl = middy(getUploadUrl).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getSecuredPublicUrl = middy(getSecuredPublicUrl).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.updateRfaFormDetails = updateRfaFormDetails;
module.exports.getUploadUrlForAttachment = middy(getUploadUrlForAttachment).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.uploadMultipleFiles = middy(uploadMultipleFiles).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.readFileContent = middy(readFileContent).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getPublicUrl = middy(getPublicUrl).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.startMultiPart = startMultiPart;
module.exports.uploadMultiPart = uploadMultiPart;
module.exports.completeMultiPart = completeMultiPart;
