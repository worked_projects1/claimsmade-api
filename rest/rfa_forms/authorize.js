const { HTTPError } = require('../../utils/httpResp');
const majorRoles = ['superAdmin'];
const { appConstants } = require('./../../appConstants');

exports.authorizeCreate = function (user) {
    if (!majorRoles.includes(user.role)) {
        throw new HTTPError(403, 'you don\'t have access');
    }
};

exports.authorizeUpdate = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(400, 'You don\'t have access')
}

exports.authorizeDelete = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(400, 'You don\'t have access')
}

exports.authorizeGetAll = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(400, 'You don\'t have access')
}

exports.authorizeGetOne = function (user) {
    if (!majorRoles.includes(user.role)) {
        throw new HTTPError(403, 'you don\'t have access');
    }
};
