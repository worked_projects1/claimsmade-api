

const { HTTPError } = require("../../utils/httpResp");
const Validator = require('validatorjs');

exports.validateCreate = function (data) {
    const rules = {
        name: 'required|min:4|max:64',
        claim_number: 'required',
        date_of_injury: 'required'
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all());
}

exports.validateUpdate = function (data) {
    const rules = {
        name: 'required'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all());
}

exports.validateGetOne = function (data) {
    const rules = {
        id: 'required',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateuploadUrl = function (data) {
    const rules = {
        file_name: 'required',
        content_type: 'required'
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};

exports.validateRfaFormDetails = function (data) {
    const rules = {
        id: 'required'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
}

exports.validateUploadMultipleFiles = function (data) {
    const rules = {
        price: 'required',
        plan: 'required'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
}