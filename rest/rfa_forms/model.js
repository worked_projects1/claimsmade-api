module.exports = (sequelize, type) => sequelize.define('RfaForms', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    name: type.STRING,
    date_of_birth: type.DATE,
    date_of_injury: type.DATE,
    claim_number: type.STRING,
    employer: type.STRING,
    form_doc: type.TEXT,
    s3_file_key: type.STRING,
    form_generate_date: type.DATE,
    practice_id: type.STRING,
    created_by: type.STRING,
    request_type: {
        type: type.STRING,
        defaultValue: null,
        get: function () {
            return JSON.parse(this.getDataValue('request_type'));
        },
        set: function (val) {
            return this.setDataValue('request_type', JSON.stringify(val));
        }
    },
    assignee_id: type.STRING,
    attachments: type.TEXT,
    claims_admin_id: type.STRING,
    generated_by: type.STRING,
    is_deleted: type.BOOLEAN
}, {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
});
