const uuid = require('uuid');
const { appConstants } = require('../../appConstants');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const { sendEmail } = require('../../utils/mailModule');
const { QueryTypes } = require('sequelize');
const {
    validateGetOne,
    validateRead
} = require('./validation');

const create = async (event) => {
    try {
        let id;
        const emailText = event.user.email;
        let userName = '';
        let newPlan = '';
        const { Practices, Plans, Subscriptions, SubscriptionHistory } = await connectToDatabase();

        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        let page = input.page;
        if (event.user.practice_id) {
            input.practice_id = event.user.practice_id;
        }

        const dataObject = Object.assign(input, { id: id || uuid.v4() });

        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        userName = practiceObject.name;


        if (!practiceObject.stripe_customer_id) {
            throw new HTTPError(400, 'Please provide the credit card information on Credit Card Info settings and try again.');
        }

        const planObject = await Plans.findOne({ where: { id: dataObject.plan_id, active: true }, raw: true });
        newPlan = planObject.plan_type;

        const subscription = await stripe.subscriptions.create({
            customer: practiceObject.stripe_customer_id,
            items: [
                { price: dataObject.stripe_price_id },  //need to check
            ],
            metadata: {
                practice_id: event.user.practice_id,
                user_email: event.user.email,
                price_id: dataObject.stripe_price_id,
                stripe_product_id: planObject.stripe_product_id,
            },
        });

        if (subscription && subscription.status && subscription.status === 'incomplete') {
            throw new HTTPError(400, 'Your bank has declined the transaction, please resolve the issue with bank or try again with different card');
        }

        const subscriptionValidity = new Date(parseInt(subscription.current_period_end) * 1000);


        let subscriptionObj = await Subscriptions.create({
            id: dataObject.id,
            practice_id: dataObject.practice_id,
            subscribed_by: event.user.id,
            subscribed_on: new Date(),
            plan_id: dataObject.plan_id,
            stripe_subscription_id: subscription.id,
            stripe_subscription_data: JSON.stringify(subscription),
            subscription_meta_data: '',
            stripe_product_id: planObject.stripe_product_id,
            subscribed_valid_till: subscriptionValidity,
        });

        const subscriptionHistoryData = {
            id: uuid.v4(),
            order_date: new Date(),
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            subscribed_by: event.user.name,
            price: dataObject.price,
            subscribed_on: new Date(),
            plan_type: dataObject.selected_plan_type,
            payment_type: 'New',
            status: 'Success',
            stripe_subscription_id: subscription.id,
            stripe_subscription_data: JSON.stringify(subscription),
            stripe_product_id: planObject.stripe_product_id,
            subscription_valid_till: subscriptionValidity
        };

        subscriptionHistoryObj = await SubscriptionHistory.create(subscriptionHistoryData);

        let bodyTxt = '';
        let subject = '';
        let firstPoint = '';
        let welcome_content = '<p>Welcome To VettedClaims.</p>';


        if (newPlan == 'monthly') {

            subject = `Your ${appConstants.APP_NAME} Subscription`;
            firstPoint = '<p>As a subscriber to the monthly unlimited plan, users in your organization can generate an unlimited number of RFAs/Forms.</p>';

            bodyTxt = `${welcome_content}` + `${firstPoint}` +
                '<p>Your credit card on file will be billed every month per our terms of service. You can cancel at anytime by emailing us at support@vettedclaims.com.</p>';
        } else if (newPlan == 'yearly') {

            subject = `Your ${appConstants.APP_NAME} Subscription`;

            firstPoint = '<p>As a subscriber to the yearly unlimited plan, users in your organization can generate an unlimited number of RFAs/Forms.</p>';

            bodyTxt = `${welcome_content}` + `${firstPoint}` +
                '<p>Your credit card on file will be billed every year per our terms of service. You can cancel at anytime by emailing us at support@vettedclaims.com.</p>';
        }
        await sendEmail(emailText, subject, `Dear ${userName}, <br/> 
           ${bodyTxt} <br/>`, appConstants.APP_NAME);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the subscriptionModelObject.' }),
        };
    }
};


const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        validateGetOne(params);
        const { Subscriptions } = await connectToDatabase();
        const subscriptionModelObject = await Subscriptions.findOne({ where: { id: params.id }, order: [['created_at', 'DESC']] });
        if (!subscriptionModelObject) throw new HTTPError(404, `Subscriptions with id: ${params.id} was not found`);
        const plainCustomerObjection = subscriptionModelObject.get({ plain: true });
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCustomerObjection),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Subscription.' }),
        };
    }
};

const update = async (event) => {
    try {
        const practiceID = event.user.practice_id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        let payment_type;
        let subscriptionValidity;
        let price = '0';
        let plan_type;
        let stripe_subscription_id;
        let stripe_subscription_data = undefined;
        let stripe_product_id;
        let activity_type = 'DEFAULT';
        let userName = '';
        let table_update = true;


        const { Subscriptions, Plans, Practices, SubscriptionHistory } = await connectToDatabase();
        const newPlanObject = await Plans.findOne({ where: { id: input.new_plan_id, active: true } });
        const subscriptionModelObject = await Subscriptions.findOne({ where: { id: params.id }, order: [['created_at', 'DESC']] });

        const practiceModelObject = await Practices.findOne({ where: { id: practiceID } });
        userName = practiceModelObject.name;

        if (!subscriptionModelObject) throw new HTTPError(404, `Subscriptions with id: ${params.id} was not found`);


        if (input.cancel_at_period_end === true) {

            const subscription = await stripe.subscriptions.update(
                subscriptionModelObject.stripe_subscription_id,
                { cancel_at_period_end: true }
            );
            subscriptionModelObject.stripe_subscription_data = JSON.stringify(subscription);
            payment_type = 'Subscription Canceled';

            const exsistingPlanObject = await Plans.findOne({ where: { stripe_product_id: subscriptionModelObject.stripe_product_id, active: true } });

            if (exsistingPlanObject && exsistingPlanObject.price) {
                price = exsistingPlanObject.price;
                plan_type = exsistingPlanObject.plan_type;
                stripe_product_id = exsistingPlanObject.stripe_product_id;
            }

            const previousSubscriptionObj = await SubscriptionHistory.findOne({
                where: { practice_id: event.user.practice_id },
                order: [['created_at', 'DESC']],
                raw: true
            });

            if (previousSubscriptionObj && previousSubscriptionObj.payment_type == 'Subscription Canceled') {
                table_update = false;
            }

            stripe_subscription_id = subscription.id;
            stripe_subscription_data = JSON.stringify(subscription);
            activity_type = 'DOWNGRADED';

            subscriptionValidity = new Date(parseInt(subscription.current_period_end) * 1000);

        } else if (input.request_type == 'upgrade') {

            const planObject = await Plans.findOne({ where: { id: input.new_plan_id, active: true } });

            const existingSubscription = await stripe.subscriptions.retrieve(subscriptionModelObject.stripe_subscription_id);

            const exsistingPlanObject = await Plans.findOne({ where: { id: subscriptionModelObject.plan_id, active: true } });

            const subscription = await stripe.subscriptions.update(
                subscriptionModelObject.stripe_subscription_id,
                {
                    billing_cycle_anchor: 'now',
                    cancel_at_period_end: false,
                    proration_behavior: 'create_prorations',
                    items: [{
                        id: existingSubscription.items.data[0].id,
                        price: input.new_price_id,
                    }],
                    metadata: {
                        practice_id: event.user.practice_id,
                        user_email: event.user.email,
                        price_id: input.new_price_id,
                        stripe_product_id: planObject.stripe_product_id,
                        plan_name: planObject.name,
                        latest_actvity: 'upgraded',
                    },
                }
            );

            if (
                (exsistingPlanObject.plan_type == 'monthly' && planObject.plan_type == 'monthly') ||
                (exsistingPlanObject.plan_type == 'yearly' && planObject.plan_type == 'yearly')
            ) {
                payment_type = 'Renewal';
            } else {
                payment_type = 'New';
            }

            if (planObject.price) price = planObject.price;
            if (planObject.plan_type) plan_type = planObject.plan_type;
            stripe_subscription_id = subscription.id;
            stripe_subscription_data = JSON.stringify(subscription);
            stripe_product_id = planObject.stripe_product_id;

            subscriptionModelObject.plan_id = input.new_plan_id;
            subscriptionModelObject.stripe_product_id = planObject.stripe_product_id;
            subscriptionModelObject.stripe_subscription_data = JSON.stringify(subscription);
            subscriptionModelObject.subscribed_valid_till = new Date(parseInt(subscription.current_period_end) * 1000);

            subscriptionValidity = new Date(parseInt(subscription.current_period_end) * 1000);

        } else if (input.request_type == 'downgrade') {
            const planObject = await Plans.findOne({ where: { id: input.new_plan_id, active: true } });
            const existingSubscription = await stripe.subscriptions.retrieve(subscriptionModelObject.stripe_subscription_id);
            // table_update = true;

            const subscription = await stripe.subscriptions.update(
                subscriptionModelObject.stripe_subscription_id,
                {
                    cancel_at_period_end: false,
                    items: [{
                        id: existingSubscription.items.data[0].id,
                        price: input.new_price_id,
                    }],
                    metadata: {
                        practice_id: event.user.practice_id,
                        user_email: event.user.email,
                        price_id: input.new_price_id,
                        stripe_product_id: planObject.stripe_product_id,
                        plan_name: planObject.name,
                        latest_actvity: 'downgraded',
                    },
                }
            );
            subscriptionModelObject.plan_id = input.new_plan_id;

            if (!subscriptionModelObject.stripe_product_id) {
                subscriptionModelObject.stripe_product_id = planObject.stripe_product_id;
            }
            subscriptionModelObject.stripe_subscription_data = JSON.stringify(subscription);
            subscriptionModelObject.subscribed_valid_till = new Date(parseInt(subscription.current_period_end) * 1000);
            payment_type = 'Subscription Canceled';
            if (planObject.price) price = planObject.price;
            if (planObject.plan_type) plan_type = planObject.plan_type;
            stripe_subscription_id = subscription.id;
            stripe_subscription_data = JSON.stringify(subscription);
            stripe_product_id = planObject.stripe_product_id;
            activity_type = 'DOWNGRADED';

            subscriptionValidity = new Date(parseInt(subscription.current_period_end) * 1000);

            // const subscriptionHistoryData = {
            //     id: uuid.v4(),
            //     order_date: new Date(),
            //     practice_id: event.user.practice_id,
            //     user_id: event.user.id,
            //     subscribed_by: event.user.name,
            //     price: planObject.price,
            //     plan_type: planObject.plan_type,
            //     subscribed_on: new Date(),
            //     plan_id: input.new_plan_id,
            //     stripe_subscription_id: subscription.id,
            //     subscription_valid_till: subscriptionValidity,
            //     stripe_subscription_data: JSON.stringify(subscription),
            //     stripe_product_id: planObject.stripe_product_id,
            //     esquiretek_activity_type: 'DOWNGRADED',
            //     status: "Success"
            // };
            // await SubscriptionHistory.create(subscriptionHistoryData);
        }

        await subscriptionModelObject.save();

        //Subscription History
        const subscriptionHistoryData = {
            id: uuid.v4(),
            order_date: new Date(),
            practice_id: event.user.practice_id,
            user_id: event.user.id,
            subscribed_by: event.user.name,
            price: price,
            plan_type: plan_type,
            subscribed_on: new Date(),
            stripe_subscription_id: stripe_subscription_id,
            subscription_valid_till: subscriptionValidity,
            stripe_subscription_data: stripe_subscription_data,
            stripe_product_id: stripe_product_id,
            activity_type: activity_type,
            status: "Success",
            payment_type: payment_type
        };
        if (table_update) {
            await SubscriptionHistory.create(subscriptionHistoryData);
        }
        let subject = '';
        let bodyTxt = '';
        const emailText = event.user.email;
        let planType = '';
        let period = '';
        let welcome_content = '<p>Welcome To VettedClaims.</p>';

        if (plan_type == "yearly") {
            planType = 'yearly unlimited plan'
            period = 'year';
        } else if (plan_type == "monthly") {
            planType = 'monthly unlimited plan'
            period = 'month';
        }
        if (newPlanObject.plan_type != 'pay_as_you_go') {
            subject = `Your ${appConstants.APP_NAME} Subscription`;
            bodyTxt = `${welcome_content}` + `<p>As a subscriber to the ${planType}, users in your organization can generate an unlimited number of RFAs/Forms.
        <p style="marigin-top:0px">Your credit card on file will be billed every ${period} per our terms of service. You can cancel at anytime by emailing us at support@vettedclaims.com.</p>`;

            await sendEmail(emailText, subject, `Dear ${userName}, <br/> 
          ${bodyTxt} <br/>`, `${appConstants.APP_NAME}`);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'Successfully processed the request',
            }),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Subscription.' }),
        };
    }
};

const adminGetAll = async (event) => {
    try {
        const query = event.headers;
        const { sequelize } = await connectToDatabase();
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.offset) query.offset = parseInt(query.offset, 10);

        let selectQuery = `SELECT SubscriptionHistories.id, Users.name as 'user_name', SubscriptionHistories.plan_type, SubscriptionHistories.price, SubscriptionHistories.order_date, SubscriptionHistories.subscription_valid_till, SubscriptionHistories.payment_type, Practices.name as 'practice_name' FROM SubscriptionHistories LEFT JOIN Users ON SubscriptionHistories.user_id = Users.id LEFT JOIN Practices ON SubscriptionHistories.practice_id = Practices.id WHERE Users.is_deleted IS NOT TRUE`;

        let countQuery = `SELECT COUNT(*) FROM SubscriptionHistories LEFT JOIN Users ON SubscriptionHistories.user_id = Users.id LEFT JOIN Practices ON SubscriptionHistories.practice_id = Practices.id WHERE Users.is_deleted IS NOT TRUE`;


        //filter
        let filterQuery = '';
        const filterObj = JSON.parse(query.filter)
        if (filterObj.practice_name != "all") {
            filterQuery = ` AND Practices.name = '${filterObj.practice_name}'`
        }
        if (filterObj.payment_type != "all") {
            filterQuery += ` AND SubscriptionHistories.payment_type = '${filterObj.payment_type}'`
        }

        //search
        let searchQuery = '';
        let searchKey = query.search;
        if (query.search != "false") {
            searchQuery = ` AND ( Users.name LIKE '%${searchKey}%' OR SubscriptionHistories.plan_type LIKE '%${searchKey}%' OR Practices.name LIKE '%${searchKey}%' OR SubscriptionHistories.payment_type LIKE '%${searchKey}%' OR SubscriptionHistories.price LIKE '%${searchKey}%' )`;
        } else {
            searchQuery = ''
        }


        //sort
        let sortQuery = '';
        if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            sortQuery = ` ORDER BY ${query.sort.column} ${query.sort.type}`;
        } else {
            sortQuery = ` ORDER BY SubscriptionHistories.created_at DESC`;
        }

        //limit
        let limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset};`;

        selectQuery += filterQuery + searchQuery + sortQuery + limitQuery;
        countQuery += filterQuery + searchQuery;

        let data = await sequelize.query(selectQuery, {
            type: QueryTypes.SELECT
        });

        let totalPageCount = await sequelize.query(countQuery, {
            type: QueryTypes.SELECT
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': totalPageCount[0]["COUNT(*)"]
            },
            body: JSON.stringify(data)
        }
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the records.' }),
        };
    }
}

const getAll = async (event) => {
    try {
        let header = event.headers;
        const query = event.queryStringParameters || {};
        if (header.offset) query.offset = parseInt(header.offset, 10);
        if (header.limit) query.limit = parseInt(header.limit, 10);
        if (query.where) query.where = JSON.parse(query.where);
        if (!query.where) {
            query.where = {};
        }

        query.where.practice_id = event.user.practice_id;
        query.order = [
            ['created_at', 'DESC'],
        ];
        validateRead(query);
        query.raw = true;
        query.limit = 1;
        query.logging = console.log;
        const { Subscriptions, Plans } = await connectToDatabase();
        let subscriptionModelObjects = await Subscriptions.findAll(query);

        if (!subscriptionModelObjects.length) {
            subscriptionModelObjects = [];
            const activePlan = await Plans.findOne({
                where: { plan_type: 'pay_as_you_go', active: true },
            });
            subscriptionModelObjects = [
                {
                    id: 'pay_as_you_go',
                    plan_type: 'pay_as_you_go',
                    planDetails: activePlan,
                },
            ];
        } else {
            for (let i = 0; i < 1; i += 1) {
                if (subscriptionModelObjects[i] && subscriptionModelObjects[i].plan_id) {
                    const stripe_subscription_data = JSON.parse(subscriptionModelObjects[i].stripe_subscription_data);
                    if (stripe_subscription_data.current_period_end && stripe_subscription_data.cancel_at_period_end == true) {
                        const current_period_end = unixTimetoDateconvert(stripe_subscription_data.current_period_end);
                        const current_date = new Date();
                        if (current_period_end < current_date) {
                            const activePlanCheck = await Plans.findOne({
                                where: { plan_type: 'pay_as_you_go', active: true },
                            });
                            subscriptionModelObjects = [
                                {
                                    id: 'pay_as_you_go',
                                    plan_id: 'pay_as_you_go',
                                    planDetails: activePlanCheck,
                                },
                            ];
                        } else {
                            const activePlan = await Plans.findOne({
                                where: { stripe_product_id: subscriptionModelObjects[i].stripe_product_id, active: true }
                            });
                            subscriptionModelObjects[i].planDetails = activePlan;
                            subscriptionModelObjects[i].stripe_subscription_data = JSON.parse(subscriptionModelObjects[i].stripe_subscription_data);
                        }
                    } else if (stripe_subscription_data.status == 'canceled' || stripe_subscription_data.status == 'past_due') {

                        const activePlaninsideif = await Plans.findOne({
                            where: { plan_type: 'pay_as_you_go', active: true },
                        });
                        subscriptionModelObjects = [
                            {
                                id: 'pay_as_you_go',
                                plan_type: 'pay_as_you_go',
                                planDetails: activePlaninsideif,
                            },
                        ];
                    } else {
                        const activePlan = await Plans.findOne({
                            where: { stripe_product_id: subscriptionModelObjects[i].stripe_product_id, active: true }
                        });
                        subscriptionModelObjects[i].planDetails = activePlan;
                        subscriptionModelObjects[i].stripe_subscription_data = JSON.parse(subscriptionModelObjects[i].stripe_subscription_data);
                    }
                }
            }
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                data: subscriptionModelObjects,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the records.' }),
        };
    }
};


// User
// Plan Type
// Price
// Order Date
// Valid upto
// Type
const getAllHistory = async (event) => {
    try {
        const query = event.headers;
        const { sequelize } = await connectToDatabase();
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.offset) query.offset = parseInt(query.offset, 10);

        let selectQuery = `SELECT SubscriptionHistories.id, Users.name, SubscriptionHistories.plan_type, SubscriptionHistories.price, SubscriptionHistories.order_date, SubscriptionHistories.subscription_valid_till, SubscriptionHistories.payment_type FROM SubscriptionHistories LEFT JOIN Users ON SubscriptionHistories.user_id = Users.id WHERE SubscriptionHistories.practice_id = '${event.user.practice_id}'`;

        let countQuery = `SELECT COUNT(*) FROM SubscriptionHistories LEFT JOIN Users ON SubscriptionHistories.user_id = Users.id WHERE SubscriptionHistories.practice_id = '${event.user.practice_id}'`;

        //search
        let searchQuery = '';
        if (query.search != "false") {
            searchQuery = ` AND ( Users.name LIKE '%${query.search}%' OR SubscriptionHistories.plan_type LIKE '%${query.search}%' OR  SubscriptionHistories.price LIKE '%${query.search}%' OR SubscriptionHistories.payment_type LIKE '%${query.search}%' )`;
        } else {
            searchQuery = ''
        }

        //sort
        let sortQuery = '';
        if (query.sort != "false") {
            query.sort = JSON.parse(query.sort);
            sortQuery = ` ORDER BY ${query.sort.column} ${query.sort.type}`;
        } else {
            sortQuery = ` ORDER BY SubscriptionHistories.created_at DESC`;
        }

        //limit
        let limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset};`;

        selectQuery += searchQuery + sortQuery + limitQuery;
        countQuery += searchQuery;

        let data = await sequelize.query(selectQuery, {
            type: QueryTypes.SELECT
        });

        let totalPageCount = await sequelize.query(countQuery, {
            type: QueryTypes.SELECT
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': totalPageCount[0]["COUNT(*)"]
            },
            body: JSON.stringify(data)
        }
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the records.' }),
        };
    }
}

const unixTimetoDateconvert = (timestamp) => {
    const unixTimestamp = timestamp;
    const date = new Date(unixTimestamp * 1000);
    return date;
};

module.exports.create = create;
module.exports.getOne = getOne;
module.exports.adminGetAll = adminGetAll;
module.exports.getAllHistory = getAllHistory;
module.exports.update = update;
module.exports.getAll = getAll;
