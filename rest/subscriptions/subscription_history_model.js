const { STRING } = require("sequelize");

module.exports = (sequelize, type) => sequelize.define('SubscriptionHistory', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    order_date: type.DATE,
    practice_id: type.STRING,
    user_id: type.STRING,
    subscribed_by: type.STRING,
    price: type.FLOAT,
    subscribed_on: type.DATE,
    plan_type: type.STRING, // New Subscription Plan
    stripe_subscription_id: type.STRING,
    subscription_valid_start: type.DATE,
    subscription_valid_till: type.DATE,
    cancel_at: type.DATE,
    cancel_at_period_end: type.BOOLEAN,
    canceled_at: type.DATE,
    activity_type: type.STRING, //doubt
    stripe_subscription_data: type.TEXT,
    stripe_product_id: type.STRING,
    status: type.STRING,
    payment_type: type.STRING,
    switched_plan: type.STRING, // New Subscription Plan
    stripe_latest_activity: type.STRING
}, {
    updatedAt: 'updated_at',
    createdAt: 'created_at',
});
