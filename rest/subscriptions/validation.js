const Validator = require('validatorjs');
const {HTTPError} = require('../../utils/httpResp');

exports.validateRead = function (data) {
    const rules = {
        offset: 'numeric',
        limit: 'numeric',
    };
    const validation = new Validator(data, rules);

    if (validation.fails()) {
        throw new HTTPError(400, validation.errors.all());
    }
};