module.exports = (sequelize, type) => sequelize.define('Subscriptions', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    practice_id: type.STRING,
    subscribed_by: type.STRING,
    subscribed_on: type.DATE,
    plan_id: type.STRING,
    stripe_subscription_id: type.STRING,
    subscribed_valid_till: type.DATE,
    stripe_subscription_data: type.TEXT,
    subscription_meta_data: type.TEXT,
    stripe_product_id: type.STRING,
}, {
    updatedAt: 'updated_at',
    createdAt: 'created_at',
});
