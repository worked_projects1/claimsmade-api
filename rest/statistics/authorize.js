const { HTTPError } = require("../../utils/httpResp");

const majorRoles = ['superAdmin'];


exports.authorizeForStatistics = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(403, 'you don\'t have access');
}
