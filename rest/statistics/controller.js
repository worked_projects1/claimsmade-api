const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { authorizeForStatistics } = require('./authorize');

const getAll = async (event) => {

    authorizeForStatistics(event.user);

    try {
        const {
            Op,
            Practices,
            Users,
            RfaForms,
            Orders,
            sequelize,
            SubscriptionHistory
        } = await connectToDatabase();

        const d = new Date();
        d.setMonth(d.getMonth() - 1);
        const filter = event.query.filter;
        let filter_query = { [Op.gte]: d };


        // filter: Last30 days, This Month, This Quarter, This Year, Last Month, Last Quarter, Last Year
        if (filter) {

            if (filter == 'this_month') {

                const date = new Date();
                const firstDayOfThisMonth = new Date(date.getFullYear(), date.getMonth(), 1);
                filter_query = { [Op.gte]: firstDayOfThisMonth };

            } else if (filter == 'this_year') {

                const firstDayOfthisYear = new Date(new Date().getFullYear(), 0, 1);
                filter_query = { [Op.gte]: firstDayOfthisYear };

            } else if (filter == 'last_month') {

                const date = new Date();
                date.setMonth(date.getMonth() - 1);
                const firstDayOfLastMonth = new Date(date.getFullYear(), date.getMonth(), 1);
                const lastDayOfLastMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                filter_query = { [Op.gte]: firstDayOfLastMonth, [Op.lte]: lastDayOfLastMonth };

            } else if (filter == 'last_year') {

                const date = new Date();
                const firstDayOfLastYear = new Date(date.getFullYear() - 1, 0, 1);
                const lastDayOfLastYear = new Date(date.getFullYear() - 1, 12, 0);
                filter_query = { [Op.gte]: firstDayOfLastYear, [Op.lte]: lastDayOfLastYear };

            } else if (filter == 'this_quarter' || filter == 'last_quarter') {
                var date = new Date();
                var quarter = parseInt(date.getMonth() / 3) + 1;

                if (filter == 'this_quarter') {
                    filter_query = await getFilterByQuarter(quarter);
                } else if (filter == 'last_quarter') {
                    let lastquarter = quarter - 1;
                    filter_query = await getFilterByQuarter(lastquarter);
                }
            }
        }

        //Practices
        const totalNumberOfPractices = await Practices.count({});
        const totalActivePractices = await Practices.count({
            include: [{
                model: Users,
                where: { last_login_ts: { [Op.gte]: d } },
            }],
        });
        const totalNewPractices = await Practices.count({
            where: { created_at: filter_query, is_deleted: { [Op.not]: true } },
        });

        //Users
        const totalNewUsers = await Users.count({
            where: { created_at: filter_query, is_deleted: { [Op.not]: true } },
        });
        const totalNumberOfUsers = await Users.count({});
        const totalActiveUsers = await Users.count({
            where: { last_login_ts: { [Op.gte]: d } },
        });

        //RFA Forms

        const totalRfaForms = await RfaForms.count();
        const totalActiceRfaForms = await RfaForms.count({
            where: { is_deleted: { [Op.not]: true } },
        });
        const totalNewRfaForms = await RfaForms.count({
            where: { created_at: filter_query },
        });

        //revenue

        // const products = await Orders.findAll({
        //     where: { created_at: filter_query },
        //     group: ['activity_type'],
        //     attributes: ['activity_type', [sequelize.fn('COUNT', sequelize.col('activity_type')), 'count']],
        //     order: [[sequelize.literal('count'), 'DESC']],
        //     raw: true,
        // });

        // const productsFinal = { RFA_FORM_DOC: 0 };
        // let productsTotal = 0;
        // for (let i = 0; i < products.length; i += 1) {
        //     const product = products[i];
        //     productsTotal += parseInt(product.count);
        //     productsFinal[product.activity_type] = product.count;
        // }
        // productsFinal.total = productsTotal;

        const revenuesModels = await Orders.findAll({
            where: { created_at: filter_query },
            attributes: [
                'activity_type',
                [sequelize.fn('sum', sequelize.col('amount_charged')), 'revenue'],
            ],
            group: ['activity_type'],
            raw: true,
        });

        const revenuesFinal = { RFA_FORM_DOC: 0 };
        let revenueTotal = 0;
        for (let i = 0; i < revenuesModels.length; i += 1) {
            const revenueObject = revenuesModels[i];
            revenueTotal += parseFloat(revenueObject.revenue);
            revenuesFinal[revenueObject.activity_type] = parseFloat(revenueObject.revenue.toFixed(2));
        }
        revenuesFinal.total = parseFloat(revenueTotal.toFixed(2));

        const totalamount = await SubscriptionHistory.findAll({
            where: { created_at: filter_query },
            attributes: [
                [sequelize.fn('sum', sequelize.col('price')), 'total_cost'],
            ],
            raw: true
        });

        let extractamount = 0;
        if (totalamount[0].total_cost) {
            extractamount = totalamount[0].total_cost;
        }

        let revenue_total = (parseFloat(revenuesFinal.RFA_FORM_DOC) + parseFloat(extractamount)).toFixed(2);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                practices: {
                    active: totalActivePractices,
                    new: totalNewPractices,
                    total: totalNumberOfPractices,
                },
                users: {
                    active: totalActiveUsers,
                    new: totalNewUsers,
                    total: totalNumberOfUsers,
                },
                rfaForms: {
                    active: totalActiceRfaForms,
                    new: totalNewRfaForms,
                    total: totalRfaForms,
                },
                totalNumberOfPractices,
                totalActivePractices,
                totalNumberOfUsers,
                totalActiveUsers,
                revenue_details: { Orders: revenuesFinal.RFA_FORM_DOC, subscriptions: extractamount, total: revenue_total }
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the records.' }),
        };
    }
};

const getPracticesAndRevenueStat = async (event) => {

    authorizeForStatistics(event.user);

    try {
        const { Op, Practices, Orders, sequelize, SubscriptionHistory } = await connectToDatabase();

        const filter = event.query.filter;

        let date = new Date(new Date().getFullYear() - 1, 0, 1);
        let date2 = new Date(new Date().getFullYear(), 0, 1);

        if (filter) {
            date = new Date(parseInt(filter), 0, 1);
            date2 = new Date(parseInt(filter) + 1, 0, 1);
            //date2 = new Date(parseInt(filter) , 12, 0);
        }

        const filter_query = { [Op.gte]: date, [Op.lte]: date2 };

        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        const practices = await Practices.findAll({
            where: { created_at: filter_query },
            group: [sequelize.fn('month', sequelize.col('created_at'))],
            attributes: [
                [sequelize.fn('COUNT', '*'), 'new'], [sequelize.fn('count', '*'), 'total'], [sequelize.fn('month', sequelize.col('created_at')), 'month'],
            ],
            raw: true,
        });

        const revenue = await Orders.findAll({
            where: { created_at: filter_query },
            attributes: [
                [sequelize.fn('sum', sequelize.col('amount_charged')), 'new'], [sequelize.fn('month', sequelize.col('created_at')), 'month'],
            ],
            group: [sequelize.fn('month', sequelize.col('created_at'))],
            raw: true,
            // logging: console.log
        });

        const sub_revenue = await SubscriptionHistory.findAll({
            where: { created_at: filter_query },
            attributes: [
                [sequelize.fn('sum', sequelize.col('price')), 'new'], [sequelize.fn('month', sequelize.col('created_at')), 'month'],
            ],
            group: [sequelize.fn('month', sequelize.col('created_at'))],
            raw: true,
            // logging: console.log
        });

        const practicesMetricsData = {};
        const revenueMtericsData = [];
        const revenueMergeData = {};
        const subscriptionMtericsData = [];

        for (let i = 0; i < months.length; i += 1) {
            const monthValue = months[i];

            if (!practicesMetricsData[monthValue]) {
                practicesMetricsData[monthValue] = {};
                practicesMetricsData[monthValue].new = 0;
                if (i <= 0) {
                    practicesMetricsData[monthValue].total = 0;
                } else {
                    practicesMetricsData[monthValue].total = practicesMetricsData[months[i - 1]].total;
                }
            }

            if (!revenueMtericsData[monthValue]) {
                revenueMtericsData[monthValue] = {};
                revenueMtericsData[monthValue].new = 0;
                if (i <= 0) {
                    revenueMtericsData[monthValue].total = 0;
                } else {
                    revenueMtericsData[monthValue].total = revenueMtericsData[months[i - 1]].total;
                }
            }

            if (!revenueMergeData[monthValue]) {
                revenueMergeData[monthValue] = {};
                revenueMergeData[monthValue].new = 0;
                if (i <= 0) {
                    revenueMergeData[monthValue].total = 0;
                } else {
                    revenueMergeData[monthValue].total = revenueMergeData[months[i - 1]].total;
                }
            }

            if (!subscriptionMtericsData[monthValue]) {
                subscriptionMtericsData[monthValue] = {};
                subscriptionMtericsData[monthValue].new = 0;
                if (i <= 0) {
                    subscriptionMtericsData[monthValue].total = 0;
                } else {
                    subscriptionMtericsData[monthValue].total = subscriptionMtericsData[months[i - 1]].total;
                }
            }

        }

        let totalPracticeCount = 0;
        for (let i = 0; i < practices.length; i += 1) {
            const practiceObject = practices[i];
            if (!practiceObject.new) {
                practiceObject.new = 0;
            }
            if (i > 0) {
                totalPracticeCount += practiceObject.new;
            } else {
                totalPracticeCount = practiceObject.new;
            }
            practicesMetricsData[months[practiceObject.month - 1]] = {
                new: practiceObject.new,
                total: totalPracticeCount
            };
        }


        let revenueTotal = 0;

        //To fill the records for new flag alone to form the common object
        for (let i = 0; i < revenue.length; i += 1) {
            const revenueObject = revenue[i];
            if (!revenueObject.new) {
                revenueObject.new = 0;
            }

            revenueMtericsData[months[revenueObject.month - 1]] = {
                new: parseFloat(revenueObject.new.toFixed(2)),
                total: parseFloat(revenueTotal.toFixed(2)),
            };
        }

        //To fill the records for new(node name) flag alone to form the common object
        for (let i = 0; i < sub_revenue.length; i += 1) {
            const subRevenueObject = sub_revenue[i];
            if (!subRevenueObject.new) {
                subRevenueObject.new = 0;
            }

            subscriptionMtericsData[months[subRevenueObject.month - 1]] = {
                new: parseFloat(subRevenueObject.new.toFixed(2)),
                total: parseFloat(revenueTotal.toFixed(2)),
            };
        }


        //To merge the revenue data for adding the both order and subscription revenue 
        let cumulativeValue = '';
        for (var i = 0; i < months.length; i++) {

            revenueMergeData[months[i]].new = subscriptionMtericsData[months[i]]["new"] + revenueMtericsData[months[i]]["new"]
            if (i > 0) {
                revenueMergeData[months[i]].total = revenueMergeData[months[i - 1]].total + revenueMergeData[months[i]].new
            } else {
                revenueMergeData[months[i]].total = revenueMergeData[months[i]].new;
            }
        }

        for (let i = 0; i < months.length; i += 1) {
            const monthValue = months[i];
            if (i > 0) {
                if (!practicesMetricsData[monthValue].total) {
                    practicesMetricsData[monthValue].total = practicesMetricsData[months[i - 1]].total;
                }
                // if (!revenueMtericsData[monthValue].total) {
                //     revenueMtericsData[monthValue].total = revenueMtericsData[months[i - 1]].total;
                // }
            }
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                practices: practicesMetricsData,
                revenue: revenueMergeData,
                range: { from: date, to: date2 },
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the practice and revenue details.' }),
        };
    }
};

const getFilterByQuarter = async (quarter) => {

    // Q1: January – March.
    // Q2: April – June.
    // Q3: July – September.
    // Q4: October – December.

    const {
        Op
    } = await connectToDatabase();


    const d = new Date();
    d.setMonth(d.getMonth() - 1); //Last 30 days
    let filter_query = { [Op.gte]: d };
    let firstDay;
    let lastDay;

    const date = new Date();

    if (quarter == '1') {
        firstDay = new Date(date.getFullYear(), 0, 1); //Firstday of Jan
        lastDay = new Date(date.getFullYear(), 3, 0);  //Lastday of Mar
    } else if (quarter == '2') {
        firstDay = new Date(date.getFullYear(), 3, 1); //Firstday of Apr
        lastDay = new Date(date.getFullYear(), 6, 0);  //Lastday of Jun
    } else if (quarter == '3') {
        firstDay = new Date(date.getFullYear(), 6, 1); //Firstday of Jul
        lastDay = new Date(date.getFullYear(), 9, 0);  //Lastday of Sep
    } else if (quarter == '4') {
        firstDay = new Date(date.getFullYear(), 9, 1); //Firstday of Oct
        lastDay = new Date(date.getFullYear(), 12, 0);  //Lastday of Dec
    }

    if (firstDay && lastDay) {
        filter_query = { [Op.gte]: firstDay, [Op.lte]: lastDay };
    }

    return filter_query;
};


module.exports.getAll = getAll;
module.exports.getPracticesAndRevenueStat = getPracticesAndRevenueStat;
