const uuid = require('uuid');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY, { apiVersion: '' });
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');

const setUpPaymentIntent = async (event) => {
    try {
        const { Practices } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        if (!practiceObject.stripe_customer_id) {
            const customer = await stripe.customers.create();
            console.log('Customer ID ' + customer.id);
            practiceObject.stripe_customer_id = customer.id;
        }

        const intent = await stripe.setupIntents.create({
            customer: practiceObject.stripe_customer_id,
        });

        await practiceObject.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                client_secret: intent.client_secret,
            }),
        };
    } catch (err) {
        console.log('Stripe error : payment intent');
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not setup the payment Intent.' }),
        };
    }
};

const saveCardData = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const { Practices } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        if (!input) {
            throw new HTTPError(400, 'fields missing');
        }
        if (!input.payment_method) {
            throw new HTTPError(400, 'Stripe Token missing');
        }
        if (practiceObject.stripe_customer_id) {
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            if (paymentMethods.data.length) {
                for (let i = 0; i < paymentMethods.data.length; i += 1) {
                    if (paymentMethods.data[i].id != input.payment_method) {
                        await stripe.paymentMethods.detach(paymentMethods.data[i].id);
                    }
                }
            }
            await stripe.paymentMethods.attach(
                input.payment_method,
                {
                    customer: practiceObject.stripe_customer_id
                }
            );
            await stripe.customers.update(
                practiceObject.stripe_customer_id,
                {
                    invoice_settings: { default_payment_method: input.payment_method },
                    email: event.user.email,
                    name: practiceObject.name,
                    metadata: {
                        practice_id: event.user.practice_id,
                    },
                }
            );
        } else {
            const stripeCustomer = await stripe.customers.create(
                {
                    description: ' Added Stripe Data',
                    email: event.user.email,
                    name: practiceObject.name,
                    metadata: {
                        practice_id: event.user.practice_id,
                    },
                }
            );
            practiceObject.stripe_customer_id = stripeCustomer.id;
            await practiceObject.save();
            await stripe.paymentMethods.attach(
                input.payment_method,
                { customer: practiceObject.stripe_customer_id }
            );
            await stripe.customers.update(
                practiceObject.stripe_customer_id,
                { invoice_settings: { default_payment_method: input.payment_method } }
            );
        }


        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: 'ok',
                message: 'card data saved',
            }),
        };
    } catch (err) {
        console.log('Stripe error : saving card data');
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not save the card details.' }),
        };
    }
};

const fetchBillingStatus = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const {
            Practices,
            Settings,
            Op,
            Subscriptions,
            Plans,
            Orders
        } = await connectToDatabase();
        const practiceObject = await Practices.findOne({ where: { id: event.user.practice_id } });
        if (!practiceObject) throw new HTTPError(400, `Practices with id: ${event.user.practice_id} was not found`);

        const settingsObject = await Settings.findOne({
            where: { key: 'global_settings' },
            raw: true,
        });

        if (!settingsObject) throw new HTTPError(400, 'Settings data not found');
        let settings = JSON.parse(settingsObject.value);

        let plan_type = 'pay_as_you_go';

        const existingSubscriptionDetails = await Subscriptions.findOne({
            where: { practice_id: event.user.practice_id },
            order: [['created_at', 'DESC']]
        });


        let existingSubscriptionDetailsPlain = undefined;

        if (existingSubscriptionDetails && existingSubscriptionDetails.stripe_subscription_id && existingSubscriptionDetails.plan_id) {
            const subscription = await stripe.subscriptions.retrieve(
                existingSubscriptionDetails.stripe_subscription_id
            );
            if (subscription) {
                existingSubscriptionDetails.stripe_subscription_data = JSON.stringify(subscription);
                const updateubscriptionDetails = await existingSubscriptionDetails.save();
                existingSubscriptionDetailsPlain = updateubscriptionDetails.get({ plain: true });

            }
        }
        let planDetails = undefined;
        if (existingSubscriptionDetails && existingSubscriptionDetails.stripe_subscription_data && existingSubscriptionDetails.plan_id) {
            const stripeSubscriptionDataObject = JSON.parse(existingSubscriptionDetails.stripe_subscription_data);
            const subscriptionValidity = new Date(parseInt(stripeSubscriptionDataObject.current_period_end) * 1000);
            const today = new Date();
            if (subscriptionValidity > today) {
                planDetails = await Plans.findOne({
                    raw: true,
                    where: { stripe_product_id: existingSubscriptionDetails.stripe_product_id, active: true },
                });

                if (planDetails && planDetails.plan_type) {
                    plan_type = planDetails.plan_type
                } else {
                    plan_type = 'pay_as_you_go'; //default plan
                }
            }
        }

        const activity_type = input.activity_type;

        let price = 0;

        if (activity_type === 'RFA_FORM_DOC' && plan_type == 'pay_as_you_go') {

            const existOrderModel = await Orders.findAll({
                where: {
                    status: 'completed',
                    practice_id: event.user.practice_id,
                    activity_type: activity_type,
                }, logging: console.log
            });

            if (existOrderModel && existOrderModel.length < parseInt(settings.no_of_free_tier)) {
                price = 0;
            } else {
                if (settings && settings.price_value && settings.price_value.rfa_form_doc) {
                    price = parseFloat(settings.price_value.rfa_form_doc, 10);
                }
            }
        } else if (activity_type === 'RFA_FORM_DOC' && plan_type != 'pay_as_you_go') {
            if (settings && settings.price_value && settings.price_value.rfa_form_doc) {
                price = parseFloat(settings.price_value.rfa_form_doc, 10);
            }
        }

        let credit_card_details_available = false;

        let stripe_payment_methods = [];

        if (practiceObject && practiceObject.stripe_customer_id) {
            const paymentMethods = await stripe.paymentMethods.list({
                customer: practiceObject.stripe_customer_id,
                type: 'card',
            });
            stripe_payment_methods = paymentMethods.data || [];
            if (paymentMethods.data.length) {
                credit_card_details_available = true;
            }
        }

        if (price && price > 0 && price < 0.50) {
            price = 0.50;
        }

        const data = {
            credit_card_details_available,
            price,
            stripe_payment_methods,
            plan_type
        };

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(data),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the billing status.' }),
        };
    }
};


module.exports.setUpPaymentIntent = setUpPaymentIntent;
module.exports.saveCardData = saveCardData;
module.exports.fetchBillingStatus = fetchBillingStatus;

