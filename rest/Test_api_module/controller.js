const connectToDatabase = require('../../db');
const { config } = require('../../test/global');
const { authorizeAdmin } = require('./authorize');
const destroyTestRecords = async (event) => {
  try {
    authorizeAdmin(event.user);
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const practice_id = input.practice_id;
    const users_email = input.users_email;

    const { Users, PasswordHistory, Op, Practices, Treatments, Diagnosis, RfaForms, ClaimsAdmins, Orders } = await connectToDatabase();

    await PasswordHistory.destroy({ where: { email: { [Op.in]: users_email } } });
    await Users.destroy({
      where: {
        email: { [Op.in]: users_email },
        practice_id: { [Op.in]: practice_id }
      }
    });
    await Practices.destroy({ where: { id: { [Op.in]: practice_id } } });
    await Users.destroy({
      where: { email: { [Op.in]: config.superAdminEmailIds } }
    })
    await PasswordHistory.destroy({
      where: { email: { [Op.in]: config.superAdminEmailIds } }
    })
    await Treatments.destroy({
      where: { id: { [Op.in]: config.treatmentTestRecordIds } }
    })
    await Diagnosis.destroy({
      where: { treatment_id: { [Op.in]: config.treatmentTestRecordIds } }
    })
    await RfaForms.destroy({
      where: { id: { [Op.in]: config.rfa_case_ids } }
    })
    await ClaimsAdmins.destroy({
      where: { id: { [Op.in]: config.claims_admin_ids } }
    })
    await Orders.destroy({
      where: { id: { [Op.in]: config.order_ids } }
    })

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ status: 'Ok' }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not update user.' }),
    };
  }
}


const createTestSuperAdmin = async (event) => {
  try {
    authorizeAdmin(event.user);
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const { Users } = await connectToDatabase();
    const res = await Users.create({
      id: input.id,
      name: input.name,
      email: input.email,
      password: input.password,
      practice_id: null,
      role: input.role,
      is_admin: null,
      is_deleted: false
    });
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ status: 'Ok' }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: JSON.stringify(err) || 'Could not create user.' }),
    };
  }
}

module.exports.destroyTestRecords = destroyTestRecords;
module.exports.createTestSuperAdmin = createTestSuperAdmin;