const { appConstants } = require("../../appConstants")
const { HTTPError } = require("../../utils/httpResp")

exports.authorizeAdmin = function (user) {
    if (!appConstants.ADMIN_EMAIL_ID == user.email) {
        throw new HTTPError(403, `Ypu don't have access`)
    }
}