const uuid = require('uuid');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY, { apiVersion: '' });
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const { unixTimeStamptoDate } = require('../../utils/timeStamp');
const { sendEmail } = require('../../utils/mailModule');


const subscriptionWebHook = async (event) => {
    try {
        console.log("Inside Subscription Webhook");
        const endpointSecret = process.env.STRIPE_SUBSCRIPTION_WEBHOOK_SECRET;
        const signature = event.headers['Stripe-Signature'];
        const stripeEvent = stripe.webhooks.constructEvent(event.body, signature, endpointSecret);
        switch (stripeEvent.type) {
            case 'customer.subscription.created':
                await updateSubscriptionWebHookStatus(stripeEvent);
                break;
            case 'customer.subscription.deleted':
                await updateSubscriptionWebHookStatus(stripeEvent);
                break;
            case 'customer.subscription.updated':
                await updateSubscriptionWebHookStatus(stripeEvent);
                break;
            default:
                console.log(`Unhandled event type ${stripeEvent.type}`);
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };


    } catch (err) {
        console.log(`Webhook signature verification failed.`, err.message);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}

const updateSubscriptionWebHookStatus = async (event) => {
    try {
        let body = '';
        if (process.env.CODE_ENV == 'local' && event.body && event.body.data) {
            body = typeof event.body.data === 'string' ? JSON.parse(event.body.data) : event.body.data;
        } else {
            body = typeof event.data === 'string' ? JSON.parse(event.data) : event.data;
        }
        // const body = typeof event.data === 'string' ? JSON.parse(event.data) : event.data;
        console.log(body.object.customer);
        const {
            Op,
            Users,
            Plans,
            Practices,
            SubscriptionHistoryWebhook,
            SubscriptionHistory,
        } = await connectToDatabase();

        const customer_id = body.object.customer;
        const practicesobj = await Practices.findOne({
            where: {
                stripe_customer_id: customer_id,
                is_deleted: { [Op.not]: true }
            }, raw: true
        });
        if (!practicesobj) throw new HTTPError(404, 'Invalid stripe customer id ' + customer_id);

        let usersobj; //To get the login user details
        if (body.object.metadata && body.object.metadata.user_email) {
            usersobj = await Users.findOne({
                where: {
                    practice_id: practicesobj.id,
                    is_deleted: { [Op.not]: true },
                    email: body.object.metadata.user_email,
                    is_admin: true
                },
                raw: true
            });
        } else {
            console.log("user_email is not present in the meta data:stripe_customer_id: ", body.object.customer);
            usersobj = await Users.findOne({
                where: {
                    practice_id: practicesobj.id,
                    is_deleted: { [Op.not]: true },
                    is_admin: true
                },
                order: [['created_at', 'ASC']],
                raw: true
            });
        }

        const previousSubscriptionObj = await SubscriptionHistory.findOne({
            where: { practice_id: practicesobj.id },
            order: [['created_at', 'DESC']],
            raw: true
        });
        // let currentSubscriptionPlan = undefined;
        // if(currentSubscriptionObj){
        //     currentSubscriptionPlan = await Plans.findOne({where:{plan_id:currentSubscriptionObj.plan_id},raw:true});
        // }

        let planObject = undefined;
        if (body.object.metadata && body.object.metadata.price_id && body.object.metadata.stripe_product_id) {
            planObject = await Plans.findOne({
                where: {
                    stripe_price_id: body.object.metadata.price_id,
                    active: true,
                    stripe_product_id: body.object.metadata.stripe_product_id
                },
                raw: true,
                logging: console.log
            });
        } else {
            console.log("Else condition: ", body.object.plan.product);
            planObject = await Plans.findOne({
                where: { stripe_product_id: body.object.plan.product, active: true },
                raw: true,
                logging: console.log
            });
        }

        const subscriptionValidityStart = new Date(parseInt(body.object.current_period_start) * 1000);
        const subscriptionValidity = new Date(parseInt(body.object.current_period_end) * 1000);
        let latest_actvity = undefined;
        let switchedPlan = undefined;
        let status = undefined;
        let activity_type = undefined;
        let payment_type = undefined;
        if (event.type == 'customer.subscription.created') {
            payment_type = 'New';
        } else if (event.type == 'customer.subscription.updated') {
            payment_type = 'Renewal';
            if (
                (previousSubscriptionObj.plan_type == 'monthly' && planObject.plan_type == 'monthly') ||
                (previousSubscriptionObj.plan_type == 'yearly' && planObject.plan_type == 'yearly')
            ) {
                payment_type = 'Renewal';
            } else {
                payment_type = 'New';
            }
        } else if (event.type == 'customer.subscription.deleted') {
            payment_type = 'Subscription Canceled';
        }
        if (body.object.cancel_at_period_end == true && body.object.canceled_at && body.object.cancel_at && body.object.status == "active") {
            latest_actvity = 'downgrade';
            switchedPlan = 'pay_as_you_go';
            activity_type = 'DOWNGRADE';
        } else {
            if (body.object.metadata.price_id) {
                if (body.object.metadata.latest_actvity) {
                    latest_actvity = body.object.metadata.latest_actvity;
                }
            }
            switchedPlan = planObject.plan_type;
            const historyCount = await SubscriptionHistory.count({ where: { practice_id: practicesobj.id } });
            if (historyCount <= 0) {
                activity_type = 'NEW_SUBSCRIPTION';
            } else {
                activity_type = 'UPGRADED';
            }
        }
        if (body.object.status == "active") {
            status = "Success";
        } else {
            status = "Success";
            activity_type = undefined;
        }

        let cancel_at = undefined;
        let canceled_at = undefined;
        if (body.object.cancel_at != null) cancel_at = new Date(parseInt(body.object.cancel_at) * 1000);
        if (body.object.canceled_at != null) canceled_at = new Date(parseInt(body.object.canceled_at) * 1000);

        const subscriptionHistoryData = {
            id: uuid.v4(),
            order_date: new Date(),
            practice_id: practicesobj.id,
            user_id: usersobj.id,
            subscribed_by: usersobj.name,
            price: planObject.price,
            plan_type: planObject.plan_type,
            subscribed_on: new Date(),
            plan_id: planObject.plan_id,
            stripe_subscription_id: body.object.id,
            subscription_valid_start: subscriptionValidityStart,
            subscription_valid_till: subscriptionValidity,
            stripe_subscription_data: JSON.stringify(body.object),
            cancel_at: cancel_at,
            cancel_at_period_end: body.object.cancel_at_period_end,
            canceled_at: canceled_at,
            activity_type: activity_type,
            payment_type: payment_type,
            stripe_latest_activity: latest_actvity,
            stripe_product_id: planObject.stripe_product_id,
            status: status,
            event_type: event.type,
            switched_plan: switchedPlan
        };
        console.log(subscriptionHistoryData);
        await SubscriptionHistory.create(subscriptionHistoryData);
        await SubscriptionHistoryWebhook.create(subscriptionHistoryData);
        return;
    } catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
        };
    }
}

const subscriptionCancel = async (event) => {
    try {
        const params = event.query;
        console.log(params);
        let cancel_at = undefined;
        let canceled_at = undefined;

        if (!params.practice_id) throw new HTTPError(404, `Practice id was not found`);
        const { Practices, Op, Subscriptions, Plans, SubscriptionHistory, Users } = await connectToDatabase();
        const practiceObj = await Practices.findOne({ where: { id: params.practice_id, is_deleted: { [Op.not]: true } }, raw: true });
        if (!practiceObj) throw new HTTPError(404, `Invalid practice id`);
        const subscriptionObj = await Subscriptions.findOne({ where: { practice_id: params.practice_id }, order: [['created_at', 'DESC'],], raw: true });
        if (subscriptionObj && subscriptionObj.stripe_subscription_id) {

            let cancel_at_period_end = false;

            const cancelObj = await stripe.subscriptions.update(subscriptionObj.stripe_subscription_id, { cancel_at_period_end: true });

            let response = await Subscriptions.update({ stripe_subscription_data: JSON.stringify(cancelObj) }, { where: { practice_id: params.practice_id } });
            
            const exsistingPlanObject = await Plans.findOne({ where: { stripe_product_id: subscriptionObj.stripe_product_id, active: true } });
            let currentPlan = `${exsistingPlanObject.plan_type} plan`;

            if (response.length != 0 && response[0] == 0) {
                throw new HTTPError(500, `Subscription details not updated`)
            } else {
                console.log(cancelObj);
                const usersObj = await Users.findOne({ where: { practice_id: practiceObj.id, is_deleted: { [Op.not]: true } }, order: [['created_at', 'ASC']], logging: console.log, raw: true });

                let endDate = unixTimeStamptoDate(cancelObj.cancel_at);
                let subject = `Your VettedClaims ${currentPlan} will be active till ${endDate}`;
                let message_body = 'Hi ' + practiceObj.name + ',<br><br>' +
                    `We have canceled the auto-renewal of your ${currentPlan} based on your request. Your plan will remain active till the end of the current subscription period (` + endDate + '). There will be no further subscription charges to your account.<br><br>' +
                    'Thank you for being a valuable customer. We are always looking to improve VettedClaims if you have any feedback for us on how to improve our platform, or if this cancellation was in error, please email (support@vettedclaims.com) with any comments or concerns. ';
                await sendEmail(usersObj.email, subject, message_body, practiceObj.name);
            }

            //Subscription History Data

            if (cancelObj.cancel_at != null) cancel_at = new Date(parseInt(cancelObj.cancel_at) * 1000);
            if (cancelObj.canceled_at != null) canceled_at = new Date(parseInt(cancelObj.canceled_at) * 1000);
            if (cancelObj.cancel_at_period_end) cancel_at_period_end = cancelObj.cancel_at_period_end;

            const subscriptionValidityStart = new Date(parseInt(cancelObj.current_period_start) * 1000);
            const subscriptionValidity = new Date(parseInt(cancelObj.current_period_end) * 1000);


            if (exsistingPlanObject && exsistingPlanObject.price) {
                price = exsistingPlanObject.price;
                plan_type = exsistingPlanObject.plan_type;
                stripe_product_id = exsistingPlanObject.stripe_product_id;
            }

            const subscriptionHistoryData = {
                id: uuid.v4(),
                order_date: new Date(),
                practice_id: params.practice_id,
                user_id: event.user.id,
                subscribed_by: event.user.name,
                price: exsistingPlanObject.price,
                plan_type: exsistingPlanObject.plan_type,
                subscribed_on: new Date(),
                cancel_at: cancel_at,
                cancel_at_period_end: cancel_at_period_end,
                canceled_at: canceled_at,
                stripe_subscription_id: subscriptionObj.stripe_subscription_id,
                subscription_valid_start: subscriptionValidityStart,
                subscription_valid_till: subscriptionValidity,
                stripe_subscription_data: JSON.stringify(cancelObj),
                stripe_product_id: subscriptionObj.stripe_product_id,
                activity_type: 'DOWNGRADE',
                status: "Success",
                payment_type: 'Subscription Canceled'
            };

            await SubscriptionHistory.create(subscriptionHistoryData);

        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
            },
            body: JSON.stringify({ status: 'ok', message: 'Subscription canceled successfully.' }),
        };

    } catch (err) {
        console.log(err);

        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not cancel subscription.' }),
        };
    }
}

module.exports.subscriptionWebHook = subscriptionWebHook;
module.exports.updateSubscriptionWebHookStatus = updateSubscriptionWebHookStatus;
module.exports.subscriptionCancel = subscriptionCancel;


