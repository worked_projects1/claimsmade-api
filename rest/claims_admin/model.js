module.exports = (sequelize, type) => {
    const ClaimsAdmins = sequelize.define('ClaimsAdmins', {
        id: {
            type: type.STRING,
            primaryKey: true,
        },
        company_name: type.STRING,
        email: type.STRING,
        practice_id: type.STRING,
        is_claims_admin: type.BOOLEAN,
        address: type.STRING,
        city: type.STRING,
        state: type.STRING,
        zip_code: type.STRING,
        phone_number: type.STRING,
        fax_number: type.STRING,
        contact_name: type.STRING,
        created_by: type.STRING,
        is_deleted: type.BOOLEAN
    }, {
        updatedAt: 'updated_at',
        createdAt: 'created_at',
    });

    return ClaimsAdmins;
};

