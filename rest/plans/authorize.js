const { HTTPError } = require('../../utils/httpResp');

const internalRoles = ['superAdmin', 'manager', 'operator'];

exports.authorizeCreate = function (user) {
    if (!internalRoles.includes(user.role)) {
        throw new HTTPError(403, 'you don\'t have access');
    }
};