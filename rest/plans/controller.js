const { QueryTypes } = require('sequelize');
const uuid = require('uuid');
const connectToDatabase = require('../../db');
const { HTTPError } = require('../../utils/httpResp');
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const {
    authorizeCreate
} = require('./authorize');
const {
    validateCreate
} = require('./validation');

const create = async (event) => {
    try {
        let id;
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        if (event.user.id) {
            input.created_by_admin_id = event.user.id;
        }

        const dataObject = Object.assign(input, { id: id || uuid.v4() });

        validateCreate(dataObject);
        authorizeCreate(event.user);
        const { Plans, Users, Op } = await connectToDatabase();

        let existObj;
        //duplicate restriction
        if (input.plan_type != 'pay_as_you_go') {
            existObj = await Plans.findOne({
                where: { name: input.name, active: true }
            });
        } else {
            existObj = await Plans.findOne({
                where: {
                    plan_type: input.plan_type,
                    active: true
                }
            });
        }
        if (existObj && existObj.name && input.plan_type != 'pay_as_you_go') throw new HTTPError(400, `Plan with name '${input.name}' is already exist.`);

        if (existObj && existObj.name && input.plan_type == 'pay_as_you_go') throw new HTTPError(400, `Plan type PAY AS-YOU-GO is already exist.`);

        if (dataObject.price && input.plan_type != 'pay_as_you_go') {
            const product = await stripe.products.create({
                name: dataObject.name,
            });
            const price = await stripe.prices.create({
                unit_amount: Math.round(dataObject.price * 100),
                currency: process.env.STRIPE_CURRENCY,
                recurring: { interval: 'day', interval_count: parseInt(input.validity) },
                product: product.id,
            });

            dataObject.stripe_product_id = product.id;
            dataObject.stripe_price_id = price.id;
        }

        const planModelObject = await Plans.create(dataObject);

        const plainplanModel = planModelObject.get({ plain: true });
        const UsersModel = await Users.findOne({
            where: {
                id: plainplanModel.created_by_admin_id,
                is_deleted: { [Op.not]: true }
            }, raw: true
        });
        if (UsersModel) plainplanModel.created_by_admin_name = UsersModel.name;
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainplanModel),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the PlanModelObject.' }),
        };
    }
};

const getAll = async (event) => {
    try {
        const query = event.headers;
        if (query.limit) query.limit = parseInt(query.limit, 10);
        if (query.offset) query.offset = parseInt(query.offset, 10);
        const { sequelize } = await connectToDatabase();

        let selectQuery = `SELECT Plans.id, Plans.stripe_product_id, Plans.stripe_price_id, Plans.plan_type, Plans.name, Plans.price, Plans.validity, Users.id AS 'created_by_admin_id', Plans.active, Plans.order, Users.name AS 'created_by_admin_name' FROM Plans LEFT JOIN Users ON Plans.created_by_admin_id = Users.id WHERE Plans.active IS TRUE`;

        let countQuery = `SELECT COUNT(*) FROM Plans LEFT JOIN Users ON Plans.created_by_admin_id = Users.id WHERE Plans.active IS TRUE`;

        // search
        let searchQuery = '';
        if (query.search != 'false') {

            searchQuery = ` AND ( Plans.name LIKE '%${query.search}%' OR Plans.plan_type LIKE '%${query.search}%' OR Plans.price LIKE '%${query.search}%' OR Plans.validity LIKE '%${query.search}%' OR Plans.order LIKE '%${query.search}%' OR Users.name LIKE '%${query.search}%' )`
        }

        // sort
        let sortQuery = '';
        if (query.sort == 'false') {
            sortQuery = ' ORDER BY Plans.order ASC'
        } else {
            const sortObj = JSON.parse(query.sort);
            sortObj.column = sortObj.column == 'order' ? 'Plans.order' : sortObj.column;
            sortQuery = ` ORDER BY ${sortObj.column} ${sortObj.type}`
        }

        // limit
        const limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset};`

        selectQuery += searchQuery + sortQuery + limitQuery;
        countQuery += searchQuery;

        const selectData = await sequelize.query(selectQuery, {
            type: QueryTypes.SELECT
        });

        const countData = await sequelize.query(countQuery, {
            type: QueryTypes.SELECT
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Expose-Headers': 'totalPageCount',
                'totalPageCount': countData[0]["COUNT(*)"]
            },
            body: JSON.stringify(selectData),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the PlanModelObjects.' }),
        };
    }
};

const getOne = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        /* validateGetOne(params);
        authorizeGetOne(event.user); */
        const { Plans, Users, Op } = await connectToDatabase();
        const planModelObject = await Plans.findOne({ where: { id: params.id, active: true } });
        if (!planModelObject) throw new HTTPError(404, `Plans with id: ${params.id} was not found`);
        let plainCustomerObjection = '';

        if (planModelObject.created_by_admin_id) {
            const UsersModel = await Users.findOne({
                where: {
                    id: planModelObject.created_by_admin_id,
                    is_deleted: { [Op.not]: true }
                }, raw: true
            });
            plainCustomerObjection = planModelObject.get({ plain: true });
            Object.assign(plainCustomerObjection, { created_by_admin_name: UsersModel.name });
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(plainCustomerObjection),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not fetch the Plans.' }),
        };
    }
};

const update = async (event) => {
    try {
        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
        const params = event.params || event.pathParameters;
        // validateUpdate(input);
        // authorizeUpdate(event.user);
        const { Plans } = await connectToDatabase();
        const planModelObject = await Plans.findOne({ where: { id: params.id, active: true } });
        if (!planModelObject) throw new HTTPError(404, `Plans with id: ${params.id} was not found`);
        if (planModelObject.plan_type !== 'pay_as_you_go' && planModelObject.plan_type !== 'free_tier') {
            if (input.price && input.price !== planModelObject.price) {
                const price = await stripe.prices.create({
                    unit_amount: Math.round(dataObject.price * 100),
                    currency: process.env.STRIPE_CURRENCY,
                    recurring: { interval: 'day', interval_count: parseInt(input.validity || planModelObject.validity) },
                    product: planModelObject.stripe_product_id,
                });
                input.stripe_price_id = price.id;
            }
            if (input.name && input.name !== planModelObject.name) {
                await stripe.products.update(
                    planModelObject.stripe_product_id,
                    { name: input.name }
                );
            }
        }

        const updatedModel = Object.assign(planModelObject, input);
        await updatedModel.save();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(updatedModel),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the Plans.' }),
        };
    }
};

const destroy = async (event) => {
    try {
        const params = event.params || event.pathParameters;
        // validateDelete(params);
        // authorizeDestroy(event.user);
        const { Plans } = await connectToDatabase();
        const adminObjectionsDetails = await Plans.findOne({ where: { id: params.id, active: true } });
        if (!adminObjectionsDetails) throw new HTTPError(404, `Plans with id: ${params.id} was not found`);
        await adminObjectionsDetails.destroy();
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(adminObjectionsDetails),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not destroy the Plans.' }),
        };
    }
};

const getAllPricingPlans = async (event) => {
    try {
        const { sequelize } = await connectToDatabase();

        let selectQuery = `SELECT Plans.id, Plans.stripe_product_id, Plans.stripe_price_id, Plans.plan_type, Plans.name, Plans.price, Plans.validity, Users.id AS 'created_by_admin_id', Plans.active, Plans.order, Users.name AS 'created_by_admin_name' FROM Plans LEFT JOIN Users ON Plans.created_by_admin_id = Users.id WHERE Plans.active IS TRUE ORDER BY Plans.order ASC`;

        const data = await sequelize.query(selectQuery, {
            type: QueryTypes.SELECT
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(data),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not destroy the Plans.' }),
        };
    }
}

module.exports.create = create;
module.exports.getAll = getAll;
module.exports.getOne = getOne;
module.exports.update = update;
module.exports.destroy = destroy;
module.exports.getAllPricingPlans = getAllPricingPlans;


