const Validator = require('validatorjs');
const { HTTPError } = require('../../utils/httpResp');

exports.validateCreate = function (data) {
    const rules = {
        name: 'required',
        plan_type: 'required'
    }
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all())
}