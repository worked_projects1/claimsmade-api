module.exports = (sequelize, type) => sequelize.define('Plans', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    stripe_product_id: type.STRING,
    stripe_price_id: type.STRING,
    plan_type: type.STRING,
    name: type.STRING,
    price: type.FLOAT,
    description: type.TEXT,
    validity: type.INTEGER,
    features_included: type.TEXT,
    custom_pricing_text: type.TEXT, // JSON Data
    created_by_admin_id: type.STRING,
    active: type.BOOLEAN,
    order: type.INTEGER,
}, {
    updatedAt: 'updated_at',
    createdAt: 'created_at',
});
