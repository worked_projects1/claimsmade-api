module.exports = (sequelize, type) => sequelize.define('Practices', {
  id: {
    type: type.STRING,
    primaryKey: true,
  },
  name: type.STRING,
  contact_name: type.STRING,
  address: type.STRING,
  city: type.STRING,
  state: type.STRING,
  zip_code: type.STRING,
  fax_number: type.STRING,
  npi_number: type.STRING,
  phone_number: type.STRING,
  logoFile: type.STRING,
  stripe_customer_id: type.STRING,
  is_deleted: type.BOOLEAN,
  created_by: type.STRING
}, {
  updatedAt: 'updated_at',
  createdAt: 'created_at'
});
