const { HTTPError } = require('../../utils/httpResp');
const majorRoles = ['superAdmin', 'manager'];
const practiceReadAccessRole = ['superAdmin', 'manager', 'operator'];

exports.authorizeCreate = function (user) {
  if (!practiceReadAccessRole.includes(user.role)) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeDestroy = function (user) {
  if (user.is_admin === false) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeUpdate = function (user) {
  if (user.is_admin === false) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeGetAll = function (user) {
  if (!practiceReadAccessRole.includes(user.role)) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeGetOne = function (user) {
  if (user.is_admin === false) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeGetAllPracticeNames = function (user) {
  if (!majorRoles.includes(user.role)) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};

exports.authorizeGetPracticesForPricing = function (user) {
  if (!majorRoles.includes(user.role)) {
    throw new HTTPError(403, 'you don\'t have access');
  }
};
