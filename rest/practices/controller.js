const connectToDatabase = require('../../db');
const { QueryTypes } = require('sequelize');
const { validateCreatePractices, validateDeletePractice, validateUpdatePractices, validateGetOnePractice } = require('./validation');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const authMiddleware = require('../../auth');
const uuid = require('uuid');
const { authorizeCreate, authorizeDestroy, authorizeUpdate, authorizeGetAll, authorizeGetOne, authorizeGetAllPracticeNames, authorizeGetPracticesForPricing } = require('./authorize');
const { HTTPError } = require('../../utils/httpResp');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();


const getAll = async (event) => {
  try {
    const { sequelize } = await connectToDatabase();
    let searchKey = '';
    let sortQuery = '';
    let searchQuery = '';
    const query = event.headers;
    authorizeGetAll(event.user);
    if (query.offset) query.offset = parseInt(query.offset, 10);
    if (query.limit) query.limit = parseInt(query.limit, 10);
    if (query.search) searchKey = query.search;

    let where = ` WHERE is_deleted IS NOT TRUE`;

    /**Sort**/
    if (query.sort == 'false') {
      sortQuery += ` ORDER BY created_at DESC`
    } else if (query.sort != 'false') {
      query.sort = JSON.parse(query.sort);
      sortQuery += ` ORDER BY ${query.sort.column} ${query.sort.type}`;
    }

    /**Search**/
    if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
      searchQuery = ` AND (name LIKE '%${searchKey}%' OR address LIKE '%${searchKey}%' OR city LIKE '%${searchKey}%' OR state LIKE '%${searchKey}%' OR zip_code LIKE '%${searchKey}%' OR fax_number LIKE '%${searchKey}%' OR npi_number LIKE '%${searchKey}%' OR phone_number LIKE '%${searchKey}%' OR created_at LIKE '%${searchKey}%')`;
    }

    /**Limit**/
    let limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset}`;

    let sqlQuery = 'SELECT id, name, created_at, address, city, state, zip_code, phone_number, fax_number, npi_number, logoFile FROM Practices';

    let sqlQueryCount = 'SELECT COUNT(*) FROM Practices';

    sqlQuery += where + searchQuery + sortQuery + limitQuery;
    sqlQueryCount += where + searchQuery;

    const serverData = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT
    });
    const tableDataCount = await sequelize.query(sqlQueryCount, {
      type: QueryTypes.SELECT
    });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Expose-Headers': 'totalPageCount',
        'totalPageCount': tableDataCount[0]['COUNT(*)']
      },
      body: JSON.stringify(serverData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not fetch the practices.' }),
    };
  }
};

const getAllPracticesNames = async (event) => {
  try {
    authorizeGetAllPracticeNames(event.user);
    const { Practices, Op } = await connectToDatabase();
    let practices = await Practices.findAll({
      where: {
        is_deleted: { [Op.not]: true }
      },
      order: [
        ['name', 'ASC'],
      ],
      attributes: ['id', 'name']
    });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(practices),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not fetch the practices.' }),
    };
  }
};


// This api will return deleted practice records also
const getPracticesForPricing = async (event) => {
  try {
    authorizeGetPracticesForPricing(event.user);
    const { Practices } = await connectToDatabase();
    let practices = await Practices.findAll({
      where: {},
      order: [
        ['name', 'ASC'],
      ],
      attributes: ['id', 'name']
    });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(practices),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not fetch the practices.' }),
    };
  }
};

const create = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

    authorizeCreate(event.user);
    const dataObject = Object.assign(input, { id: input.id || uuid.v4(), created_by: event.user.id });
    validateCreatePractices(dataObject);
    const { Practices, Op } = await connectToDatabase();

    const practiceObject = await Practices.findOne({
      where: {
        name: dataObject.name,
        is_deleted: { [Op.not]: true }
      },
    });

    if (practiceObject) throw new HTTPError(400, `Practice with name: ${dataObject.name} already exist`);

    const practices = await Practices.create(dataObject);

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(practices),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not create the practices.' }),
    };
  }
};

const destroy = async (event) => {
  try {
    authorizeDestroy(event.user);
    validateDeletePractice(event.pathParameters);
    const { Practices, Users, Op } = await connectToDatabase();
    const practiceDetails = await Practices.findOne({
      where: {
        id: event.pathParameters.id,
        is_deleted: { [Op.not]: true }
      },
    });
    if (!practiceDetails) throw new HTTPError(404, `Practices with id: ${event.pathParameters.id} was not found`);

    await Users.destroy({ where: { practice_id: practiceDetails.id } });
    practiceDetails.is_deleted = true;
    await practiceDetails.save();
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'ok',
        message: 'Practice and it\'s related data deleted successfully',
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not delete the Practice' }),
    };
  }
};

const update = async (event) => {
  try {
    authorizeUpdate(event.user);
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    validateUpdatePractices(input);

    const { Practices, Op } = await connectToDatabase();
    const practices = await Practices.findOne({
      where: {
        id: event.pathParameters.id,
        is_deleted: { [Op.not]: true }
      },
    });
    if (!practices) throw new HTTPError(400, `Practice with id: ${event.pathParameters.id} was not found`);

    const practiceObject = await Practices.findOne({
      where: {
        name: input.name,
        is_deleted: { [Op.not]: true },
        id: { [Op.not]: event.pathParameters.id }
      },
    });
    if (practiceObject) throw new HTTPError(404, `Practice with name: ${input.name} already exist`);

    const updatedModel = Object.assign(practices, input);
    const updatePractice = await updatedModel.save();
    const practiceData = updatePractice.get({ plain: true });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(practiceData),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not update the Practices.' }),
    };
  }
};

const getOne = async (event) => {
  try {
    //authorizeGetOne(event.user);
    validateGetOnePractice(event.pathParameters);
    const { Practices, Op, Subscriptions, Plans } = await connectToDatabase();

    const practices = await Practices.findOne({ where: { id: event.pathParameters.id, is_deleted: { [Op.not]: true } }, });
    if (!practices) throw new HTTPError(404, `Practices with id: ${event.pathParameters.id} was not found`);

    const practiceplain = practices.get({ plain: true });

    const subscriptionObj = await Subscriptions.findOne({ where: { practice_id: practiceplain.id }, raw: true, order: [['created_at', 'DESC']] });
    let planObj;

    if (subscriptionObj && subscriptionObj.plan_id) {
      planObj = await Plans.findOne({ where: { id: subscriptionObj.plan_id, active: true } });
      //user.plan_type = planObj.plan_type;
    }

    if (subscriptionObj && subscriptionObj.plan_id && planObj && planObj.plan_type && planObj.plan_type == 'pay_as_you_go') {
      practiceplain.plan_type = 'pay_as_you_go';
      practiceplain.subscription_plan_status = 'active_plan';
    } else if (subscriptionObj && subscriptionObj.plan_id && planObj && planObj.plan_type && planObj.plan_type != 'pay_as_you_go') {
      // const planObj = await Plans.findOne({ where: { plan_id: subscriptionObj.plan_id, active: true } });
      practiceplain.plan_type = planObj.plan_type;
      practiceplain.subscription_plan_status = 'active_plan';
    } else if (subscriptionObj && !subscriptionObj.plan_id) { //consider as plan expired.
      practiceplain.plan_type = '';
      practiceplain.subscription_plan_status = 'plan_expired';
    } else {
      practiceplain.plan_type = 'pay_as_you_go';
      practiceplain.subscription_plan_status = 'active_plan';
    }
    if (subscriptionObj && subscriptionObj.plan_id && subscriptionObj.stripe_subscription_data) {
      const stripe_obj = JSON.parse(subscriptionObj.stripe_subscription_data);
      practiceplain.plan_cancel_at_period = stripe_obj.cancel_at_period_end;
      practiceplain.current_period_start = stripe_obj.current_period_start;
      practiceplain.current_period_end = stripe_obj.current_period_end;
    } else {
      practiceplain.plan_cancel_at_period = true;
    }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(practiceplain),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not fetch the Practices.' }),
    };
  }
};

const getUploadURLForS3 = async (input) => {
  let fileExtention = '';
  if (input.file_name) {
    fileExtention = `.${input.file_name.split('.').pop()}`;
  }

  const s3Params = {
    Bucket: process.env.S3_BUCKET_FOR_PUBLIC_ASSETS,
    Key: `${input.file_name}.${uuid.v4()}.${fileExtention}`,
    ContentType: input.content_type,
    ACL: 'public-read',
  };

  return new Promise((resolve, reject) => {
    const uploadURL = s3.getSignedUrl('putObject', s3Params);
    resolve({
      uploadURL,
      s3_file_key: s3Params.Key,
      public_url: `https://${s3Params.Bucket}.s3.amazonaws.com/${s3Params.Key}`,
    });
  });
};

const getUploadURL = async (event) => {
  try {
    const input = JSON.parse(event.body);
    const uploadURLObject = await getUploadURLForS3(input);
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        uploadURL: uploadURLObject.uploadURL,
        s3_file_key: uploadURLObject.s3_file_key,
        public_url: uploadURLObject.public_url,
      }),
    };
  } catch (err) {
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not get the upload URL.' }),
    };
  }
};


module.exports.getAll = getAll;
module.exports.getAllPracticesNames = getAllPracticesNames;
module.exports.create = create;
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getUploadURL = middy(getUploadURL).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getPracticesForPricing = getPracticesForPricing;



