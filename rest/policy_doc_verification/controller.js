const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.S3_BUCKET_DEFAULT_REGION || 'us-west-2' });
const s3 = new AWS.S3();
const { readFileContent, uploadContentToS3, deleteObjectsFroms3, getDownloadUrl, getAllKeyObjects } = require('../../utils/awsService');
const connectToDatabase = require('../../db');
const { decrypt } = require('../../utils/encrptDecrypt');
const { updateDiagnosis, removeSpaces, removeHyphen, prefixName, removeSlash, removeDuplicateDiagnosis, diagnosisNameValidation, updateReferenceLink, areBracketsBalanced } = require('./validation');
const { HTTPError } = require('../../utils/httpResp');
const JSZip = require('jszip');

const getVerificationResponse = async (treatmentFileKeys, created_by) => {

    let bucketName = process.env.POLICY_DOC_VERIFICATION_UPLOAD
    let jsonObj = {};
    let treatmentName = '';
    let treatmentNameForUpload = '';
    let failureTreatmentContent = {};
    let key = '';

    try {
        for (let i = 0; i < treatmentFileKeys.length; i++) {

            key = treatmentFileKeys[i]
            let jsonFileContent = await readFileContent(key, bucketName);

            let inputContent = typeof jsonFileContent === 'string' ? JSON.parse(jsonFileContent) : jsonFileContent;

            failureTreatmentContent = inputContent;

            treatmentNameForUpload = inputContent["treatment"]["index-name"] + '.json';

            // To remove the spaces in the treatment name
            treatmentName = inputContent["treatment"]["label-name"];
            inputContent["treatment"]["label-name"] = removeSpaces(treatmentName);

            // To remove spaces, hyphen, bracket validations in the diagnosis
            inputContent["policies"] = diagnosisNameValidation(inputContent["policies"]);

            // To remove the unwanted hyphen from the treatment index value
            let treatmentIndexValue = inputContent["treatment"]["index-name"];
            inputContent["treatment"]["index-name"] = await removeHyphen(treatmentIndexValue);

            //To check whether the treatment names have balanced brackets
            inputContent["treatment"]["label-name"] = await areBracketsBalanced(inputContent["treatment"]["label-name"]);
            inputContent["treatment"]["index-name"] = await areBracketsBalanced(inputContent["treatment"]["index-name"]);

            // To Remove Duplicate Diagnosis from the input file
            inputContent["policies"] = removeDuplicateDiagnosis(inputContent["policies"]);

            // To Find out the duplicate diagnosis and merging the disgnosis if there is no duplicate
            const { BodySystem, Treatments } = await connectToDatabase();
            const bodySystem = await BodySystem.findOne({ where: { name: inputContent["body-system"]["label-name"] } });

            const treatment = await Treatments.findOne({ where: { name: inputContent["treatment"]["label-name"], body_system_id: bodySystem.id, type: inputContent["treatment"]["treatment-type"] } });

            if (treatment && treatment.id) {
                const diagnosis = JSON.parse(await decrypt(treatment.policy_doc)).policies;
                for (let i = 0; i < diagnosis.length; i++) {
                    for (let j = 0; j < inputContent["policies"].length; j++) {
                        if (inputContent["policies"][j]["diagnosis"]["label-name"] == diagnosis[i].diagnosis['label-name']) {
                            throw new HTTPError(400, `Diagnosis name: '${inputContent["policies"][j]["diagnosis"]["label-name"]}' already exists`)
                        }
                    }
                }
                if (!jsonObj || !jsonObj.treatment) {
                    jsonObj = JSON.parse(await decrypt(treatment.policy_doc));
                }
                jsonObj.policies = updateDiagnosis(jsonObj, inputContent["policies"])
            } else {
                if (!jsonObj || !jsonObj.treatment) {
                    jsonObj = inputContent;
                } else {
                    jsonObj.policies = updateDiagnosis(jsonObj, inputContent["policies"])
                }
            }

            // need to merge the reference link here
            // To update the reference link
            jsonObj.references = updateReferenceLink(jsonObj.references, inputContent['references'])
        }

    } catch (err) {
        console.log(err);
        return {
            status: 'failed',
            file_name: key,
            failedKeys: treatmentFileKeys,
            treatment_name: treatmentName,
            treatmentNameForUpload: treatmentNameForUpload,
            failureTreatmentContent: JSON.stringify(failureTreatmentContent, null, 2),
            reason: err.message
        }
    }

    //To change the created-by name
    jsonObj.version['created-by'] = created_by;

    return {
        status: 'success',
        treatmentName: jsonObj["treatment"]["index-name"] + '.json',
        verifiedKeys: treatmentFileKeys,
        content: JSON.stringify(jsonObj, null, 2)
    };
}


const getUploadUrlForPolicyDocVerify = async (event) => {

    try {

        const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

        let bucketKey = `${input.file_name}`;
        let bucketName = process.env.POLICY_DOC_VERIFICATION_UPLOAD;

        const s3Params = {
            Bucket: bucketName,
            Key: bucketKey, //Should be unique (eg: timeStamp)
            ContentType: input.content_type,
            ACL: 'private'
        };
        //const s3_bucket = new AWS.S3({ useAccelerateEndpoint: true });

        const uploadURLObject = await new Promise((resolve, reject) => {
            const uploadURL = s3.getSignedUrl('putObject', s3Params);
            resolve({
                uploadURL,
                s3_file_key: s3Params.Key,
            });
        });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                uploadURL: uploadURLObject.uploadURL,
                s3_file_key: uploadURLObject.s3_file_key,
            }),
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the upload URL.' }),
        };
    }
};

const verifyPolicyDocs = async (event) => {
    try {
        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        const created_by = input.created_by ? input.created_by : 'Siva';
        let verifiedTreatments = [];
        let failedTreatments = [];
        const uploadedTreatments = input.s3_file_keys;

        let response = [];
        let successFileszip = new JSZip();

        let treatmentResponse = [];

        uploadedTreatments.sort();
        for (let i = 0; i < uploadedTreatments.length; i++) {
            let thisTreatment = prefixName(uploadedTreatments[i]);
            let sameTreatmentKeys = [];
            for (let j = 0; j < uploadedTreatments.length; j++) {
                if (thisTreatment == prefixName(uploadedTreatments[j])) {
                    sameTreatmentKeys.push(uploadedTreatments[j])
                }
            }

            treatmentResponse.push(getVerificationResponse(sameTreatmentKeys, created_by));

            thisTreatment = uploadedTreatments[i + sameTreatmentKeys.length]
            i += (sameTreatmentKeys.length - 1);
        }
        treatmentResponse = await Promise.all(treatmentResponse);

        let successFileCount = 0;
        let downloadURLResponse = {};

        for (let i = 0; i < treatmentResponse.length; i++) {
            if (treatmentResponse[i].status == 'success') {

                successFileCount += 1;
                successFileszip.file(removeSlash(treatmentResponse[i].treatmentName), treatmentResponse[i].content, { binary: false });
                verifiedTreatments = verifiedTreatments.concat(treatmentResponse[i].verifiedKeys);

            } else if (treatmentResponse[i].status == 'failed') {
                response.push(treatmentResponse[i]);
                failedTreatments = failedTreatments.concat(treatmentResponse[i].failedKeys)
            }

        }

        if (successFileCount > 0) {
            const zipBuffer = await successFileszip.generateAsync({ type: "nodebuffer", compression: 'DEFLATE' });
            const zipFileNameForSuccess = `Success_Policies_${Date.now()}.zip`
            await uploadContentToS3(process.env.VERIFIED_POLICY_DOC, zipFileNameForSuccess, zipBuffer, 'application/zip', 'private');
            let download_url = await getDownloadUrl(process.env.VERIFIED_POLICY_DOC, zipFileNameForSuccess, '', process.env.S3_BUCKET_DEFAULT_REGION);
            Object.assign(downloadURLResponse, { success_files_download_url: download_url.body.publicUrl });
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                total_count: uploadedTreatments.length,
                success_count: verifiedTreatments.length,
                failed_count: failedTreatments.length,
                command: `aws s3 cp s3://verified-policy-result-doc/ . --recursive`,
                treatments: response,
                failedFilesKeys: failedTreatments,
                download_url: downloadURLResponse

            })
        };
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not verify the policy documents.' }),
        };
    }
}

const deleteObjects = async (event) => {
    try {
        let uploadbucket = process.env.POLICY_DOC_VERIFICATION_UPLOAD; // bucket name that is used for uploading the policy docs

        const responseData = await deleteObjectsFroms3(uploadbucket);

        let resultBucket = process.env.VERIFIED_POLICY_DOC; // bucket name that is used for uploading the result policy document after the verification

        const responseData1 = await deleteObjectsFroms3(resultBucket);

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ "Deleted Count": responseData }),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the keys.' }),
        };

    }
}

const checkExistFileCount = async (event) => {
    try {

        let isFileExist = false;

        let uploadbucket = process.env.POLICY_DOC_VERIFICATION_UPLOAD; // bucket name that is used for uploading the policy docs

        const responseData = await getAllKeyObjects(uploadbucket);

        let resultBucket = process.env.VERIFIED_POLICY_DOC; // bucket name that is used for uploading the result policy document after the verification

        const responseData1 = await getAllKeyObjects(resultBucket);

        if (responseData && responseData.length && (responseData.length > 0)) {
            isFileExist = true;
        } else if (responseData1 && responseData1.length && (responseData1.length > 0)) {
            isFileExist = true;
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ "isFileExist": isFileExist }),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the Existing File Count' }),
        };

    }
}

const getDownloadUrlForFailedDocs = async (event) => {
    try {

        const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
        let treatmentFileKeys = input.fileKeys;
        let downloadURLResponse = {};
        let zip = new JSZip();

        let treatmentResponse = [];

        for (i = 0; i < treatmentFileKeys.length; i++) {

            let key = treatmentFileKeys[i];
            let bucketName = process.env.POLICY_DOC_VERIFICATION_UPLOAD

            treatmentResponse.push(readFileContent(key, bucketName));

        }

        treatmentResponse = await Promise.all(treatmentResponse);

        for (let i = 0; i < treatmentResponse.length; i++) {

            let fileContent = typeof treatmentResponse[i] === 'string' ? JSON.parse(treatmentResponse[i]) : treatmentResponse[i];
            let treatmentkey = treatmentFileKeys[i];
            let uploadContent = JSON.stringify(fileContent, null, 2);

            zip.file(removeSlash(treatmentkey), uploadContent, { binary: false });

        }

        const zipBuffer = await zip.generateAsync({ type: "nodebuffer", compression: 'DEFLATE' });
        const zipFileNameForFailure = `Failed_Policies_${Date.now()}.zip`
        await uploadContentToS3(process.env.VERIFIED_POLICY_DOC, zipFileNameForFailure, zipBuffer, 'application/zip', 'private');
        let download_url = await getDownloadUrl(process.env.VERIFIED_POLICY_DOC, zipFileNameForFailure, '', process.env.S3_BUCKET_DEFAULT_REGION);
        Object.assign(downloadURLResponse, { failure_files_download_url: download_url.body.publicUrl });

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(downloadURLResponse),
        };

    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the download URL' }),
        };

    }
}


module.exports.verifyPolicyDocs = verifyPolicyDocs;
module.exports.getUploadUrlForPolicyDocVerify = getUploadUrlForPolicyDocVerify;
module.exports.deleteObjects = deleteObjects;
module.exports.checkExistFileCount = checkExistFileCount;
module.exports.getDownloadUrlForFailedDocs = getDownloadUrlForFailedDocs;

