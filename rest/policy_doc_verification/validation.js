
exports.updateDiagnosis = function (jsonObj, policies) {

    if (!policies.length > 0) return [];

    const diagnoses = [];
    for (var i = 0; i < jsonObj.policies.length; i++) {
        diagnoses.push(jsonObj.policies[i].diagnosis['label-name'])
    }

    let updated_policies = [];
    updated_policies = jsonObj.policies;

    for (var i = 0; i < policies.length; i++) {
        if (!diagnoses.includes(policies[i].diagnosis['label-name'])) {
            updated_policies.push(policies[i]);
            diagnoses.push(policies[i].diagnosis['label-name']);
        }
    }
    return updated_policies;
}


exports.updateReferenceLink = function (existingRef, inputRef) {

    if (existingRef['mtus-link'] != "") {
        existingRef['mtus-link'] = updateMtusLink(existingRef, inputRef);
    } else {
        existingRef['odg-link'] = updateOdgLink(existingRef, inputRef);
    }

    return existingRef;
}



const updateMtusLink = function (existingRef, inputRef) {
    let inputRefLinks = '';

    if (inputRef['mtus-link']) {
        if (existingRef['mtus-link'] && !existingRef['mtus-link'].split(',').includes(inputRef['mtus-link'])) {
            inputRefLinks = existingRef['mtus-link'] + ',' + inputRef['mtus-link']
        } else {
            inputRefLinks = inputRef['mtus-link']
        }
    } else if (inputRef['odg-link']) {
        if (existingRef['mtus-link'] && !existingRef['mtus-link'].split(',').includes(inputRef['odg-link'])) {
            inputRefLinks = existingRef['mtus-link'] + ',' + inputRef['odg-link']
        } else {
            inputRefLinks = inputRef['odg-link']
        }
    }
    return removeDuplicateLinks(inputRefLinks)
}

const updateOdgLink = function (existingRef, inputRef) {
    let inputRefLinks = '';

    if (inputRef['mtus-link']) {
        if (existingRef['odg-link'] && !existingRef['odg-link'].split(',').includes(inputRef['mtus-link'])) {
            inputRefLinks = existingRef['odg-link'] + ',' + inputRef['mtus-link']
        } else {
            inputRefLinks = inputRef['mtus-link']
        }
    } else if (inputRef['odg-link']) {
        if (existingRef['odg-link'] && !existingRef['odg-link'].split(',').includes(inputRef['odg-link'])) {
            inputRefLinks = existingRef['odg-link'] + ',' + inputRef['odg-link']
        } else {
            inputRefLinks = inputRef['odg-link']
        }
    }

    return removeDuplicateLinks(inputRefLinks);
}

const removeDuplicateLinks = function (refLinks) {
    let linkList = refLinks.split(',');
    let filteredList = [...new Set(linkList)];

    let uniqueList = '';
    for (let i = 0; i < filteredList.length; i++) {
        uniqueList += filteredList[i];

        if (filteredList[i + 1]) {
            uniqueList += ','
        }
    }
    return uniqueList;
}

exports.removeDuplicateDiagnosis = function (policies) {
    let updated_policies = [];
    let diagnoses = [];

    for (let i = 0; i < policies.length; i++) {
        if (!diagnoses.includes(policies[i].diagnosis['label-name'])) {
            updated_policies.push(policies[i]);
            diagnoses.push(policies[i].diagnosis['label-name'])
        }
    }
    return updated_policies;
}

exports.diagnosisNameValidation = function (policies) {
    let updated_policies = [];

    for (let i = 0; i < policies.length; i++) {

        // remove spaces
        policies[i].diagnosis['label-name'] = policies[i].diagnosis['label-name'].trim();

        // remove hyphen
        policies[i].diagnosis['index-name'] = removeHyphen(policies[i].diagnosis['index-name']);

        // bracket validation
        policies[i].diagnosis['index-name'] = areBracketsBalanced(policies[i].diagnosis['index-name']);
        policies[i].diagnosis['label-name'] = areBracketsBalanced(policies[i].diagnosis['label-name']);

        updated_policies.push(policies[i]);
    }

    function removeHyphen(name) {

        let firstChar = name.charAt(0)
        let lastChar = name.charAt(name.length - 1)

        if (firstChar == '-') {
            name = name.substring(1, name.length);
        }
        if (lastChar == '-') {
            name = name.substring(0, name.length - 1);
        }

        return name;
    }

    function areBracketsBalanced(input) {

        let stack = [];
        let resultName = input;

        for (let i = 0; i < input.length; i++) {
            let x = input[i];

            if (x == '(' || x == ')') {
                stack.push(x);
            }

        }

        //To add the missing close bracket at the end of string
        if (stack.length && stack.length == 1 && stack[0] == '(') {
            resultName = input + ')';
        }

        return resultName;
    }

    return updated_policies;
}

exports.removeHyphenInDiagnosis = function (policies) {
    let updated_policies = [];

    function removeHyphen(name) {

        let firstChar = name.charAt(0)
        let lastChar = name.charAt(name.length - 1)

        if (firstChar == '-') {
            name = name.substring(1, name.length);
        }
        if (lastChar == '-') {
            name = name.substring(0, name.length - 1);
        }

        return name;
    }

    for (let i = 0; i < policies.length; i++) {
        policies[i].diagnosis['index-name'] = removeHyphen(policies[i].diagnosis['index-name']);
        updated_policies.push(policies[i]);
    }

    return updated_policies;
}

exports.removeSpaces = function (name) {
    return name.trim();
}

exports.removeHyphen = function (name) {

    let firstChar = name.charAt(0)
    let lastChar = name.charAt(name.length - 1)

    if (firstChar == '-') {
        name = name.substring(1, name.length);
    }
    if (lastChar == '-') {
        name = name.substring(0, name.length - 1);
    }

    return name;
}

exports.prefixName = function (treatmentName) {
    const regex = /_\d*.json/;
    return treatmentName.replace(regex, '');
}

exports.removeSlash = function (name) {
    const regex = /[/]/g;
    return name.replace(regex, '-');
}

exports.areBracketsBalanced = async (input) => {
    let stack = [];
    let resultName = input;

    for (let i = 0; i < input.length; i++) {
        let x = input[i];

        if (x == '(' || x == ')') {
            stack.push(x);
        }

    }

    //To add the missing close bracket at the end of string
    if (stack.length && stack.length == 1 && stack[0] == '(') {
        resultName = input + ')';
    }

    return resultName;
}

exports.areBracketsBalancedForDiagnosis = function (policies) {
    let updated_policies = [];

    function areBracketsBalanced(input) {

        let stack = [];
        let resultName = input;

        for (let i = 0; i < input.length; i++) {
            let x = input[i];

            if (x == '(' || x == ')') {
                stack.push(x);
            }

        }

        //To add the missing close bracket at the end of string
        if (stack.length && stack.length == 1 && stack[0] == '(') {
            resultName = input + ')';
        }

        return resultName;
    }

    for (let i = 0; i < policies.length; i++) {
        policies[i].diagnosis['index-name'] = areBracketsBalanced(policies[i].diagnosis['index-name']);
        policies[i].diagnosis['label-name'] = areBracketsBalanced(policies[i].diagnosis['label-name']);

        updated_policies.push(policies[i]);
    }

    return updated_policies;
}
