module.exports = (sequelize, type) => sequelize.define('Treatments', {
    id: {
        type: type.STRING,
        primaryKey: true,
    },
    body_system: type.STRING,
    body_system_id: type.STRING,
    body_system_index: type.STRING,
    name: type.STRING,
    type: type.STRING,
    status: type.STRING,
    s3_file_key: type.STRING,
    policy_doc: type.TEXT('long'),
    is_deleted: type.BOOLEAN,
    created_by: type.STRING
}, {
    updatedAt: 'updated_at',
    createdAt: 'created_at'
});
