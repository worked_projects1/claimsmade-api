const connectToDatabase = require('../.././db');
const { validateCreateTreatment, validateUpdateTreatment, validateGetOneTreatment, validateuploadUrl, validateTreatmentStatus } = require('./validation');
const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const authMiddleware = require('../../auth');
const uuid = require('uuid');
const { authorizeCreate, authorizeGetPolicies, authorizeDestroy, authorizeGetOne, authorizeGetAll, authorizeAdmin } = require('./authorize');
const { HTTPError } = require('../../utils/httpResp');
const { encrypt } = require('../.././utils/encrptDecrypt');
const { QueryTypes } = require('sequelize');
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION || 'us-west-2' });
const s3 = new AWS.S3();
const { appConstants } = require('../../appConstants');
const https = require('https');

const create = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    authorizeAdmin(event.user);
    const inputObj = {
      body_system: input["body-system"]["label-name"],
      name: input.treatment["label-name"],
      type: input.treatment["treatment-type"]
    }
    validateCreateTreatment(inputObj);
    if (inputObj.type == "surgery-add-on" && input["policies"].length > 1) throw new HTTPError(400, `Treatment type with 'Suergery Add-on' could not have more than one Diagnosis`)
    const encryptedObj = await encrypt(input);
    const { Treatments, Op, Diagnosis, BodySystem, Settings } = await connectToDatabase();

    const existingObj = await Treatments.findOne({
      where: {
        name: input.treatment["label-name"],
        body_system: input["body-system"]["label-name"],
        type: input.treatment["treatment-type"]
      },
    });

    if (existingObj) throw new HTTPError(400, `Treatment with name: ${existingObj.name} already exist.`)
    const bodySystemObj = await BodySystem.findOne({
      where: { is_deleted: { [Op.not]: true }, name: input["body-system"]["label-name"] }
    });
    if (!bodySystemObj) throw new HTTPError(400, `Body System not found`);

    const treatmentObj = Object.assign({
      id: input.id || uuid.v4(),
      body_system_id: bodySystemObj.id,
      body_system: input["body-system"]["label-name"],
      body_system_index: input["body-system"]["index-name"],
      name: input.treatment["label-name"],
      type: input.treatment["treatment-type"],
      policy_doc: encryptedObj,
      status: appConstants.STATUS_ACTIVE,
      created_by: input.version["created-by"]
    });

    const treatment = await Treatments.create(treatmentObj);

    const policy_datas = input.policies;
    if (treatment.type && treatment.type != 'surgery-add-on') {
      if (treatment && policy_datas && policy_datas.length > 0) {
        for (let i = 0; i < policy_datas.length; i += 1) {
          const diagnosisName = policy_datas[i].diagnosis["label-name"];
          const source = policy_datas[i].source;
          await Diagnosis.create({
            id: uuid.v4(),
            name: diagnosisName,
            body_system_id: bodySystemObj.id,
            treatment_id: treatment.id,
            source,
            created_by: treatment.created_by
          });
        }
      } else {
        await Diagnosis.create({
          id: uuid.v4(),
          body_system_id: bodySystemObj.id,
          name: "NoDiagnosisFound",
          treatment_id: treatment.id,
          source: "",
          created_by: treatment.created_by
        });
      }
    }

    const settings = await Settings.findOne({
      where: { key: 'global_settings' }
    });
    const updatedObject = Object.assign(JSON.parse(settings.value), { policy_doc_updated_date: new Date() })
    settings.value = JSON.stringify(updatedObject);
    await settings.save();

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(treatment),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not create the treatment.' }),
    };
  }
};

const update = async (event) => {
  try {
    const treatment = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const existingTreatmentId = event.pathParameters.id;
    if (existingTreatmentId == "undefined") throw new HTTPError(400, `Could not update Treatment`)
    authorizeAdmin(event.user);

    const inputObj = {
      body_system: treatment["body-system"]["label-name"],
      name: treatment.treatment["label-name"],
      type: treatment.treatment["treatment-type"]
    }
    validateUpdateTreatment(inputObj);
    const { Treatments, Diagnosis, Op, BodySystem, Settings } = await connectToDatabase();

    const bodySystemObj = await BodySystem.findOne({
      where: { is_deleted: { [Op.not]: true }, name: treatment["body-system"]["label-name"] }
    });
    if (!bodySystemObj) throw new HTTPError(400, `Body Systsem not found`);

    //duplicate restriction
    const alreadyExist = await Treatments.findOne({
      where: { id: { [Op.not]: existingTreatmentId }, name: inputObj.name, is_deleted: { [Op.not]: true }, body_system: inputObj.body_system, type: inputObj.type }
    });
    if (alreadyExist) throw new HTTPError(400, `Treatment with ${inputObj.name} already exist`);

    //to remove existing record
    const existingTreatment = await Treatments.findOne({
      where: {
        id: existingTreatmentId,
        is_deleted: { [Op.not]: true }
      }
    });
    if (!existingTreatment) throw new HTTPError(404, `Record not found`)

    await Diagnosis.destroy({ where: { treatment_id: existingTreatment.id } });
    await Treatments.destroy({
      where: {
        id: existingTreatment.id
      }
    });

    const encryptedText = await encrypt(treatment);
    const treatmentObject = {
      id: existingTreatmentId,
      body_system_id: bodySystemObj.id,
      body_system: treatment["body-system"]["label-name"],
      body_system_index: treatment["body-system"]["index-name"],
      name: treatment.treatment["label-name"],
      type: treatment.treatment["treatment-type"],
      status: appConstants.STATUS_ACTIVE,
      policy_doc: encryptedText,
      created_by: treatment.version["created-by"]
    };

    const treatmentResponse = await Treatments.create(treatmentObject);
    const policy_datas = treatment.policies;
    if (treatmentResponse.type && treatmentResponse.type != 'surgery-add-on') {
      if (treatmentResponse && policy_datas && policy_datas.length > 0) {
        for (let i = 0; i < treatment.policies.length; i++) {
          const diagnosisObject = {
            id: uuid.v4(),
            body_system_id: bodySystemObj.id,
            treatment_id: treatmentResponse.id,
            name: treatment.policies[i].diagnosis["label-name"],
            source: treatment.policies[i].source,
            created_by: treatmentResponse.created_by
          };
          await Diagnosis.create(diagnosisObject);
        }
      } else {
        const diagnosisObj = Object.assign({
          id: uuid.v4(),
          body_system_id: bodySystemObj.id,
          name: "NoDiagnosisFound",
          treatment_id: treatmentResponse.id,
          source: "",
          created_by: treatmentResponse.created_by
        });
        await Diagnosis.create(diagnosisObj);
      }
    }

    const settings = await Settings.findOne({
      where: { key: 'global_settings' }
    });
    const updatedObject = Object.assign(JSON.parse(settings.value), { policy_doc_updated_date: new Date() })
    settings.value = JSON.stringify(updatedObject);
    await settings.save();

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(treatmentResponse),
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: error.message || 'Could not update the reatment' }),
    };
  }
}

const updateTreatmentStatus = async (event) => {
  try {
    const input = typeof event.body == "string" ? JSON.parse(event.body) : event.body;
    const treatmentId = event.pathParameters.id;
    validateTreatmentStatus(input);
    const { Treatments, Op, Settings } = await connectToDatabase();
    const treatmentObj = await Treatments.findOne({
      where: {
        id: treatmentId,
        is_deleted: { [Op.not]: true }
      }
    });
    treatmentObj.status = input.status
    const response = await treatmentObj.save();

    const settings = await Settings.findOne({
      where: { key: 'global_settings' }
    });
    const updatedObject = Object.assign(JSON.parse(settings.value), { policy_doc_updated_date: new Date() })
    settings.value = JSON.stringify(updatedObject);
    await settings.save();

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(response),
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: error.message || 'Could not update the treatment status' }),
    };
  }

}

const getAll = async (event) => {
  try {
    // authorizeGetAll(event.user);
    const query = event.headers;
    if (query.offset) query.offset = parseInt(query.offset, 10);
    if (query.limit) query.limit = parseInt(query.limit, 10);
    query.sort = typeof query.sort == "string" ? JSON.parse(query.sort) : query.sort
    const { sequelize } = await connectToDatabase();

    let selectQuery = `SELECT BodySystems.name as body_system, BodySystems.index as body_system_index, Treatments.created_at, 
    Treatments.created_by, Treatments.id, Treatments.name, Treatments.type, Treatments.status,
    Treatments.s3_file_key, Treatments.updated_at FROM Treatments INNER JOIN BodySystems ON Treatments.body_system_id = BodySystems.id
     WHERE Treatments.is_deleted IS NOT true `;

    let countQuery = `SELECT COUNT(*) FROM Treatments INNER JOIN BodySystems ON Treatments.body_system_id = BodySystems.id
     WHERE Treatments.is_deleted IS NOT true `;

    /*Filter*/
    if (query.filter && query.filter != 'false') {
      const filterObj = JSON.parse(query.filter);
      selectQuery += `AND Treatments.body_system_id = '${filterObj.body_system}' `
      countQuery += `AND Treatments.body_system_id = '${filterObj.body_system}' `
    }

    let searchQuery = '';
    if (query.search != "false") {
      const searchTypeKey = query.search.trim().toLowerCase().replace(/\s/g, '-');
      searchQuery = `AND ( Treatments.name LIKE '%${query.search}%' OR 
                            BodySystems.name LIKE '%${query.search}%' OR
                            Treatments.type LIKE '%${searchTypeKey}%' OR
                            Treatments.created_by LIKE '%${query.search}%' )`;
    }

    let orderQuery;
    if (query.sort && query.sort != "false") {
      orderQuery = ` ORDER BY ${query.sort.column} ${query.sort.type}`;
    } else {
      orderQuery = ` ORDER BY created_at DESC`;
    }

    let limitQuery;
    limitQuery = ` LIMIT ${query.limit} OFFSET ${query.offset};`;

    selectQuery = selectQuery + searchQuery + orderQuery + limitQuery;
    countQuery = countQuery + searchQuery;

    const data = await sequelize.query(selectQuery, {
      type: QueryTypes.SELECT
    });
    const tableDataCount = await sequelize.query(countQuery, {
      type: QueryTypes.SELECT
    });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Expose-Headers': 'totalPageCount',
        'totalPageCount': tableDataCount[0]["COUNT(*)"]
      },
      body: JSON.stringify(data)
    }
  } catch (error) {
    console.log(error);
    const errorResponse = { error: error.message || 'Could not fetch the records' };
    return {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponse)
    };
  }
};

const get_policies = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    authorizeGetPolicies(event.user);
    const test = Object.assign({ input });

    const { Treatments, Op, Diagnosis } = await connectToDatabase();

    let treatments = await Treatments.findAll({
      where: {
        is_deleted: { [Op.not]: true },
        status: { [Op.not]: appConstants.STATUS_INACTIVE },
        body_system_index: input.body_system
      },
      order: [
        ['name', 'ASC'],
      ],
      attributes: ['name', 'id'],

      include: [{
        model: Diagnosis,
        attributes: ['name'],
        where: {

        },
        required: true
      }]
    });

    if (!treatments) throw new HTTPError(404, 'Couldn\'t find any treatment');

    const responseData = {
      "Treatments": treatments
    };

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(responseData),
    };
  } catch (err) {
    console.log(err);
    const errorResponseData = { error: err.message || 'Couldn\'t fetch the policies' };
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponseData),
    };
  }
};


const get_policy_doc = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    // authorizeGetPolicies(event.user);

    const { Treatments, Op } = await connectToDatabase();

    let treatments = await Treatments.findOne({
      where: {
        is_deleted: { [Op.not]: true },
        body_system_index: input.body_system,
        id: input.treatment_id
      },
      attributes: ['policy_doc']
    });

    if (!treatments) throw new HTTPError(404, 'Couldn\'t find any treatment');

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },

      body: JSON.stringify(treatments),
    };
  } catch (err) {
    console.log(err);
    const errorResponseData = { error: err.message || 'Couldn\'t fetch the policy doc' };
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponseData),
    };
  }
}

const destroy = async (event) => {
  try {
    const treatmentId = event.pathParameters.id;
    authorizeDestroy(event.user);
    const { Treatments, Diagnosis } = await connectToDatabase();
    const treatment = await Treatments.findOne({ where: { id: treatmentId } });
    if (!treatment) throw new HTTPError(404, `No treatment found`);
    treatment.is_deleted = true;
    await treatment.save();
    await Diagnosis.destroy({
      where: {
        treatment_id: treatment.id
      }
    });
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        status: 'Ok',
        message: 'Deleted'
      }),
    }
  } catch (error) {
    console.log(error);
    const errorResponseData = { error: error.message || 'Couldn\'t delete the policy' };
    return {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponseData),
    };
  }
};

const getOne = async (event) => {
  try {
    // authorizeGetOne(event.user);
    validateGetOneTreatment(event.pathParameters);
    const { Treatments, Op } = await connectToDatabase();

    let treatment = await Treatments.findOne({
      where: {
        is_deleted: { [Op.not]: true },
        id: event.pathParameters.id
      },
      attributes: { exclude: ['is_deleted', 'updated_at'] }
    });

    if (!treatment) throw new HTTPError(404, `Treatments with id: ${event.pathParameters.id} was not found`);

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(treatment),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not fetch the policy.' }),
    };
  }
};

const getAllDiagnoses = async (event) => {
  try {
    // authorizeGetAll(event.user);
    const query = event.headers;
    let searchKey = '';
    const sortKey = {};
    let sortQuery = '';
    let searchQuery = '';

    if (query.offset) query.offset = parseInt(query.offset, 10);
    if (query.limit) query.limit = parseInt(query.limit, 10);
    if (query.search) searchKey = query.search;

    const { Treatments, sequelize, Op } = await connectToDatabase();
    let codeSnip = ''; // limit,offset
    let codeSnip2 = ''; // totalcount

    let where = `WHERE Treatments.is_deleted IS NOT true AND Treatments.status != '${appConstants.STATUS_INACTIVE}' `;

    /*Filter*/
    if (query.filter && query.filter != "false") {
      const filterObj = JSON.parse(query.filter);
      if (filterObj.body_system && filterObj.body_system != 'all') {
        where += `AND Treatments.body_system_id LIKE '${filterObj.body_system}' `;
      }
      if (filterObj.treatment && filterObj.treatment != 'all') {
        where += `AND Treatments.id LIKE '${filterObj.treatment}' `;
      }
    }

    /**Sort**/
    if (query.sort == 'false') {
      sortQuery += ` ORDER BY Diagnosis.created_at DESC`
    } else if (query.sort != 'false') {
      query.sort = JSON.parse(query.sort);
      sortKey.column = query.sort.column;
      sortKey.type = query.sort.type;

      if (sortKey.column != '' && sortKey.column) {
        sortQuery += ` ORDER BY ${sortKey.column}`; //TableName.colum_name
      }
      if (sortKey.type != '' && sortKey.type) {
        sortQuery += ` ${sortKey.type}`;//asc,desc
      }
    }

    /**Search**/
    if (searchKey != 'false' && searchKey.length >= 1 && searchKey != '') {
      searchQuery = ` Diagnosis.name LIKE '%${searchKey}%' OR Diagnosis.source LIKE '%${searchKey}%' OR BodySystem.name LIKE '%${searchKey}%' OR Treatments.name LIKE '%${searchKey}%' OR Treatments.created_by LIKE '%${searchKey}%' `;
    }

    if (searchQuery != '' && sortQuery != '') {
      codeSnip = where + ' AND (' + searchQuery + ')' + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where + ' AND (' + searchQuery + ')' + sortQuery;
    } else if (searchQuery == '' && sortQuery != '') {
      codeSnip = where + sortQuery + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where + sortQuery;
    } else if (searchQuery != '' && sortQuery == '') {
      codeSnip = where + ' AND (' + searchQuery + ')' + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where + ' AND (' + searchQuery + ')';
    } else if (searchQuery == '' && sortQuery == '') {
      codeSnip = where + ` LIMIT ${query.limit} OFFSET ${query.offset}`;
      codeSnip2 = where;
    }

    let sqlQuery = `SELECT Diagnosis.id, Diagnosis.name, BodySystem.name as 'body_system', Treatments.name as treatment_name, Diagnosis.source, Diagnosis.created_by, Diagnosis.created_at,
     Treatments.id as 'treatment_id', BodySystem.index as 'body_system_index' FROM Diagnosis
    INNER JOIN Treatments ON Diagnosis.treatment_id=Treatments.id INNER JOIN BodySystems as BodySystem ON BodySystem.id=Diagnosis.body_system_id ` + codeSnip;

    let sqlQueryCount = `SELECT Diagnosis.id, Diagnosis.name, Treatments.body_system, Treatments.name as treatment_name, Diagnosis.source, Diagnosis.created_by, Diagnosis.created_at FROM Diagnosis
    INNER JOIN Treatments ON Diagnosis.treatment_id=Treatments.id INNER JOIN BodySystems as BodySystem ON BodySystem.id=Diagnosis.body_system_id ` + codeSnip2;

    const data = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT
    });

    const tableDataCount = await sequelize.query(sqlQueryCount, {
      type: QueryTypes.SELECT
    });

    const treatments = await Treatments.findAll({
      where: {
        is_deleted: { [Op.not]: true },
        status: { [Op.not]: appConstants.STATUS_INACTIVE }
      },
      attributes: ['name', 'id']
    });


    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Expose-Headers': 'totalPageCount',
        'totalPageCount': tableDataCount.length
      },
      body: JSON.stringify({
        treatments: treatments,
        data: data
      })
    }
  } catch (error) {
    console.log(error);
    const errorResponse = { error: error.message || 'Could not fetch the diagnosis records' };
    return {
      statusCode: error.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponse)
    };
  }
};

const getSignedUrlForS3 = async (input, event) => {
  let fileExtention = '';
  if (input.file_name) {
    fileExtention = `.${input.file_name.split('.').pop()}`;
  }

  let Timestamp = Date.now();

  const s3Params = {
    Bucket: process.env.S3_BUCKET_FOR_TREATMENT_DOCUMENT,
    Key: `${Timestamp}${fileExtention}`,
    ContentType: input.content_type,
    ACL: 'private',
    Metadata: {
      user_id: event.user.id
    },
  };
  return new Promise((resolve, reject) => {
    const uploadURL = s3.getSignedUrl('putObject', s3Params);
    resolve({
      uploadURL,
      s3_file_key: s3Params.Key,
    });
  });
};

const getUploadUrl = async (event) => {
  try {

    authorizeCreate(event.user);

    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;

    validateuploadUrl(input);

    const uploadURLObject = await getSignedUrlForS3(input, event);

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        uploadURL: uploadURLObject.uploadURL,
        s3_file_key: uploadURLObject.s3_file_key,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not get the upload URL.' }),
    };
  }
};

const readSecuredFile = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    return new Promise(function (resolve, reject) {
      AWS.config.update({ region: process.env.S3_BUCKET_FOR_TREATMENT_DOCUMENT_REGION });
      const s3BucketParams = {
        Bucket: process.env.S3_BUCKET_FOR_TREATMENT_DOCUMENT,
        Key: input.s3_file_key,
      };
      s3.getObject(s3BucketParams, function (err, data) {
        if (err) {
          console.log(err);
          reject(err.message);
        } else {
          var data = Buffer.from(data.Body).toString('utf8');
          resolve(data);
        }
      });
    });
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not Read the Document.' }),
    };
  }
}

const uploadDocument = async (event) => {
  try {
    authorizeCreate(event.user);
    const inputObj = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const fileText = await readSecuredFile(event);
    const input = typeof fileText === 'string' ? JSON.parse(fileText) : fileText;

    const validateInput = {
      body_system: input["body-system"]["label-name"],
      name: input.treatment["label-name"],
    }
    validateCreateTreatment(validateInput);

    const { Treatments, Op, Diagnosis, BodySystem, TreatmentHistory } = await connectToDatabase();

    const bodySystemObj = await BodySystem.findOne({
      where: { is_deleted: { [Op.not]: true }, name: input["body-system"]["label-name"] }
    });
    if (!bodySystemObj) throw new HTTPError(400, 'Body system not found');

    const encryptedObj = await encrypt(input);
    const treatmentObj = Object.assign({
      id: input.id || uuid.v4(),
      body_system_id: bodySystemObj.id,
      body_system: input["body-system"]["label-name"],
      body_system_index: input["body-system"]["index-name"],
      name: input.treatment["label-name"],
      status: appConstants.STATUS_ACTIVE,
      policy_doc: encryptedObj,
      created_by: input["version"]["created-by"],
      s3_file_key: inputObj.s3_file_key
    });


    const treatmentObject = await Treatments.findOne({
      where: {
        name: treatmentObj.name,
        body_system: treatmentObj.body_system,
        is_deleted: { [Op.not]: true }
      },
    });

    if (treatmentObject) {
      await Diagnosis.destroy({ where: { treatment_id: treatmentObject.id } });
      await Treatments.destroy({
        where: {
          id: treatmentObject.id
        }
      });
    }

    const treatments = await Treatments.create(treatmentObj);
    await TreatmentHistory.create({
      id: uuid.v4(),
      body_system_id: treatments.body_system_id,
      treatment_id: treatments.id,
      name: treatments.name,
      s3_file_key: treatments.s3_file_key,
      policy_doc: treatments.policy_doc,
      updated_by: event.user.name
    });
    const policy_datas = input.policies;
    if (treatments.type && treatments.type != 'surgery-add-on') {
      if (treatments && policy_datas && policy_datas.length > 0) {
        for (let i = 0; i < policy_datas.length; i += 1) {
          const diagnosisName = policy_datas[i].diagnosis["label-name"];
          const source = policy_datas[i].source;
          const diagnosisObj = Object.assign({
            id: uuid.v4(),
            body_system_id: bodySystemObj.id,
            name: diagnosisName,
            treatment_id: treatments.id,
            source,
            created_by: treatments.created_by
          });
          await Diagnosis.create(diagnosisObj);
        }
      } else {
        const diagnosisObj = Object.assign({
          id: uuid.v4(),
          name: "NoDiagnosisFound",
          body_system_id: bodySystemObj.id,
          treatment_id: treatments.id,
          source: "",
          created_by: treatments.created_by
        });
        await Diagnosis.create(diagnosisObj);
      }
    }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(treatments),
    };

  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not upload the treatment.' }),
    };
  }
};

const getSecuredPublicUrl = async (event) => {
  try {
    authorizeGetOne(event.user);
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    AWS.config.update({ region: process.env.S3_BUCKET_FOR_TREATMENT_DOCUMENT_REGION });
    const publicUrl = await s3.getSignedUrlPromise('getObject', {
      Bucket: process.env.S3_BUCKET_FOR_TREATMENT_DOCUMENT,
      Expires: 60 * 60 * 1,
      Key: input.s3_file_key,
    });
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        publicUrl,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not get the public URL.' }),
    };
  }
};

const getAllDiagnosisNames = async (event) => {
  try {
    // authorizeGetAll(event.user);
    const { sequelize } = await connectToDatabase();
    let sqlQuery = `SELECT DISTINCT (Diagnosis.name) FROM Diagnosis
    INNER JOIN Treatments ON Diagnosis.treatment_id=Treatments.id  WHERE Treatments.is_deleted IS NOT true AND Treatments.status != '${appConstants.STATUS_INACTIVE}'`;

    let diagnosis = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT
    })
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(diagnosis),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not get the diagnosis names.' }),
    };
  }
};

const uploadContentToS3 = async (event) => {
  try {
    let input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    let Timestamp = Date.now();

    const s3Params = {
      Bucket: process.env.S3_BUCKET_FOR_TREATMENT_DOCUMENT,
      Key: `${Timestamp}${event.user.id}`,
      Body: JSON.stringify(input, null, 2),
      ContentType: 'application/json',
      ACL: 'private',
      Metadata: {
        user_id: event.user.id
      },
    };

    return new Promise(function (resolve, reject) {
      s3.upload(s3Params, function (err, data) {
        if (err) {
          console.log(err);
          reject(err.message);
        } else {
          const data = {
            s3_file_key: s3Params.Key
          }
          resolve(data);
        }
      });
    });

  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not upload the content to s3.' }),
    };
  }
};

const getTreatmentHistories = async (event) => {
  try {
    const treatmentId = event.pathParameters.id;
    const { TreatmentHistory } = await connectToDatabase();
    const histories = await TreatmentHistory.findAll({
      where: {
        treatment_id: treatmentId
      },
      order: [
        ['created_at', 'DESC']
      ]
    })
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(histories),
    }
  } catch (error) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not get the treatment histories.' }),
    };
  }
}


// const addFileToGit = async (event) => {
//   try {
//     const input = typeof event.body == "string" ? JSON.parse(event.body) : event.body;
//     var codecommit = new AWS.CodeCommit({ apiVersion: '2015-04-13' });
//     const jsonContent = JSON.stringify(input, null, 2)
//     var branchObj = {
//       branchName: 'master',
//       repositoryName: 'claimsmade_api_testing'
//     };
//     let parentCommitId = new Promise(function (resolve, reject) {
//       codecommit.getBranch(branchObj, function (err, data) {
//         if (err) {
//           console.log(err);
//           reject(err.message);
//         } else {
//           console.log(data.branch.commitId);
//           resolve(data.branch.commitId);
//         }
//       });
//     });
//     var commitObj = {
//       branchName: 'master',
//       repositoryName: 'claimsmade_api_testing',
//       authorName: 'Siva1',
//       commitMessage: 'Adding accupuncture.json',
//       email: 'siva1@miotiv.com',
//       keepEmptyFolders: true,
//       parentCommitId: await parentCommitId,
// deleteFiles: [
//   {
//     filePath: 'policies/cryotherapy.json',
//   }
// ],
// putFiles: [
//         {
//           filePath: 'policies/acupuncture.json',
//           fileContent: jsonContent,
//           fileMode: "EXECUTABLE",
//         },
//       ],
//     };
// codecommit.createCommit(commitObj, function (err, data) {
//   if (err) console.log(err, err.stack);
//   else console.log(data);
// });
//     return {
//       statusCode: 200,
//       headers: {
//         'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true
//       },
//       body: JSON.stringify({
//         status: "Ok"
//       }),
//     }
//   } catch (error) {
//     console.log(error);
//     return {
//       statusCode: error.statusCode || 500,
//       headers: {
//         'Content-Type': 'text/plain',
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Credentials': true
//       },
//       body: JSON.stringify({ error: error.message || 'Could not update' }),
//     };
//   }
// }


const destroyTreatmentRecords = async (event) => {
  try {
    const treatment_id = event.pathParameters.id;
    console.log(treatment_id);

    const { Treatments, Op, Diagnosis, TreatmentHistory } = await connectToDatabase();

    await Diagnosis.destroy({ where: {} });
    await Treatments.destroy({ where: {} });
    await TreatmentHistory.destroy({ where: {} });

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ status: 'Ok' }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not delete the record.' }),
    };
  }
}

const uploadContentToS3ForDownload = async (event) => {
  try {
    let input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    const treatmentName = input["treatment"]["index-name"];
    let timeStamp = Date.now();
    const key = treatmentName + "_" + timeStamp;

    const s3Params = {
      Bucket: process.env.S3_BUCKET_FOR_TREATMENT_DOCUMENT,
      Key: `${key}`,
      Body: JSON.stringify(input, null, 2),
      ACL: 'private',
      Metadata: {
        user_id: event.user.id
      },
    };

    return new Promise(function (resolve, reject) {
      s3.upload(s3Params, function (err, data) {
        if (err) {
          console.log(err);
          reject(err.message);
        } else {
          const data = {
            s3_file_key: s3Params.Key
          }
          resolve(data);
        }
      });
    });

  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not upload the content to s3.' }),
    };
  }
};

const getDownloadUrl = async (event) => {
  try {
    authorizeGetOne(event.user);

    const uploadContentObject = await uploadContentToS3ForDownload(event);
    AWS.config.update({ region: process.env.S3_BUCKET_FOR_TREATMENT_DOCUMENT_REGION });

    const publicUrl = await s3.getSignedUrlPromise('getObject', {
      Bucket: process.env.S3_BUCKET_FOR_TREATMENT_DOCUMENT,
      Expires: 60 * 60 * 1,
      Key: uploadContentObject.s3_file_key,
      ResponseContentDisposition: 'attachment; filename ="' + encodeURIComponent(uploadContentObject.s3_file_key) + ".json" + '"'
    });
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        publicUrl,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({ error: err.message || 'Could not get the download URL.' }),
    };
  }
};

const getTreatmentDiagnosisByType = async (event) => {
  try {
    const input = typeof event.body === 'string' ? JSON.parse(event.body) : event.body;
    // authorizeGetPolicies(event.user);
    const test = Object.assign({ input });

    const { Treatments, Op, Diagnosis } = await connectToDatabase();
    let treatments = await Treatments.findAll({
      where: {
        is_deleted: { [Op.not]: true },
        status: { [Op.not]: appConstants.STATUS_INACTIVE },
        body_system_index: input.body_system
      },
      order: [
        ['name', 'ASC'],
      ],
      attributes: ['name', 'id', 'type'],
    });

    let diagnosis = await Diagnosis.findAll({
      where: {

      },
      order: [
        ['name', 'ASC'],
      ],
      attributes: ['name', 'id'],
    });

    // if (!treatments) throw new HTTPError(404, 'Couldn\'t find any treatment');

    const responseData = {
      "Treatments": treatments,
      "Diagnosis": diagnosis
    };

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(responseData),
    };
  } catch (err) {
    console.log(err);
    const errorResponseData = { error: err.message || 'Couldn\'t fetch the records' };
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponseData),
    };
  }
};

const getAllTreatmentNames = async (event) => {
  try {
    const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;

    const { sequelize } = await connectToDatabase();
    const sqlQuery = `SELECT Treatments.id, Treatments.name, Treatments.type FROM Treatments WHERE Treatments.body_system_id = '${input.body_system_id}'`
    const treatmentData = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT
    })

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(treatmentData)
    }

  } catch (err) {
    console.log(err);
    const errorResponseData = { error: err.message || 'Couldn\'t fetch the records' };
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponseData),
    };
  }
}

const getAllSyncData = async (event) => {
  try {

    const { sequelize } = await connectToDatabase();
    const bodySystemQuery = 'SELECT * from BodySystems'
    const bodySystemData = await sequelize.query(bodySystemQuery, {
      type: QueryTypes.SELECT
    })

    const treatmentQuery = 'SELECT * from Treatments'
    const treatmentData = await sequelize.query(treatmentQuery, {
      type: QueryTypes.SELECT
    })

    const diagnosisQuery = 'SELECT * from Diagnosis'
    const diagnosisData = await sequelize.query(diagnosisQuery, {
      type: QueryTypes.SELECT
    })

    const resultData = { bodySystemData: bodySystemData, treatmentData: treatmentData, diagnosisData: diagnosisData }

    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(resultData)
    }

  } catch (err) {
    console.log(err);
    const errorResponseData = { error: err.message || 'Couldn\'t fetch the records' };
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponseData),
    };
  }
}

const updateAPIForsync = async (event) => {
  const { Treatments, BodySystem, Diagnosis } = await connectToDatabase();
  console.log(event.user);

  try {

    if (process.env.NODE_ENV === 'production' && event.user.email === appConstants.ADMIN_EMAIL_ID) {
      function getRequest() {
        const url = process.env.POLICY_DOC_SYNC_URL;

        return new Promise((resolve, reject) => {
          const req = https.get(url, res => {
            let rawData = '';

            res.on('data', chunk => {
              rawData += chunk;
            });

            res.on('end', () => {
              try {
                resolve(JSON.parse(rawData));
              } catch (err) {
                reject(new Error(err));
              }
            });
          });

          req.on('error', err => {
            reject(new Error(err));
          });
        });
      }

      const result = await getRequest();

      //To clean up the existing data
      await Diagnosis.destroy({
        where: {}
      })
      await Treatments.destroy({
        where: {}
      })
      await BodySystem.destroy({
        where: {}
      })

      //To update the data from staging to production
      await BodySystem.bulkCreate(result.bodySystemData);
      await Treatments.bulkCreate(result.treatmentData);
      await Diagnosis.bulkCreate(result.diagnosisData);

    } else {
      throw new HTTPError(400, `You don\'t have access`)
    }

    // 👇️️response structure assume you use proxy integration with API gateway
    return {
      statusCode: 200,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ Message: 'Synced successfully' })
    };
  } catch (error) {
    console.log('Error is: ', error);
    return {
      statusCode: 400,
      body: JSON.stringify({ error: error.message || 'Could not update the record.' }),
    };
  }
};

const getAllDiagnosisByBodysystemId = async (event) => {
  try {
    const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
    const { sequelize } = await connectToDatabase();

    const diagnosisQuery = `SELECT Diagnosis.id, Diagnosis.name FROM Diagnosis WHERE treatment_id = '${input.treatment_id}' AND body_system_id = '${input.body_system_id}';`;

    const othersQuery = `SELECT Diagnosis.id, Diagnosis.name FROM Diagnosis WHERE body_system_id = '${input.body_system_id}' AND treatment_id != '${input.treatment_id}' `;

    const diagnosisList = await sequelize.query(diagnosisQuery, {
      type: QueryTypes.SELECT
    })
    const othersList = await sequelize.query(othersQuery, {
      type: QueryTypes.SELECT
    })
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify({
        diagnosisList: diagnosisList,
        othersList: othersList
      })
    }
  } catch (err) {
    console.log(err);
    const errorResponseData = { error: err.message || 'Couldn\'t fetch the records' };
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponseData),
    };
  }
}

const getAllTreatmentDetails = async (event) => {
  try {
    const { sequelize } = await connectToDatabase();
    const sqlQuery = `SELECT BodySystems.id as 'bodySystemId', BodySystems.index, BodySystems.name as 'bodySystemName', Treatments.id as 'treatmentsId',  Treatments.type, Treatments.name as 'treatmentsName', Diagnosis.id as 'diagnosisId', Diagnosis.name as 'diagnosisName' FROM BodySystems LEFT JOIN Treatments ON BodySystems.id = Treatments.body_system_id LEFT JOIN Diagnosis ON Treatments.id = Diagnosis.treatment_id WHERE BodySystems.is_deleted IS NOT TRUE AND Treatments.status = '${appConstants.STATUS_ACTIVE}';`

    const data = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT
    })
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log(err);
    const errorResponseData = { error: err.message || 'Couldn\'t fetch the records' };
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponseData),
    };
  }
}


const getAllTreatmentByBodySystems = async (event) => {
  try {
    const input = typeof event.body == 'string' ? JSON.parse(event.body) : event.body;
    const { sequelize } = await connectToDatabase();
    const bodySystemIds = input.bodySystemIds;

    var sqlList = '('
    for (let i = 0; i < bodySystemIds.length; i++) {
      sqlList += "'" + bodySystemIds[i] + "'";
      bodySystemIds[i + 1] ? sqlList += ', ' : ''
    }
    sqlList += ')'
    const sqlQuery = `SELECT BodySystems.id as 'bodySystemId', BodySystems.index, BodySystems.name as 'bodySystemName', Treatments.id as 'treatmentsId',  Treatments.type, Treatments.name as 'treatmentsName', Diagnosis.id as 'diagnosisId', Diagnosis.name as 'diagnosisName' FROM BodySystems LEFT JOIN Treatments ON BodySystems.id = Treatments.body_system_id LEFT JOIN Diagnosis ON Treatments.id = Diagnosis.treatment_id WHERE BodySystems.is_deleted IS NOT TRUE AND Treatments.status = '${appConstants.STATUS_ACTIVE}' AND Treatments.body_system_id IN ${sqlList} AND Treatments.is_deleted IS NOT TRUE;`;

    const data = await sequelize.query(sqlQuery, {
      type: QueryTypes.SELECT
    });
    return {
      statusCode: 200,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log(err);
    const errorResponseData = { error: err.message || 'Couldn\'t fetch the records' };
    return {
      statusCode: err.statusCode || 500,
      headers: {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': true
      },
      body: JSON.stringify(errorResponseData),
    };
  }

}


module.exports.create = middy(create).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = getAll;
module.exports.get_policies = middy(get_policies).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.get_policy_doc = middy(get_policy_doc).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAllDiagnoses = getAllDiagnoses;
module.exports.getUploadUrl = middy(getUploadUrl).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.uploadDocument = middy(uploadDocument).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getSecuredPublicUrl = middy(getSecuredPublicUrl).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.readSecuredFile = readSecuredFile;
module.exports.getAllDiagnosisNames = getAllDiagnosisNames;
module.exports.uploadContentToS3 = middy(uploadContentToS3).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.updateTreatmentStatus = middy(updateTreatmentStatus).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getTreatmentHistories = getTreatmentHistories;
module.exports.destroyTreatmentRecords = middy(destroyTreatmentRecords).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getDownloadUrl = middy(getDownloadUrl).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getTreatmentDiagnosisByType = getTreatmentDiagnosisByType;
module.exports.getAllTreatmentNames = getAllTreatmentNames;
module.exports.getAllSyncData = getAllSyncData;
module.exports.updateAPIForsync = updateAPIForsync;
module.exports.getAllDiagnosisByBodysystemId = getAllDiagnosisByBodysystemId;
module.exports.getAllTreatmentDetails = getAllTreatmentDetails;
module.exports.getAllTreatmentByBodySystems = getAllTreatmentByBodySystems;

