const Validator = require('validatorjs');
const { appConstants } = require('../../appConstants');
const { HTTPError } = require('../../utils/httpResp');
const treatmentStatus = [appConstants.STATUS_ACTIVE, appConstants.STATUS_INACTIVE]

exports.validateCreateTreatment = function (data) {
  const rules = {
    body_system: 'required',
    name: 'required',
    type: 'required'
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }

  if (!appConstants.POLICY_TYPES.includes(data.type)) throw new HTTPError(400, 'Invalid treatment type')
};

exports.validateUpdateTreatment = function (data) {
  const rules = {
    body_system: 'required',
    name: 'required',
    type: 'required'
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }

  if (!appConstants.POLICY_TYPES.includes(data.type)) throw new HTTPError(400, 'Invalid treatment type')
};

exports.validateGetOneTreatment = function (data) {
  const rules = {
    id: 'required',
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};

exports.validateuploadUrl = function (data) {
  const rules = {
    file_name: 'required',
    content_type: 'required'
  };
  const validation = new Validator(data, rules);

  if (validation.fails()) {
    throw new HTTPError(400, validation.errors.all());
  }
};

exports.validateTreatmentStatus = function (data) {
  const rules = {
    status: 'required'
  }
  const validation = new Validator(data, rules);
  if (validation.fails()) throw new HTTPError(400, validation.errors.all());

  if (!treatmentStatus.includes(data.status)) throw new HTTPError(400, 'Invalid status')
}