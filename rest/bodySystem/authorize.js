const { HTTPError } = require("../../utils/httpResp");

const majorRoles = ['superAdmin'];

exports.authorizeCreate = function (user) {
    if (!majorRoles.includes(user.role)) {
        throw new HTTPError(403, 'you don\'t have access');
    }
}

exports.authorizeGetOne = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(403, 'you don\'t have access');
}

exports.authorizeUpdate = function (user) {
    if (!majorRoles.includes(user.role)) throw new HTTPError(403, 'you don\'t have access');
}