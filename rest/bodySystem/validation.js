const { HTTPError } = require("../../utils/httpResp");
const Validator = require('validatorjs');

exports.validateCreate = function (data) {
    const rules = {
        name: 'required|min:4|max:64'
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all());
}

exports.validateUpdate = function (data) {
    const rules = {
        name: 'required|min:4|max:64'
    };
    const validation = new Validator(data, rules);
    if (validation.fails()) throw new HTTPError(400, validation.errors.all());
}