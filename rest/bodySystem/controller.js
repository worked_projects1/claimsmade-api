const middy = require('@middy/core');
const doNotWaitForEmptyEventLoop = require('@middy/do-not-wait-for-empty-event-loop');
const authMiddleware = require('../../auth');
const connectToDataBase = require('../.././db');
const uuid = require('uuid');
const { HTTPError } = require('../../utils/httpResp');
const { validateCreate, validateUpdate } = require('./validation');
const { authorizeCreate, authorizeGetOne, authorizeUpdate } = require('./authorize');

const create = async (event) => {
    try {
        const input = typeof event.body == "string" ? JSON.parse(event.body) : event.body;
        authorizeCreate(event.user);
        validateCreate(input);
        const indexName = input.name.replace(/\s+/g, "-").toLowerCase();
        const { BodySystem, Op, Settings } = await connectToDataBase();
        const isAlreadyExist = await BodySystem.findOne({
            where: {
                name: input.name,
                is_deleted: { [Op.not]: true }
            }
        });
        if (isAlreadyExist) throw new HTTPError(400, `Body System with name: "${input.name}" already exists`);
        const bodySysObj = {
            id: uuid.v4(),
            name: input.name,
            index: indexName,
            created_by: event.user.id
        }
        const res = await BodySystem.create(bodySysObj);

        const settings = await Settings.findOne({
            where: { key: 'global_settings' }
        });
        const updatedObject = Object.assign(JSON.parse(settings.value), { policy_doc_updated_date: new Date() })
        settings.value = JSON.stringify(updatedObject);
        await settings.save();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(res)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not create the bodysystem.' })
        }
    }
}

const getOne = async (event) => {
    try {
        authorizeGetOne(event.user);
        const bodySystemId = event.pathParameters.id;
        const { BodySystem, Users, Op } = await connectToDataBase();
        const data = await BodySystem.findOne({
            where: { id: bodySystemId },
        });
        if (!data) throw (404, `Body system not found`)
        const user = await Users.findOne({
            where: {
                id: data.created_by,
                is_deleted: { [Op.not]: true }
            }
        });
        if (user) {
            data.created_by = user.name;
        }
        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(data)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the record.' })
        }
    }
}

const update = async (event) => {
    try {
        const input = typeof event.body == "string" ? JSON.parse(event.body) : event.body;
        const bodySystemId = event.pathParameters.id;
        validateUpdate(input);
        authorizeUpdate(event.user);
        const { BodySystem, Op, Settings } = await connectToDataBase();
        const bodySystemObj = await BodySystem.findOne({
            where: {
                id: bodySystemId,
                is_deleted: { [Op.not]: true }
            }
        });
        if (!bodySystemObj) throw new HTTPError(404, `No Record found`);

        const isAlreadyExist = await BodySystem.findOne({
            where: {
                id: { [Op.not]: bodySystemId },
                name: input.name,
                is_deleted: { [Op.not]: true }
            }
        });
        if (isAlreadyExist) throw new HTTPError(400, `Body system with name: "${input.name}" already found`)

        input.index = input.name.replace(/\s+/g, "-").toLowerCase();
        const updatedBodySystem = Object.assign(bodySystemObj, input)
        const response = await updatedBodySystem.save();

        const settings = await Settings.findOne({
            where: { key: 'global_settings' }
        });
        const updatedObject = Object.assign(JSON.parse(settings.value), { policy_doc_updated_date: new Date() })
        settings.value = JSON.stringify(updatedObject);
        await settings.save();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(response)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not update the record.' })
        }
    }
}

const destroy = async (event) => {
    try {
        const bodySystemId = event.pathParameters.id;
        const { BodySystem, Op, Settings } = await connectToDataBase();
        const bodySysObj = await BodySystem.findOne({
            where: {
                id: bodySystemId,
                is_deleted: { [Op.not]: true }
            }
        });
        if (!bodySysObj) throw new HTTPError(404, `No Record Found`);
        const updatedObj = Object.assign(bodySysObj, { is_deleted: true });
        await updatedObj.save();

        const settings = await Settings.findOne({
            where: { key: 'global_settings' }
        });
        const updatedObject = Object.assign(JSON.parse(settings.value), { policy_doc_updated_date: new Date() })
        settings.value = JSON.stringify(updatedObject);
        await settings.save();

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({
                status: "Ok",
                message: "Deleted"
            })
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not delete the record.' })
        }
    }
}

const getAll = async (event) => {
    try {
        const input = typeof event.body == "string" ? JSON.parse(event.body) : event.body;
        const query = event.headers;
        const { BodySystem, Users, Op, sequelize } = await connectToDataBase();
        let sortObj = {};
        if (query.limit) query.limit = parseInt(query.limit, 25);
        if (query.offset) query.offset = parseInt(query.offset, 0);

        if (!query.sort || query.sort == "false") {
            sortObj = ['created_at', 'DESC']
        } else {
            query.sort = JSON.parse(query.sort);
            sortObj = [query.sort.column, query.sort.type]
        }

        if (!query.search || query.search == "false") {
            query.search = ""
        }
        const data = await BodySystem.findAll({
            where: {
                [Op.or]: [
                    { name: { [Op.like]: '%' + query.search + '%' } },
                ],
                is_deleted: { [Op.not]: true },
            },
            limit: query.limit,
            offset: query.offset,
            order: [
                sortObj
            ],
            raw: true
        });
        //To get the createdBy name using userId
        for (let i = 0; i < data.length; i++) {
            let userId = data[i].created_by;
            const user = await Users.findOne({ where: { id: userId } });
            if (user) {
                data[i].created_by = user.name;
            }
        }

        return {
            statusCode: 200,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify(data)
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: err.statusCode || 500,
            headers: {
                'Content-Type': 'text/plain',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': true
            },
            body: JSON.stringify({ error: err.message || 'Could not get the records.' })
        }
    }
}

const getAllBodySystemNames = async (event) => {
    try {
        const headers = event.headers;
        const { BodySystem, Op } = await connectToDataBase();
        const bodySysObj = await BodySystem.findAll({
            where: {
                is_deleted: { [Op.not]: true }
            },
            attributes: ['id', 'name', 'index']
        });
        return {
            statusCode: 200, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify(bodySysObj),
        };
    } catch (err) {
        return {
            statusCode: err.statusCode || 500, headers: {
                "Content-Type": "text/plain",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
            }, body: JSON.stringify({
                error: err.message || "Could not fetch the body system names.",
            }),
        };
    }
}


module.exports.create = create;
module.exports.update = middy(update).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getOne = middy(getOne).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.destroy = middy(destroy).use(authMiddleware()).use(doNotWaitForEmptyEventLoop());
module.exports.getAll = getAll;
module.exports.getAllBodySystemNames = getAllBodySystemNames;

